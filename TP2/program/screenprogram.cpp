
#include <QtGui/QOpenGLTexture>

#include <iostream>
#include "screenprogram.h"

namespace Program {

ScreenProgram::ScreenProgram()
    : AScreenProgram(DEFAULT)
{
}

bool ScreenProgram::queryInput()
{
    _input["vertexIn"] = _attributeLocation("vertexIn");
    _input["renderedTexture"] = _uniformLocation("renderedTexture");
    return true;
}

bool ScreenProgram::render(QOpenGLFramebufferObject *fbo)
{
    if (fbo != nullptr) {

        glEnable(GL_TEXTURE_2D);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, fbo->texture());
        _program->setUniformValue(_input["renderedTexture"], 1);

        static float vertices[] = {-1, -1, 0, 1, -1, 0, 1, 1, 0, -1, 1, 0 };
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, vertices);

        static unsigned int indexes[] = { 0, 1, 2, 0, 2, 3 };
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indexes);

        glDisableClientState(GL_VERTEX_ARRAY);

        glBindTexture(GL_TEXTURE_2D, 0);

        glDisable(GL_TEXTURE_2D);

        return true;
    }
    return false;
}

}

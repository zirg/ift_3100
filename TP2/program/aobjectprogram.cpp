
#include "aobjectprogram.h"

namespace Program {

AObjectProgram::AObjectProgram(eProgram type)
    : _type(type)
{
}

AObjectProgram::~AObjectProgram()
{
    _light_input.clear();
}

AObjectProgram::eProgram   AObjectProgram::getType() const
{
    return _type;
}

void    AObjectProgram::setType(eProgram type)
{
    _type = type;
}


bool    AObjectProgram::queryInput()
{
    _input["pos"] = _attributeLocation("pos");
    _input["normal"] = _attributeLocation("normal");
    _input["perpective_mat"] = _uniformLocation("cam.perspective_matrix");
    _input["mouvement_mat"] = _uniformLocation("cam.mouvement_matrix");
    //_input["normal_mat"] = _uniformLocation("cam.normal_matrix");
    _input["transform_mat"] = _uniformLocation("cam.transform_matrix");
    _input["ambiant_color"]= _uniformLocation("mat.ambiant_color");
    _input["diffuse_color"] = _uniformLocation("mat.diffuse_color");
    for (int i = 0; i < 8; i++)
        _queryLight(i);
    return true;
}

void    AObjectProgram::_queryLight(int idx)
{
    char    name[128];

    sprintf(name, "lights[%d].%s", idx, "type");
    _light_input[idx]["type_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "position");
    _light_input[idx]["pos_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "direction");
    _light_input[idx]["dir_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "ambient");
    _light_input[idx]["ambient_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "diffuse");
    _light_input[idx]["diffuse_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "specular");
    _light_input[idx]["specular_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "intensity");
    _light_input[idx]["intensity_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "constant_att");
    _light_input[idx]["constant_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "linear_att");
    _light_input[idx]["linear_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "quadratic_att");
    _light_input[idx]["quadratic_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "cutoff");
    _light_input[idx]["cutoff_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "coscutoff");
    _light_input[idx]["coscutoff_l"] = _uniformLocation(name);
    sprintf(name, "lights[%d].%s", idx, "exponent");
    _light_input[idx]["exponent_l"] = _uniformLocation(name);
}

bool AObjectProgram::bindCamera(Camera *c)
{
    if (c == nullptr)
        return false;

    _program->setUniformValue(_input["perpective_mat"], c->perspective());
    _program->setUniformValue(_input["mouvement_mat"], c->position());
    //_program->setUniformValue(_input["normal_mat"], c->normal());
    return true;
}

bool AObjectProgram::bindWorld(const QMatrix4x4& mat4)
{
    _program->setUniformValue(_input["transform_mat"], mat4);
    return true;
}

bool AObjectProgram::bindMaterial(Material *m)
{
    if (m == nullptr)
        return false;

    _program->setUniformValue(_input["ambiant_color"], m->getAmbiant());
    _program->setUniformValue(_input["diffuse_color"], m->getDiffuse());
    return true;
}

bool AObjectProgram::bindLight(std::list<Light *>& l)
{
    if (l.size() == 0)
        return false;

    int i = 0;
    for (auto it = l.begin(); it != l.end(); ++it)
    {
        Light *li = *it;
        _program->setUniformValue(_light_input[i]["type_l"], li->getType());
        _program->setUniformValue(_light_input[i]["pos_l"], li->getTranslation());
        _program->setUniformValue(_light_input[i]["dir_l"], li->getDirection());
        _program->setUniformValue(_light_input[i]["ambient_l"], li->getAmbiant());
        _program->setUniformValue(_light_input[i]["diffuse_l"], li->getDiffuse());
        if (_light_input[i]["specular_l"] != -1)
        {
            _program->setUniformValue(_light_input[i]["specular_l"], li->getSpecular());
        }
        _program->setUniformValue(_light_input[i]["intensity_l"], li->getIntensity());
        _program->setUniformValue(_light_input[i]["constant_l"], li->getConstantAtt());
        _program->setUniformValue(_light_input[i]["linear_l"], li->getLinearAtt());
        _program->setUniformValue(_light_input[i]["quadratic_l"], li->getQuadraticAtt());
        //_program->setUniformValue(_light_input[i]["cutoff_l"], li->getCutoff());
        _program->setUniformValue(_light_input[i]["coscutoff_l"], li->getCosCutOff());
        _program->setUniformValue(_light_input[i]["exponent_l"], li->getExponent());
        i++;
    }
    while (i < 8)
    {
        _program->setUniformValue(_light_input[i]["type_l"], -1);
        i++;
    }
    return true;
}

bool AObjectProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

   //VBO BUFFER
   g->vboVertex()->bind();
   _program->enableAttributeArray(_input["pos"]);
   _program->setAttributeBuffer(_input["pos"], GL_FLOAT, 0, 3);
   g->vboVertex()->release();

   g->vboNormal()->bind();
   _program->enableAttributeArray(_input["normal"]);
   _program->setAttributeBuffer(_input["normal"], GL_FLOAT, 0, 3);
   g->vboNormal()->release();

   //Draw faces triangles
   g->vboIndex()->bind();
   glDrawElements(GL_TRIANGLES, g->faces().size(), GL_UNSIGNED_INT, NULL);
   g->vboIndex()->release();

   _program->disableAttributeArray(_input["pos"]);
   _program->disableAttributeArray(_input["normal"]);

   return true;
}
}

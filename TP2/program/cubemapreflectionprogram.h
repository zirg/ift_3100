#ifndef CUBEMAPREFLECTIONPROGRAM_H
#define CUBEMAPREFLECTIONPROGRAM_H

#include <QOpenGLTexture>

#include "aobjectprogram.h"

namespace Program {

class CubeMapReflectionProgram : public AObjectProgram
{
private:
    QOpenGLTexture *_texture;
public:
    CubeMapReflectionProgram();

    ~CubeMapReflectionProgram() = default;

    bool queryInput();

    bool bindMaterial(Material *m);
    bool bindCamera(Camera *c);
    bool bindLight(std::list<Light *>& l);

    bool renderGeometry(Geometry *g);
};

}

#endif // CUBEMAPREFLECTIONPROGRAM_H


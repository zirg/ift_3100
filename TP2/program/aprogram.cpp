#include "aprogram.h"

Program::AProgram::AProgram()
    :  _program(nullptr)
{
    if (QOpenGLContext::currentContext() != nullptr)
    {
         initializeOpenGLFunctions();
         _program = new QOpenGLShaderProgram(QOpenGLContext::currentContext());
    }
}

Program::AProgram::~AProgram()
{
    if (_program != nullptr)
        delete _program;
    _program = nullptr;
    _input.clear();
}

bool Program::AProgram::addVertexShader(const std::string& path)
{
    //QGLShader deprecated
    QString file_qrc(path.data());
    return _program->addShaderFromSourceFile(QOpenGLShader::Vertex, file_qrc);
}

bool Program::AProgram::addFragmentShader(const std::string& path)
{
    //QGLShader deprecated
    QString file_qrc(path.data());
    return _program->addShaderFromSourceFile(QOpenGLShader::Fragment, file_qrc);
}

bool Program::AProgram::link()
{
    if (_program == NULL)
        return false;
    if (_program->link() == false)
        return false;
    _program->dumpObjectInfo();
    _program->bind();
    queryInput();
    _program->release();
    return true;
}

bool Program::AProgram::bind()
{
    if (_program == NULL)
        return false;
    if (_program->bind() == false)
        return false;
    return true;
}

bool Program::AProgram::release()
{
    if (_program == NULL)
        return false;
    _program->release();
    return true;
}

GLint Program::AProgram::_attributeLocation(const char *name)
{
    if (_program == NULL)
        return -1;
    GLint v = glGetAttribLocation(_program->programId(),name);
    return v;
}

//#include <iostream>
GLint Program::AProgram::_uniformLocation(const char *name)
{
    if (_program == NULL)
        return -1;
    GLint v = glGetUniformLocation(_program->programId(), name);

    //std::cout << "Uniform [" << name << "]: "<< v  << std::endl;
    return v;
}

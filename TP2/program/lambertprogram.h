#ifndef LAMBERTPROGRAM_H
#define LAMBERTPROGRAM_H

#include <iostream>
#include <list>
#include <map>

#include "program.h"

namespace Program {

class LambertProgram : public AObjectProgram
{
public:
    LambertProgram();
    ~LambertProgram();

    bool bindCamera(Camera *c);
    bool bindMaterial(Material *m);
    bool bindWorld(const QMatrix4x4& mat4);
    bool bindLight(std::list<Light *>& l);
    bool renderGeometry(Geometry *g);
};
}
#endif // LAMBERTPROGRAM_H

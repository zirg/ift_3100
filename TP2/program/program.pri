HEADERS += $$PWD/program.h \
    $$PWD/basicprogram.h \
    $$PWD/pointprogram.h \
    $$PWD/lineprogram.h \
    $$PWD/lambertprogram.h \
    $$PWD/aprogram.h \
    $$PWD/phongprogram.h \
    $$PWD/textureprogram.h \
    $$PWD/normalmapprogram.h \
    $$PWD/toonprogram.h \
    $$PWD/aobjectprogram.h \
    $$PWD/screenprogram.h \
    $$PWD/ascreenprogram.h \
    $$PWD/cubemapprogram.h \
    $$PWD/cubemapreflectionprogram.h \
    $$PWD/earthprogram.h

SOURCES += \
    $$PWD/basicprogram.cpp \
    $$PWD/pointprogram.cpp \
    $$PWD/lineprogram.cpp \
    $$PWD/lambertprogram.cpp \
    $$PWD/aprogram.cpp \
    $$PWD/phongprogram.cpp \
    $$PWD/textureprogram.cpp \
    $$PWD/normalmapprogram.cpp \
    $$PWD/toonprogram.cpp \
    $$PWD/aobjectprogram.cpp \
    $$PWD/screenprogram.cpp \
    $$PWD/ascreenprogram.cpp \
    $$PWD/cubemapprogram.cpp \
    $$PWD/cubemapreflectionprogram.cpp \
    $$PWD/earthprogram.cpp

#include "lineprogram.h"

Program::LineProgram::LineProgram()
{
    _type = WIREFRAME;
}

Program::LineProgram::~LineProgram()
{

}

bool Program::LineProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

    //VBO BUFFER
    g->vboVertex()->bind();
    _program->enableAttributeArray(_input["position"]);
    _program->setAttributeBuffer(_input["position"], GL_FLOAT, 0, 3);
    g->vboVertex()->release();

    //Draw faces triangles
    g->vboIndex()->bind();
    glDrawElements(GL_LINES, g->faces().size(), GL_UNSIGNED_INT, NULL);
    g->vboIndex()->release();

    _program->disableAttributeArray(_input["position"]);
    return true;
}

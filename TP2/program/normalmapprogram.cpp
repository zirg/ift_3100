#include "normalmapprogram.h"

namespace Program {

NormalMapProgram::NormalMapProgram()
    : AObjectProgram(NORMAL_MAP) {
}

bool NormalMapProgram::queryInput()
{

    _input["uv"] = _attributeLocation("uv");
    _input["texture"] = _uniformLocation("texture");
    _input["texture_repeat"] = _uniformLocation("texture_repeat");
    _input["texture_alpha"] = _uniformLocation("texture_alpha");
    _input["texture_normal"] = _uniformLocation("texture_normal");
    return AObjectProgram::queryInput();
}

bool NormalMapProgram::bindCamera(Camera *c)
{
  return AObjectProgram::bindCamera(c);
}

bool NormalMapProgram::bindWorld(const QMatrix4x4& mat4)
{
    return AObjectProgram::bindWorld(mat4);
}

bool NormalMapProgram::bindMaterial(Material *m)
{
    if (m == nullptr)
        return false;

    AObjectProgram::bindMaterial(m);
    _program->setUniformValue(_input["texture_repeat"], m->getTextureRepeat());
    _program->setUniformValue(_input["texture_alpha"], m->getTextureAlpha());
    QOpenGLTexture* texture = m->getTexture();
    if (texture != nullptr)
    {
        glActiveTexture(GL_TEXTURE1);
        texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
        texture->setMagnificationFilter(QOpenGLTexture::Linear);
        texture->bind();
        _program->setUniformValue(_input["texture"], 1);
    }
    QOpenGLTexture* textureMap = m->getNormalMap();
    if (textureMap != nullptr)
    {
        glActiveTexture(GL_TEXTURE2);
        textureMap->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
        textureMap->setMagnificationFilter(QOpenGLTexture::Linear);
        textureMap->bind();
        _program->setUniformValue(_input["texture_normal"], 2);
        return true;
    }
    return false;
}

bool NormalMapProgram::bindLight(std::list<Light *>& l)
{
    return AObjectProgram::bindLight(l);
}

bool NormalMapProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

    glEnable(GL_TEXTURE_2D);

    glEnableVertexAttribArray(_input["uv"]);
    glVertexAttribPointer(_input["uv"], 2, GL_FLOAT, GL_FALSE, 0, g->uvs().data());

    AObjectProgram::renderGeometry(g);

    glDisableVertexAttribArray(_input["uv"]);

    glDisable(GL_TEXTURE_2D);

    return true;
}

}

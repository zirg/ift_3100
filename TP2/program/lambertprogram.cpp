#include "lambertprogram.h"

Program::LambertProgram::LambertProgram()
    : AObjectProgram(LAMBERT) {

}

Program::LambertProgram::~LambertProgram()
{

}

bool Program::LambertProgram::bindCamera(Camera *c)
{
    return AObjectProgram::bindCamera(c);
}

bool Program::LambertProgram::bindWorld(const QMatrix4x4& mat4)
{
    return AObjectProgram::bindWorld(mat4);
}

bool Program::LambertProgram::bindMaterial(Material *m)
{
    return AObjectProgram::bindMaterial(m);
}

bool Program::LambertProgram::bindLight(std::list<Light *>& l)
{
    return AObjectProgram::bindLight(l);
}

bool Program::LambertProgram::renderGeometry(Geometry *g)
{
    return AObjectProgram::renderGeometry(g);
}

#ifndef LINEPROGRAM_H
#define LINEPROGRAM_H

#include <iostream>
#include <list>
#include <map>

#include "program.h"
#include "basicprogram.h"

namespace Program {

class LineProgram : public BasicProgram
{
public:
    LineProgram();
    ~LineProgram();
    bool renderGeometry(Geometry *g);
};
}
#endif // LINEPROGRAM_H

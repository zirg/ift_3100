#ifndef POINTPROGRAM_H
#define POINTPROGRAM_H

#include <iostream>
#include <list>
#include <map>

#include "program.h"
#include "basicprogram.h"

namespace Program {

class PointProgram : public BasicProgram
{
public:
    PointProgram();
    ~PointProgram();
    bool renderGeometry(Geometry *g);
};
}
#endif // POINTPROGRAM_H

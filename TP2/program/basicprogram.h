#ifndef BASICPROGRAM_H
#define BASICPROGRAM_H

#include <iostream>
#include <list>
#include <map>

#include "aobjectprogram.h"

namespace Program {

class BasicProgram : public AObjectProgram
{
public:
    BasicProgram();
    ~BasicProgram();

    bool queryInput();

    bool bindCamera(Camera *c);
    bool bindWorld(const QMatrix4x4& mat4);
    bool bindMaterial(Material *m);
    bool bindLight(std::list<Light *>& l);
    virtual bool renderGeometry(Geometry *g);
};

}

#endif // BASICPROGRAM_H

#ifndef NORMALMAPPROGRAM_H
#define NORMALMAPPROGRAM_H

#include "aobjectprogram.h"

namespace Program {

class NormalMapProgram : public AObjectProgram
{
public:
    NormalMapProgram();

    ~NormalMapProgram() = default;

    bool queryInput();

    bool bindCamera(Camera *c);

    bool bindWorld(const QMatrix4x4& );

    bool bindMaterial(Material *m);

    bool bindLight(std::list<Light *>& l);

    bool renderGeometry(Geometry *g);
};

}

#endif // NORMALMAPPROGRAM_H


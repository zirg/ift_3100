#ifndef APROGRAM
#define APROGRAM

#include <list>
#include <map>
#include <QOpenGLFunctions>
#include <QOpenGLContext>
#include <QOpenGLShaderProgram>
#include <QOpenGLShader>

namespace Program {

class AProgram : protected QOpenGLFunctions
{
public:
    AProgram();
    ~AProgram();

    bool addVertexShader(const std::string& path);
    bool addFragmentShader(const std::string& path);
    bool link(); //Calls queryInput
    bool bind();
    bool release();

    virtual bool queryInput() = 0;

protected:
    QOpenGLShaderProgram                          *_program;
    std::map<std::string, GLint>                  _input;

    GLint _attributeLocation(const char *name);
    GLint _uniformLocation(const char *name);
};
}

#endif // APROGRAM



#include "textureprogram.h"


namespace Program {

TextureProgram::TextureProgram()
    : AObjectProgram(TEXTURE) {
}

bool TextureProgram::queryInput()
{
    _input["uv"] = _attributeLocation("uv");
    _input["texture"] = _uniformLocation("texture");
    _input["texture_repeat"] = _uniformLocation("texture_repeat");
    _input["texture_alpha"] = _uniformLocation("texture_alpha");
    return AObjectProgram::queryInput();
}

bool TextureProgram::bindMaterial(Material *m)
{
    if (m == nullptr)
        return false;

    AObjectProgram::bindMaterial(m);
    _program->setUniformValue(_input["texture_repeat"], m->getTextureRepeat());
    _program->setUniformValue(_input["texture_alpha"], m->getTextureAlpha());
    QOpenGLTexture* texture = m->getTexture();
    if (texture != nullptr)
    {
        glActiveTexture(GL_TEXTURE1);
        texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
        texture->setMagnificationFilter(QOpenGLTexture::Linear);
        texture->bind();
        _program->setUniformValue(_input["texture"], 1);

        return true;
    }
    return false;
}

bool TextureProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

    glEnable(GL_TEXTURE_2D);

    glEnableVertexAttribArray(_input["uv"]);
    glVertexAttribPointer(_input["uv"], 2, GL_FLOAT, GL_FALSE, 0, g->uvs().data());

    AObjectProgram::renderGeometry(g);

    glDisableVertexAttribArray(_input["uv"]);

    glDisable(GL_TEXTURE_2D);

    return true;
}

}

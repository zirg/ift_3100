#include "pointprogram.h"

Program::PointProgram::PointProgram()
{
    _type = POINT;
}

Program::PointProgram::~PointProgram()
{

}

bool Program::PointProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

    //VBO BUFFER
    g->vboVertex()->bind();
    _program->enableAttributeArray(_input["position"]);
    _program->setAttributeBuffer(_input["position"], GL_FLOAT, 0, 3);
    g->vboVertex()->release();

    //Draw faces triangles
    glPointSize(2.0);
    g->vboIndex()->bind();
    glDrawElements(GL_POINTS, g->faces().size(), GL_UNSIGNED_INT, NULL);
    g->vboIndex()->release();

    _program->disableAttributeArray(_input["position"]);
    return true;
}

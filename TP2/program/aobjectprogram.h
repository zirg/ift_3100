#ifndef AOBJECTPROGRAM_H
#define AOBJECTPROGRAM_H

#include "aprogram.h"

#include "../geometry.h"
#include "../material.h"
#include "../light.h"
#include "../camera.h"

namespace Program {

class AObjectProgram : public AProgram
{
public:

    enum eProgram
    {
        UNDEFINED = 0,
        POINT,
        WIREFRAME,
        BASIC,
        LAMBERT,
        PHONG,
        TOON,
        TEXTURE,
        NORMAL_MAP,
        REFLECTIONMAP,
        EARTH,
        CUBEMAP
    };

    AObjectProgram(eProgram type);
    ~AObjectProgram();

    eProgram    getType() const;
    void        setType(eProgram type);

    virtual bool queryInput();
    virtual bool bindCamera(Camera *c);
    virtual bool bindWorld(const QMatrix4x4& );
    virtual bool bindMaterial(Material *m);
    virtual bool bindLight(std::list<Light *>& l);
    virtual bool renderGeometry(Geometry *g);
protected:
    virtual void    _queryLight(int idx);

protected:
    eProgram    _type;
    std::map< int, std::map<std::string, GLint> > _light_input;
};
}

#endif // AOBJECTPROGRAM_H


#include "earthprogram.h"

namespace Program {

EarthProgram::EarthProgram()
    : AObjectProgram(EARTH),
      _clouds(0.0)
{
}

bool EarthProgram::queryInput()
{
    _input["uv"] = _attributeLocation("uv");

    _input["Time"] = _uniformLocation("Time");

    _input["Map"] = _uniformLocation("Map");
    _input["Lights"] = _uniformLocation("Lights");
    _input["Spec"] = _uniformLocation("Spec");
    _input["Clouds"] = _uniformLocation("Clouds");
    _input["Bump"] = _uniformLocation("Bump");
    _input["Normal"] = _uniformLocation("Normal");
    return AObjectProgram::queryInput();
}

bool EarthProgram::bindLight(std::list<Light *>& l)
{
    if (l.size() == 0)
        return true;
    return AObjectProgram::bindLight(l);
}

bool EarthProgram::bindMaterial(Material *m)
{
    if (m == nullptr)
        return false;

    AObjectProgram::bindMaterial(m);
    std::vector<QOpenGLTexture*> textures(m->getTextures().rbegin(), m->getTextures().rend());

    glActiveTexture(GL_TEXTURE0);
    textures[0]->bind(_input["Map"]);
    _program->setUniformValue(_input["Map"], _input["Map"]);

    glActiveTexture(GL_TEXTURE1);
    textures[1]->bind(_input["Lights"]);
    _program->setUniformValue(_input["Lights"], _input["Lights"]);

    glActiveTexture(GL_TEXTURE2);
    textures[2]->bind(_input["Spec"]);
    _program->setUniformValue(_input["Spec"], _input["Spec"]);

    glActiveTexture(GL_TEXTURE3);
    textures[3]->bind(_input["Clouds"]);
    _program->setUniformValue(_input["Clouds"], _input["Clouds"]);

    glActiveTexture(GL_TEXTURE4);
    textures[4]->bind(_input["Bump"]);
    _program->setUniformValue(_input["Bump"], _input["Bump"]);

    glActiveTexture(GL_TEXTURE5);
    textures[5]->bind(_input["Normal"]);
    _program->setUniformValue(_input["Normal"], _input["Normal"]);


    return true;
}

bool EarthProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

    _program->setUniformValue(_input["Time"], _clouds);
    _clouds += 0.001;

    glVertexAttribPointer(_input["uv"], 2, GL_FLOAT, GL_FALSE, 0, g->uvs().data());
    glEnableVertexAttribArray(_input["uv"]);

    AObjectProgram::renderGeometry(g);

    glDisableVertexAttribArray(_input["uv"]);
    return true;
}

}

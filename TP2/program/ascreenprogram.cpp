#include "ascreenprogram.h"

namespace Program {

AScreenProgram::AScreenProgram(eProgram type)
    : _type(type)
{
}

AScreenProgram::eProgram   AScreenProgram::getType() const
{
    return _type;
}

void    AScreenProgram::setType(eProgram type)
{
    _type = type;
}

}

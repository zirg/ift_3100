#include "basicprogram.h"

Program::BasicProgram::BasicProgram() :
    AObjectProgram(BASIC) {
}

Program::BasicProgram::~BasicProgram()
{

}

bool Program::BasicProgram::queryInput()
{
    _input["perspective_matrix"] = _uniformLocation("perspective_matrix");
    _input["mouvement_matrix"] = _uniformLocation("mouvement_matrix");
    _input["ambiant"] = _attributeLocation("ambiant");
    _input["transform"] = _uniformLocation("transform");
    _input["position"] = _attributeLocation("position");
    return true;
}

bool Program::BasicProgram::bindCamera(Camera *c)
{
    if (c == nullptr)
        return false;
    _program->setUniformValue(_input["perspective_matrix"], c->perspective());
    _program->setUniformValue(_input["mouvement_matrix"], c->position());
    return true;
}

bool Program::BasicProgram::bindWorld(const QMatrix4x4& mat4)
{
    _program->setUniformValue(_input["transform"], mat4);
    return true;
}

bool Program::BasicProgram::bindMaterial(Material *m)
{
    if (m == nullptr)
        return false;
   _program->setAttributeValue(_input["ambiant"], m->getAmbiant());
   return true;
}

bool Program::BasicProgram::bindLight(std::list<Light *>& l)
{
    if (l.size() == 0)
        return true; // does not support light
    return true;
}

bool Program::BasicProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr)
        return false;

    //VBO BUFFER
    g->vboVertex()->bind();
    _program->enableAttributeArray(_input["position"]);
    _program->setAttributeBuffer(_input["position"], GL_FLOAT, 0, 3);
    g->vboVertex()->release();

    //Draw faces triangles
    g->vboIndex()->bind();
    glDrawElements(GL_TRIANGLES, g->faces().size(), GL_UNSIGNED_INT, NULL);
    g->vboIndex()->release();

    _program->disableAttributeArray(_input["position"]);
    return true;
}

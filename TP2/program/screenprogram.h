#ifndef SCREENPROGRAM_H
#define SCREENPROGRAM_H

#include "ascreenprogram.h"

namespace Program {

class ScreenProgram : public AScreenProgram
{
public:

    ScreenProgram();

    ~ScreenProgram() = default;

    bool queryInput();

    bool render(QOpenGLFramebufferObject *fbo);
};

}

#endif // SCREENPROGRAM_H

#ifndef TEXTUREPROGRAM_H
#define TEXTUREPROGRAM_H

#include "aobjectprogram.h"

namespace Program {

class TextureProgram : public AObjectProgram
{
public:
    TextureProgram();

    ~TextureProgram() = default;

    bool queryInput();

    bool bindMaterial(Material *m);

    bool renderGeometry(Geometry *g);
};

}

#endif // TEXTUREPROGRAM_H

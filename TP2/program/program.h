#ifndef PROGRAM_H
#define PROGRAM_H

namespace Program {

class AProgram;
class PointProgram;
class LineProgram;
class BasicProgram;
class LambertProgram;
class PhongProgram;
class ToonProgram;
class TextureProgram;
class NormalMapProgram;
class ScreenProgram;
class CubeMapProgram;
class CubeMapReflectionProgram;
class EarthProgram;

}

#include "aprogram.h"
#include "basicprogram.h"
#include "lineprogram.h"
#include "pointprogram.h"
#include "lambertprogram.h"
#include "toonprogram.h"
#include "phongprogram.h"
#include "textureprogram.h"
#include "normalmapprogram.h"
#include "screenprogram.h"
#include "cubemapprogram.h"
#include "cubemapreflectionprogram.h"
#include "earthprogram.h"

#endif // PROGRAM_H

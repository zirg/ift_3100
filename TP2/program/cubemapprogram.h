#ifndef CUBEMAPPROGRAM_H
#define CUBEMAPPROGRAM_H

#include "aobjectprogram.h"

namespace Program {

class CubeMapProgram : public AObjectProgram
{
public:
    CubeMapProgram();

    ~CubeMapProgram() = default;

    bool queryInput();

    bool bindCamera(Camera *c);

    bool bindWorld(const QMatrix4x4& );

    virtual bool bindLight(std::list<Light *>& l);

    bool bindMaterial(Material *m);

    bool renderGeometry(Geometry *g);
};

}

#endif // CUBEMAPPROGRAM_H


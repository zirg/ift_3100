#include "cubemapreflectionprogram.h"

namespace Program {

CubeMapReflectionProgram::CubeMapReflectionProgram()
    : AObjectProgram(REFLECTIONMAP) {

    _texture = new QOpenGLTexture(QOpenGLTexture::TargetCubeMap);

    _texture->create();
    _texture->bind();

    const char * suffixes[] = { "right", "left", "top", "bottom", "back", "front" };
    GLuint targets[] = { GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                         GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                         GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                         GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                         GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                         GL_TEXTURE_CUBE_MAP_NEGATIVE_Z};
    for( int i = 0; i < 6; i++ ) {
        QString fileName = QString("../assets/skybox/") + suffixes[i] + ".jpg";
        QImage image(fileName);
        image = image.convertToFormat(QImage::Format_RGB888);
        glTexImage2D(targets[i], 0, GL_RGB, image.width(), image.height(),0, GL_RGB, GL_UNSIGNED_BYTE, image.bits());
    }

    _texture->release();
}

bool CubeMapReflectionProgram::bindCamera(Camera *c)
{
    _program->setUniformValue(_input["cam_pos"], c->getTranslation());
    return AObjectProgram::bindCamera(c);
}

bool CubeMapReflectionProgram::queryInput()
{
    _input["cam_pos"] = _uniformLocation("cam.position");
    _input["cube_texture"] = _uniformLocation("cube_texture");
    return AObjectProgram::queryInput();
}

bool CubeMapReflectionProgram::bindLight(std::list<Light *>&l) {
    if (l.size() == 0)
        return true;
    return AObjectProgram::bindLight(l);
}


bool CubeMapReflectionProgram::bindMaterial(Material */*m*/)
{
    _texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::ClampToEdge);
    _texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::ClampToEdge);
    _texture->setMinificationFilter(QOpenGLTexture::Linear);
    _texture->setMagnificationFilter(QOpenGLTexture::Linear);
    _texture->bind(_input["cube_texture"]);
    _program->setUniformValue(_input["cube_texture"], _input["cube_texture"]);

    return true;
}

bool CubeMapReflectionProgram::renderGeometry(Geometry *g)
{
    glEnable(GL_TEXTURE_2D);

    AObjectProgram::renderGeometry(g);

    glDisable(GL_TEXTURE_2D);
    return true;
}

}


#ifndef ToonPROGRAM_H
#define ToonPROGRAM_H

#include <iostream>
#include <list>
#include <map>

#include "aobjectprogram.h"

namespace Program {

class ToonProgram : public AObjectProgram
{
public:
    ToonProgram();
    ~ToonProgram();

    bool bindCamera(Camera *c);
    bool bindMaterial(Material *m);
    bool bindWorld(const QMatrix4x4& mat4);
    bool bindLight(std::list<Light *>& l);
    bool renderGeometry(Geometry *g);
};

}
#endif // ToonPROGRAM_H

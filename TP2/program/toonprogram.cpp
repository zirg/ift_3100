#include "toonprogram.h"

Program::ToonProgram::ToonProgram() :
    AObjectProgram(TOON) {

}

Program::ToonProgram::~ToonProgram()
{

}

bool Program::ToonProgram::bindCamera(Camera *c)
{
    return AObjectProgram::bindCamera(c);
}

bool Program::ToonProgram::bindWorld(const QMatrix4x4& mat4)
{
    return AObjectProgram::bindWorld(mat4);
}

bool Program::ToonProgram::bindMaterial(Material *m)
{
    return AObjectProgram::bindMaterial(m);
}

bool Program::ToonProgram::bindLight(std::list<Light *>& l)
{
    return AObjectProgram::bindLight(l);
}

bool Program::ToonProgram::renderGeometry(Geometry *g)
{
    return AObjectProgram::renderGeometry(g);
}

#ifndef PHONGPROGRAM_H
#define PHONGPROGRAM_H

#include <iostream>
#include <list>
#include <map>

#include "aobjectprogram.h"

namespace Program {

class PhongProgram : public AObjectProgram
{
public:
    PhongProgram();
    ~PhongProgram();

    bool queryInput();
    bool bindCamera(Camera *c);
    bool bindMaterial(Material *m);
    bool bindWorld(const QMatrix4x4& mat4);
    bool bindLight(std::list<Light *>& l);
    bool renderGeometry(Geometry *g);
};

}
#endif // PHONGPROGRAM_H

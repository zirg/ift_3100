
#ifndef EARTH_PROGRAM_H
#define EARTH_PROGRAM_H

#include "aobjectprogram.h"

namespace Program
{

class EarthProgram : public AObjectProgram
{
public:
    EarthProgram();

    ~EarthProgram() = default;

    bool queryInput();

    bool bindMaterial(Material *m);

    bool bindLight(std::list<Light *>& l);

    bool renderGeometry(Geometry *g);

private:
    float _clouds;
};

}

#endif // EARTH_PROGRAM_H

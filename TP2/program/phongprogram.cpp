#include "phongprogram.h"

Program::PhongProgram::PhongProgram() :
    AObjectProgram(PHONG) {

}

Program::PhongProgram::~PhongProgram()
{

}
bool Program::PhongProgram::queryInput()
{
    _input["cam_pos"] = _uniformLocation("cam.position");
    _input["specular_color"] = _uniformLocation("mat.specular_color");
    _input["shine"] = _uniformLocation("mat.shineness");
    return AObjectProgram::queryInput();
}

bool Program::PhongProgram::bindCamera(Camera *c)
{
    _program->setUniformValue(_input["cam_pos"], c->getTranslation());
    return AObjectProgram::bindCamera(c);
}

bool Program::PhongProgram::bindWorld(const QMatrix4x4& mat4)
{
    return AObjectProgram::bindWorld(mat4);
}

bool Program::PhongProgram::bindMaterial(Material *m)
{
    _program->setUniformValue(_input["specular_color"], m->getSpecular());
    _program->setUniformValue(_input["shine"], m->getShineness());
    return AObjectProgram::bindMaterial(m);
}

bool Program::PhongProgram::bindLight(std::list<Light *>& l)
{
    return AObjectProgram::bindLight(l);
}

bool Program::PhongProgram::renderGeometry(Geometry *g)
{
    return AObjectProgram::renderGeometry(g);
}


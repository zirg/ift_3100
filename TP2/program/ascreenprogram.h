#ifndef ASCREENPROGRAM_H
#define ASCREENPROGRAM_H

#include <QOpenGLFramebufferObject>

#include "aprogram.h"

namespace Program {

class AScreenProgram : public AProgram
{
public:

    enum eProgram
    {
        DEFAULT = 0,
        BLACKnWHITE,
        BLUR,
        BLOOM,
        FISHEYE
    };

    AScreenProgram(const eProgram type);

    ~AScreenProgram() = default;

    eProgram    getType() const;

    void        setType(const eProgram type);

    virtual bool render(QOpenGLFramebufferObject *fbo) = 0;

protected:
    eProgram    _type;
};

}

#endif // ASCREENPROGRAM_H

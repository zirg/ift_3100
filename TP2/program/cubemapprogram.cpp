#include "cubemapprogram.h"

namespace Program {

CubeMapProgram::CubeMapProgram()
    : AObjectProgram(CUBEMAP) {
}

bool CubeMapProgram::queryInput()
{
    _input["position"] = _attributeLocation("position");

    _input["perspective_matrix"] = _uniformLocation("perspective_matrix");
    _input["mouvement_matrix"] = _uniformLocation("mouvement_matrix");
    _input["transform_matrix"] = _uniformLocation("transform_matrix");
    _input["cube_texture"] = _uniformLocation("cube_texture");
    return true;
}

bool CubeMapProgram::bindCamera(Camera *c)
{
    if (c != nullptr) {
        _program->setUniformValue(_input["perspective_matrix"], c->perspective());
        _program->setUniformValue(_input["mouvement_matrix"], c->position());
        return true;
    }
    return false;
}

bool CubeMapProgram::bindWorld(const QMatrix4x4& w)
{
    _program->setUniformValue(_input["transform_matrix"], w);
    return true;
}

bool CubeMapProgram::bindMaterial(Material *m)
{
    if (m == nullptr)
        return false;

    QOpenGLTexture* texture = m->getTexture();
    if (texture != nullptr)
    {
        texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::ClampToEdge);
        texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::ClampToEdge);
        texture->setMinificationFilter(QOpenGLTexture::Linear);
        texture->setMagnificationFilter(QOpenGLTexture::Linear);
        texture->bind(_input["cube_texture"]);
        _program->setUniformValue(_input["cube_texture"], _input["cube_texture"]);
        return true;
    }
    return false;
}

bool CubeMapProgram::bindLight(std::list<Light *>&)
{
    return true;
}

bool CubeMapProgram::renderGeometry(Geometry *g)
{
    if (g == nullptr) return false;

    glEnable(GL_TEXTURE_2D);

    g->vboVertex()->bind();
    _program->setAttributeBuffer(_input["position"], GL_FLOAT, 0, 3);
    _program->enableAttributeArray(_input["position"]);
    g->vboVertex()->release();

    //Draw faces triangles
    g->vboIndex()->bind();
    glDrawElements(GL_TRIANGLES, g->faces().size(), GL_UNSIGNED_INT, NULL);
    g->vboIndex()->release();

    glDisable(GL_TEXTURE_2D);
    _program->disableAttributeArray(_input["position"]);
    return true;
}

}

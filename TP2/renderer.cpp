#include "renderer.h"

Renderer::Renderer()
    : _takeSreenshot(false),
      _promiseSreenshot(new std::promise<QImage>())
{
    QMatrix4x4    m;
    m.setToIdentity();
    _matrix.push(m);
}

bool Renderer::init()
{
    initializeOpenGLFunctions();

    Program::AObjectProgram *p;
    p = new Program::PointProgram();
    p->addVertexShader("./GLSL/flat_shader.vert");
    p->addFragmentShader("./GLSL/flat_shader.frag");
    p->link();
    _progObject[Material::POINT_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::LineProgram();
    p->addVertexShader("./GLSL/flat_shader.vert");
    p->addFragmentShader("./GLSL/flat_shader.frag");
    p->link();
    _progObject[Material::LINE_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::BasicProgram();
    p->addVertexShader("./GLSL/flat_shader.vert");
    p->addFragmentShader("./GLSL/flat_shader.frag");
    p->link();
    _progObject[Material::BASE_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::LambertProgram();
    p->addVertexShader("./GLSL/lambert.vert");
    p->addFragmentShader("./GLSL/lambert.frag");
    p->link();
    _progObject[Material::LAMBERT_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::PhongProgram();
    p->addVertexShader("./GLSL/phong.vert");
    p->addFragmentShader("./GLSL/phong.frag");
    p->link();
    _progObject[Material::PHONG_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::ToonProgram();
    p->addVertexShader("./GLSL/toon.vert");
    p->addFragmentShader("./GLSL/toon.frag");
    p->link();
    _progObject[Material::TOON_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::TextureProgram();
    p->addVertexShader("./GLSL/texture_shader.vert");
    p->addFragmentShader("./GLSL/texture_shader.frag");
    p->link();
    _progObject[Material::TEXTURE_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::NormalMapProgram();
    p->addVertexShader("./GLSL/normalmap_shader.vert");
    p->addFragmentShader("./GLSL/normalmap_shader.frag");
    p->link();
    _progObject[Material::NORMAL_MAP_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::CubeMapProgram();
    p->addVertexShader("./GLSL/cubemap_shader.vert");
    p->addFragmentShader("./GLSL/cubemap_shader.frag");
    p->link();
    _progObject[Material::CUBE_MAP_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::CubeMapReflectionProgram();
    p->addVertexShader("./GLSL/cubemapreflection_shader.vert");
    p->addFragmentShader("./GLSL/cubemapreflection_shader.frag");
    p->link();
    _progObject[Material::CUBE_MAP_REFLECTION] = p;
    std::cout << glGetError() << std::endl;

    p = new Program::EarthProgram();
    p->addVertexShader("./GLSL/earth_shader.vert");
    p->addFragmentShader("./GLSL/earth_shader.frag");
    p->link();
    _progObject[Material::EARTH_MATERIAL] = p;
    std::cout << glGetError() << std::endl;

    Program::AScreenProgram *screen = new Program::ScreenProgram();
    screen->addVertexShader("./GLSL/screen_shader.vert");
    screen->addFragmentShader("./GLSL/screen_shader.frag");
    screen->link();
    _progScreen[Program::AScreenProgram::DEFAULT] = screen;
    std::cout << glGetError() << std::endl;

    screen = new Program::ScreenProgram();
    screen->addVertexShader("./GLSL/blacknwhite_shader.vert");
    screen->addFragmentShader("./GLSL/blacknwhite_shader.frag");
    screen->link();
    _progScreen[Program::AScreenProgram::BLACKnWHITE] = screen;
    std::cout << glGetError() << std::endl;

    screen = new Program::ScreenProgram();
    screen->addVertexShader("./GLSL/blur_shader.vert");
    screen->addFragmentShader("./GLSL/blur_shader.frag");
    screen->link();
    _progScreen[Program::AScreenProgram::BLUR] = screen;
    std::cout << glGetError() << std::endl;

    screen = new Program::ScreenProgram();
    screen->addVertexShader("./GLSL/bloom_shader.vert");
    screen->addFragmentShader("./GLSL/bloom_shader.frag");
    screen->link();
    _progScreen[Program::AScreenProgram::BLOOM] = screen;
    std::cout << glGetError() << std::endl;

    screen = new Program::ScreenProgram();
    screen->addVertexShader("./GLSL/fisheye_shader.vert");
    screen->addFragmentShader("./GLSL/fisheye_shader.frag");
    screen->link();
    _progScreen[Program::AScreenProgram::FISHEYE] = screen;
    std::cout << glGetError() << std::endl;

    // frame buffer object
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    format.setMipmap(true);
    format.setTextureTarget(GL_TEXTURE_2D);
    format.setInternalTextureFormat(GL_RGBA8);
     _sampledFbo = new QOpenGLFramebufferObject(682, 582, format);
    format.setSamples(16);
    _fbo = new QOpenGLFramebufferObject(682, 582, format);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "frame buffer error" << std::endl;

    glClearColor(0.2, 0.2, 0.2, 1.0);
    glEnable(GL_DEPTH_TEST);

    return true;
}

QImage Renderer::capture()
{
    _takeSreenshot = true;
    std::future<QImage> image = _promiseSreenshot->get_future();
    image.wait();
    delete _promiseSreenshot;
    _promiseSreenshot = new std::promise<QImage>();
    return image.get();
}

void    Renderer::_resizeBuffers(const int width, const int height)
{
    if (_fbo == nullptr
            || _fbo->width() != width
            || _fbo->height() != height)
    {
        if (_fbo != nullptr) {
            _fbo->release();
            delete _fbo;
            _fbo = nullptr;
        }

        if (_sampledFbo != nullptr) {
            _sampledFbo->release();
            delete _sampledFbo;
            _sampledFbo = nullptr;
        }

        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
        format.setMipmap(true);
        format.setTextureTarget(GL_TEXTURE_2D);
        format.setInternalTextureFormat(GL_RGBA8);
        _fbo = new QOpenGLFramebufferObject(width, height, format);
        format.setSamples(16);
        _sampledFbo = new QOpenGLFramebufferObject(width, height, format);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "frame buffer error" << std::endl;

    }
}

bool Renderer::render(Scene *scene)
{
    static int frame;
    if (++frame % 2) {
        Camera *c = scene->getCamera();
        _resizeBuffers(c->width(), c->height());

        _sampledFbo->bind();
        _renderObjects3D(scene);
        _sampledFbo->release();
    } else {
        _renderScreen(scene);
        if (_takeSreenshot == true) {
            _promiseSreenshot->set_value(_sampledFbo->toImage());
            _takeSreenshot = false;
        }
    }
    return true;
}

bool    Renderer::_renderScreen(Scene *scene)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    Camera *c = scene->getCamera();
    if (c == nullptr)
        return false;

    glViewport(0, 0, c->width(), c->height());

    Program::AScreenProgram *screen = _progScreen[scene->getScreenEffect()];

    screen->bind();
    QOpenGLFramebufferObject::blitFramebuffer(_fbo, _sampledFbo, GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT| GL_STENCIL_BUFFER_BIT, GL_NEAREST);
    screen->render(_fbo);
    screen->release();

    return true;
}

bool    Renderer::_renderObjects3D(Scene *scene)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    Camera *c = scene->getCamera();
    std::list<Light *>& l = scene->getLight();
    std::list<Object3D *>& objs = scene->getObject();

    if (c == nullptr)
        return false;

    glViewport(0, 0, c->width(), c->height());

    if (scene->cubeMapIsVisible()) {
        _renderObject3D(c, l, scene->getCubeMap());
    }

    if (scene->gridIsVisible()) {
        _renderObject3D(c, l, scene->getGrid());
    }

    for (std::list<Object3D *>::iterator it = objs.begin();it != objs.end(); ++it)
    {
        _renderObject3D(c, l, *it);
    }
    return true;
}

bool    Renderer::_renderObject3D(Camera *cam, std::list<Light *>& light, Object3D * obj)
{
    //Add current transformation

    if (obj) {
        _pushMatrix(obj->position());

        //Render geometry
        for (std::list<Geometry *>::iterator geo = obj->getGeometry().begin();
             geo != obj->getGeometry().end(); ++geo)
        {
            Material *m = obj->getMaterial((*geo)->getMaterialName());
            if (m != nullptr)
            {
                _renderGeometry(cam, light, *geo, m);

                // release progs once displayed
                _progObject[m->getType()]->release();
                std::list<QOpenGLTexture*> textures = m->getTextures();
                for (auto it = textures.begin(); it != textures.end(); ++it) {
                    QOpenGLTexture* texture = *it;
                    if (texture != nullptr) {
                        texture->release();
                    }
                }
                if (m->getTexture() != nullptr)
                    m->getTexture()->release();
                if (m->getNormalMap() != nullptr)
                    m->getNormalMap()->release();

            }
        }

        //Recursive to render child objects
        for (std::list<Object3D *>::iterator child = obj->getChildren().begin();
             child != obj->getChildren().end(); ++child)

        {
            _renderObject3D(cam, light, *child);
        }
    }

    //Remove current transformations
    _popMatrix();
    return true;
}

bool    Renderer::_renderGeometry(Camera *cam, std::list<Light *>& l,
                                  Geometry *g, Material *m)
{
    //if (g->isGenerated() == false) {
        g->destroy();
        g->compile();
    //}

    Program::AObjectProgram *p =_progObject[m->getType()];

    _bindProgram(m->getType());
    if (p->bindCamera(cam) == false)
        return false;
    if (p->bindLight(l) == true)
    {
        if (p->bindMaterial(m) == false)
            return false;
        if (p->bindWorld(_matrix.top()) == false)
            return false;
        if (p->renderGeometry(g) == false)
            return false;
    }
    return true;
}

void    Renderer::_bindProgram(Material::eMaterial type)
{
    _progObject[type]->bind();
}

void    Renderer::_pushMatrix(const QMatrix4x4& mat4)
{
    QMatrix4x4 newMat = _matrix.top() * mat4;
    _matrix.push(newMat);
}

void    Renderer::_popMatrix()
{
    if (_matrix.size() > 1)
    {
        _matrix.pop();
    }
}

#ifndef TREESERIALIZED_H
#define TREESERIALIZED_H

#include <QTreeWidget>
#include <object3d.h>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "light.h"
#include "scene.h"
#include "objectUiSettings/objectcontainer.h"

class GraphTreeView;
class TreeSerialized {
   private:
      QString _path;
      GraphTreeView *_root;
      QTreeWidgetItem *_scene;
      QXmlStreamWriter *_xmlWriter;
      QXmlStreamReader *_xmlReader;
      objectUiSettings::ObjectContainer _container;
      std::list<Movable *> _listObjLoad;

      typedef void (TreeSerialized::*pfManip)(Object3D *);
      typedef std::pair<pfManip, pfManip> objmanip_t;
      std::map<QString, objmanip_t> _objParse;
   public:
     TreeSerialized(GraphTreeView *root);
     void toXML(const char *path);
     const std::list<Movable *> &fromXML(const char *path);
   private:
     void _sceneToXML(Scene *);
     void _materialToXML(Material *mat);
     void _cameraToXML(Camera *);
     void _lightToXML(Light *light);
     void _itemToXML(QTreeWidgetItem *item);
     void _XMLToForm(QTreeWidgetItem *parent);
     void _XMLToLight(QTreeWidgetItem *parent);
     void _XMLToLightOptions(Light *light);
     void _XMLToScene(Scene *scene);
     void _XMLToMaterial(Material *mat);
     void _XMLToCamera(Camera *);
     void _XMLToCameraOptions(Camera *);

   private:
     void coneToXML(Object3D *);
     void XMLtoCone(Object3D *);
     void cylinderToXML(Object3D *);
     void XMLtoCylinder(Object3D *);
     void kochsnowflakeToXML(Object3D *);
     void XMLtoKochsnowflake(Object3D *);
     void torusToXML(Object3D *);
     void XMLtoTorus(Object3D *);
     void planeToXML(Object3D *);
     void XMLtoPlane(Object3D *);
     void bezierToXML(Object3D *);
     void XMLtoBezier(Object3D *);
     void sphereToXML(Object3D *);
     void XMLtoSphere(Object3D *);
};

#endif // TREESERIALIZED

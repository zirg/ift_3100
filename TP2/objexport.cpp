#include <fstream>

#include "geometry.h"
#include "objexport.h"

ObjExport::ObjExport(Object3D *obj) {
    _lobj.push_back(obj);
}

ObjExport::ObjExport(const std::list<Object3D *> &lobj)
    : _lobj(lobj) {

}

void ObjExport::_exportObject(std::ostream &ostream, Object3D *obj) {
    Geometry *geo = obj->getGeometry().front();
    std::vector<float> vertices = geo->vertices();
    for (unsigned int i = 0; i < vertices.size(); i += 3) {
        ostream << "v " << vertices[i] << " " << vertices[i + 1] << " " << vertices[i + 2] << std::endl;
    }
    std::vector<float> uvs = geo->uvs();
    if (!uvs.empty()) {
        for (unsigned int i = 0; i < uvs.size(); i += 2) {
            ostream << "vt " << uvs[i] << " " << uvs[i + 1] << std::endl;
        }
    }
    std::vector<float> normals = geo->normals();
    for (unsigned int i = 0; i < normals.size(); i += 3) {
        ostream << "vn " << normals[i] << " " << normals[i + 1] << " " << normals[i + 2] << std::endl;
    }

    ostream << "s off" << std::endl;
    std::vector<int> faces = geo->faces();
    for (unsigned int i = 0; i < faces.size(); i += 3) {
        ostream << "f";
        for (unsigned int j = 0; j < 3; j++) {
            ostream << " " << _countFaces + i + j + 1;
            if (uvs.empty() && !normals.empty()) {
                ostream << "//" << _countFaces + i + j + 1;
            } else if (!uvs.empty() && normals.empty()) {
                ostream << "/" << _countFaces + i + j + 1;
            } else {
                ostream << "/" << _countFaces + i + j + 1 << "/" << _countFaces + i + j + 1;
            }
        }
        ostream << std::endl;
    }
    _countFaces += geo->faces().size();
}

void ObjExport::save(const std::string &path) {
    std::fstream onfile(path, std::fstream::out | std::fstream::trunc);
    if (onfile.fail()) {
        std::cout << "Failed to export \"" << path << "\"" << std::endl;
    }
    else {
        Object3D *obj;
        _countFaces = 0;
        while (!_lobj.empty()) {
            obj = _lobj.front();
            _lobj.pop_front();
            _exportObject(onfile, obj);
        }
        onfile.close();
    }
}

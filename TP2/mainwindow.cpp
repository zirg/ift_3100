#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "objectUiSettings/objectDescriptor/objectloaderdescription.h"
#include "objectUiSettings/iobjectdescriptor.h"
#include "objectUiSettings/sceneconfiguration.h"
#include "objectUiSettings/config.d.h"

#include <thread>
#include <QIcon>
#include <QMessageBox>
#include <QCheckBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _fileMenu(nullptr),
    _quitAction(nullptr),
    _saveAction(nullptr),
    _saveAsAction(nullptr),
    _openAction(nullptr),
    _newMenu(nullptr),
    _newObjectMenu(nullptr),
    _newLightMenu(nullptr)
{
    ui->setupUi(this);
    this->initObjectSettings();
    this->createMenuBar();
    QObject::connect(this->ui->graph, SIGNAL(itemSelectionChanged()), this, SLOT(itemChanged()));
    this->ui->settingsLayout->setAlignment(Qt::AlignTop);
    this->ui->graph->setEventObject(this);
    this->ui->graph->init(this->ui->wGl->getScene(), this->ui->wGl->getScene()->getCamera());
    this->ui->infoLabel->setWordWrap(true);
    this->ui->infoLabel->hide();
    this->initDefaultObject();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_O)
    {
        QString objectType = "Loader";
        this->addObject(objectType);
    }
    else
    {
        QString type;
        Movable *movable = this->ui->graph->getCurrentMovable(type);
        if (movable != NULL && type != "Scene" && type != "Camera")
        {
            this->transformationByKey.transformMovableByKey(event, movable);
            this->refreshContentTabSetting();
        }
    }
}

void MainWindow::on_pushButton_clicked()
{
    //this->ui->wGl->takeScreenShot(this->ui->filename->text().toUtf8().constData());
}

void MainWindow::itemChanged()
{
    //this->_newObjectMenu->setEnabled(this->ui->graph->isObject3D());
}

void MainWindow::onNewSceneClicked() {
    if (!this->ui->graph->isEmpty()) {
        QMessageBox msgBox;
        msgBox.setText("Are you sure");
        msgBox.setInformativeText("Do you want save the scene ?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Yes) onSaveSceneClicked();
        if (ret == QMessageBox::No || ret == QMessageBox::Yes) {
            this->clearContentTabSetting();
            _currentPathSave.clear();
            this->ui->graph->reset();
            this->ui->wGl->clear();
            QString light("Ambient");
            this->addLight(light);
        }
    }
    Scene *scene = this->ui->wGl->getScene();
    scene->reset();
    scene->getCamera()->reset();
}

void MainWindow::onSaveSceneClicked() {
    if (!_currentPathSave.isEmpty()) {
        this->ui->graph->toXML(_currentPathSave.toLocal8Bit());
    } else onSaveAsSceneClicked();
}

void MainWindow::onSaveAsSceneClicked() {
    _currentPathSave = QFileDialog::getSaveFileName(NULL, tr("Scene file"), ".", "Scene Files(*.xml)");
    if (!_currentPathSave.isEmpty()) {
        if (!_currentPathSave.endsWith(".xml"))
            _currentPathSave += ".xml";
        this->ui->graph->toXML(_currentPathSave.toLocal8Bit());
        this->setWindowTitle("IFT-3100 TP2 - " + _currentPathSave.mid(_currentPathSave.lastIndexOf('/') + 1));
    }
}

void MainWindow::onExportAsClicked() {
    _currentPathSave = QFileDialog::getSaveFileName(NULL, tr("Export"), ".", "Images (*.png *.bmp)");
    if (!_currentPathSave.isEmpty()) {
        if (!_currentPathSave.endsWith(".png") && !_currentPathSave.endsWith(".bmp"))
            _currentPathSave += ".png";
        this->ui->wGl->takeScreenShot(_currentPathSave.toLocal8Bit());
    }
}

void MainWindow::onOpenSceneClicked() {

    int ret = -1;
    if (!this->ui->graph->isEmpty()) {
        QMessageBox msgBox;
        msgBox.setText("Are you sure");
        msgBox.setInformativeText("Do you want save the scene ?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Yes);
        ret = msgBox.exec();
        if (ret == QMessageBox::Yes) onSaveSceneClicked();
        if (ret == QMessageBox::No || ret == QMessageBox::Yes) {
            this->clearContentTabSetting();
            _currentPathSave.clear();
            this->ui->graph->reset();
            this->ui->wGl->clear();
            this->ui->wGl->getScene()->reset();
            this->ui->wGl->getScene()->getCamera()->reset();
            if (ret == QMessageBox::No) {
                QString light("Ambient");
                this->addLight(light);
            }
        }
    }

    if (ret == QMessageBox::Yes || this->ui->graph->isEmpty()) {
        QString path = QFileDialog::getOpenFileName(NULL, tr("Scene file"), ".", "Scene Files(*.xml)");
        _currentPathSave = path;
        if (!path.isEmpty()) {
            this->ui->graph->reset();
            this->ui->wGl->clear();
            this->clearContentTabSetting();
                this->ui->graph->fromXML(_currentPathSave.toLocal8Bit());
            this->setWindowTitle("IFT-3100 TP2 - " + path.mid(path.lastIndexOf('/') + 1));
            objectUiSettings::SceneConfiguration *sceneConfiguration = objectUiSettings::SceneConfiguration::Create();
            sceneConfiguration->init(this->ui->wGl->getScene());
            this->setContentTabSetting(sceneConfiguration);
        }
    }
}

void MainWindow::onQuitClicked() {
    if (!this->ui->graph->isEmpty()) {
        QMessageBox msgBox;
        msgBox.setText("Are you sure");
        msgBox.setInformativeText("Do you want save the scene ?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Yes) onSaveSceneClicked();
        if (ret != QMessageBox::Cancel) close();
    }
    else close();
}

void MainWindow::onAddObjectClicked()
{
    QAction *action = (QAction *)QObject::sender();
    QString objectType = action->data().toString();

    this->addObject(objectType);
}

void MainWindow::onAddLightClicked()
{
    QAction *action = (QAction *)QObject::sender();
    QString type = action->data().toString();
    this->addLight(type);
}

void MainWindow::createMenuBar()
{
    QMenuBar *menuBar = this->menuBar();

    this->_fileMenu = menuBar->addMenu(tr("&File"));
    this->_newAction = this->_fileMenu->addAction(tr("&New"));
    QObject::connect(this->_newAction, SIGNAL(triggered()), this, SLOT(onNewSceneClicked()));
    this->_openAction = this->_fileMenu->addAction(tr("&Open"));
    QObject::connect(this->_openAction, SIGNAL(triggered()), this, SLOT(onOpenSceneClicked()));
    this->_fileMenu->addSeparator();
    this->_saveAction = this->_fileMenu->addAction(tr("&Save"));
    QObject::connect(this->_saveAction, SIGNAL(triggered()), this, SLOT(onSaveSceneClicked()));
    this->_saveAsAction = this->_fileMenu->addAction(tr("&Save As"));
    QObject::connect(this->_saveAsAction, SIGNAL(triggered()), this, SLOT(onSaveAsSceneClicked()));
    this->_fileMenu->addSeparator();
    this->_saveAsAction = this->_fileMenu->addAction(tr("&Export As"));
    QObject::connect(this->_saveAsAction, SIGNAL(triggered()), this, SLOT(onExportAsClicked()));
    this->_fileMenu->addSeparator();
    this->_quitAction = this->_fileMenu->addAction(tr("&Quit"));
    QObject::connect(this->_quitAction, SIGNAL(triggered()), this, SLOT(onQuitClicked()));


    this->_newMenu = menuBar->addMenu(tr("&Add"));
    this->_newObjectMenu = this->_newMenu->addMenu(tr("&Object"));

    QStringList::const_iterator it;
    for (it = this->_container.getObjectType().begin(); it != this->_container.getObjectType().end(); ++it) {
        QString type = *(it);
        QAction *action = this->_newObjectMenu->addAction(tr(type.toStdString().c_str()));
        action->setData(type);
        QObject::connect(action, SIGNAL(triggered()), this, SLOT(onAddObjectClicked()));
        this->_objects.push_back(action);
    }
    this->_newLightMenu = this->_newMenu->addMenu(tr("&Light"));

    for (int i = 0; i < objectUiSettings::gtotal_light_config; ++i) {
        QString type = objectUiSettings::gLight_config[i].name;
        QAction *action = this->_newLightMenu->addAction(tr(type.toStdString().c_str()));
        action->setData(type);
        QObject::connect(action, SIGNAL(triggered()), this, SLOT(onAddLightClicked()));
        this->_lights.push_back(action);
    }
}

void MainWindow::addObject(QString &objectType)
{
    if (this->ui->wGl->getScene()->empty() && objectType != "Light" &&
            this->ui->wGl->getScene()->getLight().size() == 0) {
        QMessageBox msgBox;
        msgBox.setText("Light");
        msgBox.setInformativeText("Do you want to add a light too ?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Yes) {
            objectUiSettings::LightDescription *ldesc = dynamic_cast<objectUiSettings::LightDescription *>(objectUiSettings::LightDescription::Create());
            this->ui->wGl->addMovableToScene(ldesc->getWrappedObject()->getObject());
            ldesc->setType(Light::AMBIANT);
            this->ui->graph->addObject(ldesc->getWrappedObject(),
                                       QString::fromStdString(ldesc->getLightType().toStdString() + " " +
                                                              ldesc->getName().toStdString()), true);
        }
    }


    objectUiSettings::IObjectDescription *settingView = this->_container.getSettingView(objectType);
    if (settingView != NULL) {
        objectUiSettings::IWrapper *wrappedObject = settingView->getWrappedObject();
        if (settingView->getType() == "Loader") {
            objectUiSettings::ObjectLoaderDescription *objectLoadDesc = dynamic_cast<objectUiSettings::ObjectLoaderDescription *>(settingView);
            this->ui->graph->addObjectLoadRef(wrappedObject, objectLoadDesc->getPath());
        }
        if (wrappedObject != NULL && wrappedObject->getObject() != NULL) {
            this->setContentTabSetting(settingView);
            this->ui->graph->addObject(wrappedObject, QString(settingView->getName()));
            return;
        }
    }
}

void MainWindow::addObject(QString &objectType, Movable *movable)
{
    objectUiSettings::IWrapper *wrapper = this->_container.getObjectWrapper(objectType, movable);
    if (wrapper != NULL && wrapper->getObject() != NULL) {
        this->ui->graph->addObject(wrapper, QString(objectType));
        return;
    }
}

void MainWindow::addLight(QString &type)
{
    Light::eLight lightType = this->convertStringToLightType(type);
    objectUiSettings::LightDescription *desc =
            new objectUiSettings::LightDescription();
    desc->setType(lightType);
    desc->init(false);
    objectUiSettings::IWrapper *wrappedObject = desc->getWrappedObject();
    this->setContentTabSetting(desc);
    this->ui->graph->addObject(wrappedObject,
                               QString::fromStdString(type.toStdString() + " " +
                                                      desc->getName().toStdString()), true);

}

void MainWindow::addLight(QString &type, Movable *light)
{
    Light::eLight lightType = this->convertStringToLightType(type);
    objectUiSettings::LightDescription *desc =
            new objectUiSettings::LightDescription(NULL, light);
    desc->setType(lightType);
    desc->init(false);
    objectUiSettings::IWrapper *wrappedObject = desc->getWrappedObject();
    this->ui->graph->addObject(wrappedObject,
                               QString::fromStdString(desc->getLightType().toStdString() + " " +
                                                      desc->getName().toStdString()), true);
}

void MainWindow::onItemDeleted(objectUiSettings::IWrapper *wrapper)
{
    this->clearContentTabSetting();
    this->ui->wGl->removeMovableToScene(wrapper->getObject());
    //delete wrapper->getObject();
}

void MainWindow::onItemEdited(objectUiSettings::IWrapper *wrapper)
{
    if (wrapper->getObjectType() == "Scene") {
        objectUiSettings::SceneConfiguration *sceneConfiguration = objectUiSettings::SceneConfiguration::Create();
        sceneConfiguration->init((Scene *)wrapper->getObject());
        this->setContentTabSetting(sceneConfiguration);
        return;
    }
    objectUiSettings::IObjectDescription *settingView = this->_container.getSettingView(wrapper->getObjectType(), true);
    if (settingView != NULL) {
        this->setContentTabSetting(settingView);
        settingView->setObjectSettingInformation(wrapper);
        settingView->setCamera(this->ui->wGl->getScene()->getCamera());
        return;
    }
    QMessageBox::information(this, tr("Information"), tr("Object cannot be created."));
}

void MainWindow::onItemAdded(objectUiSettings::IWrapper *wrapper, bool add)
{
    Movable *obj = wrapper->getObject();
    if (add)
        this->ui->wGl->addMovableToScene(obj);
    objectUiSettings::IObjectDescription *settingView = this->_container.getSettingView(wrapper->getObjectType(), true);
    if (settingView != NULL) {
        this->setContentTabSetting(settingView);
        settingView->setObjectSettingInformation(wrapper);
        settingView->setCamera(this->ui->wGl->getScene()->getCamera());
        return;
    }
}

void MainWindow::initObjectSettings()
{
    for (int i = 0; i < objectUiSettings::gtotal_object_config; ++i) {
        this->_container.registerObject(QString(objectUiSettings::gObjects_config[i].type.c_str()),
                                        objectUiSettings::gObjects_config[i].create,
                                        objectUiSettings::gObjects_config[i].etype == objectUiSettings::object_type::OBJECT);
    }
}

void MainWindow::setContentTabSetting(QWidget *widget)
{
    this->clearContentTabSetting();
    QLayout *layout = this->ui->settingsLayout;
    layout->addWidget(widget);
}

void MainWindow::clearContentTabSetting()
{
    QLayout *layout = this->ui->settingsLayout;
    QLayoutItem *layoutItem = layout->takeAt(0);
    if (layoutItem != NULL) {
        QWidget *previous = layoutItem->widget();
        if (previous != NULL) {
            delete previous;
        }
    }
}

void MainWindow::refreshContentTabSetting()
{
    objectUiSettings::IObjectDescription *descriptor;
    QLayout *layout = this->ui->settingsLayout;
    QLayoutItem *item = layout->takeAt(0);
    descriptor = reinterpret_cast<objectUiSettings::IObjectDescription *>(item->widget());
    if (descriptor != NULL)
    {
        descriptor->refreshDescription();
        layout->addWidget(descriptor);
    }
}

Light::eLight MainWindow::convertStringToLightType(const QString &type)
{
    for (int i = 0; i < objectUiSettings::gtotal_light_config; ++i) {
        if (objectUiSettings::gLight_config[i].name == type) {
            return objectUiSettings::gLight_config[i].type;
        }
    }
    return Light::AMBIANT;
}

void MainWindow::initDefaultObject()
{
    for (int i = 0; i < objectUiSettings::gTotal_default_object; ++i) {
        if (objectUiSettings::gDefault_object[i].first == "Light") {
            this->addLight(objectUiSettings::gDefault_object[i].first, objectUiSettings::gDefault_object[i].second);
        } else {
            this->addObject(objectUiSettings::gDefault_object[i].first, objectUiSettings::gDefault_object[i].second);
        }
    }
}

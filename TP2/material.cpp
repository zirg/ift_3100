#include "material.h"

Material::Material(eMaterial type, const std::string& name)
    : _type(type), _name(),
      _ambiant(QVector4D(0.0f, 0.0f, 0.0f, 1.0f)),
      _diffuse(QVector4D(1.0f, 1.0f, 1.0f, 1.0f)),
      _specular(QVector4D(1.0f, 1.0f, 1.0f, 1.0f)),
      _texture(nullptr), _normalMap(nullptr), _textureRepeat(1), _textureAlpha(1.0)
{
    _name.assign(name);
    _shineness = 64.0;
}

Material::~Material()
{}

Material::eMaterial Material::getType() const { return _type; }
void                Material::setType(Material::eMaterial type) { _type = type; }
const std::string&  Material::getName() { return _name; }
void                Material::setAmbiant(QVector4D color) { _ambiant = color; }
const QVector4D&    Material::getAmbiant() { return _ambiant; }
void                Material::setDiffuse(QVector4D color) { _diffuse = color; }
const QVector4D&    Material::getDiffuse() { return _diffuse; }
void                Material::setSpecular(QVector4D color) { _specular = color; }
const QVector4D&    Material::getSpecular() { return _specular; }
void                Material::setShineness(float shine) { _shineness = shine; }
float               Material::getShineness() const { return _shineness; }


void                Material::setTexture(const std::string &path, const unsigned int repeat, const float alpha)
{
    if (_texture != nullptr)
        delete _texture;
    _texture = new QOpenGLTexture(QImage(path.c_str()).mirrored());
    _texturePath = path;
    _textureRepeat = repeat;
    _textureAlpha = alpha;
}

void                Material::setTexture(QOpenGLTexture *texture, const unsigned int repeat, const float alpha)
{
    if (_texture != nullptr)
        delete _texture;
    _texture = texture;
    _textureRepeat = repeat;
    _textureAlpha = alpha;
}

void                Material::setTextureRepeat(const unsigned int repeat) {
    _textureRepeat = repeat;
}

QOpenGLTexture*     Material::getTexture() { return _texture; }
unsigned int        Material::getTextureRepeat() const { return _textureRepeat; }
float               Material::getTextureAlpha() const { return _textureAlpha; }
void                Material::setNormalMap(const std::string &path)
{
    if (_normalMap != nullptr)
        delete _normalMap;
    _normalMap = new QOpenGLTexture(QImage(path.c_str()).mirrored());
    _normalMapPath = path;
}

QOpenGLTexture*     Material::getNormalMap() { return _normalMap; }

const std::string &Material::getTexturePath() const {
    return _texturePath;
}

const std::string &Material::getNormalMapPath() const {
    return _normalMapPath;
}

void Material::addTexture(const std::string& path)
{
    _textures.push_front(new QOpenGLTexture(QImage(path.c_str()).mirrored()));
    _texturesPath.push_front(path);
}

std::list<QOpenGLTexture*>  &Material::getTextures() { return _textures; }
std::list<std::string>      &Material::getTexturesPath() { return _texturesPath; }



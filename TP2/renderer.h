#ifndef RENDERER_H
#define RENDERER_H

#include <iostream>
#include <list>
#include <map>
#include <stack>
#include <future>
#include <atomic>

#include <QOpenGLFunctions>
#include <QTimer>

#include "scene.h"
#include "camera.h"
#include "object3d.h"
#include "light.h"
#include "form/form.h"
#include "program/program.h"

class Renderer : protected QOpenGLFunctions
{
public:
    Renderer();
    ~Renderer() = default;

    bool init();
    bool render(Scene *scene);
    QImage capture();

private:
    bool    _renderScreen(Scene *scene);
    bool    _renderObjects3D(Scene *scene);
    bool    _renderObject3D(Camera *cam, std::list<Light *>&, Object3D * obj);
    bool    _renderGeometry(Camera *cam, std::list<Light *>&,
                            Geometry *, Material *);
    void    _pushMatrix(const QMatrix4x4&);
    void    _popMatrix();
    void    _bindProgram(Material::eMaterial);
    void    _resizeBuffers(const int width, const int height);

private:
    std::map<Material::eMaterial, Program::AObjectProgram *> _progObject;
    std::map<Program::AScreenProgram::eProgram, Program::AScreenProgram *> _progScreen;
    std::stack<QMatrix4x4>        _matrix;
    QOpenGLFramebufferObject      *_sampledFbo;
    QOpenGLFramebufferObject      *_fbo;
    std::atomic<bool>              _takeSreenshot;
    std::promise<QImage>          *_promiseSreenshot;

};

#endif // RENDERER_H

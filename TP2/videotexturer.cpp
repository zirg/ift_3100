
#include "videotexturer.h"

VideoTexturer::VideoTexturer(Material& material)
    : _material(material)
{
}

void VideoTexturer::onNewFrame(const QImage& frame)
{
    _material.setTexture(new QOpenGLTexture(frame.mirrored()));
}

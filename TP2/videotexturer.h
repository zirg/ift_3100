#ifndef VIDEOTEXTURER_H
#define VIDEOTEXTURER_H

#include "video/observer.h"
#include "material.h"

class VideoTexturer : public Video::Observer
{
public:

    VideoTexturer(Material& material);

    ~VideoTexturer() = default;

private:

    void onNewFrame(const QImage& frame);

    Material& _material;
};

#endif // VIDEOTEXTURER_H

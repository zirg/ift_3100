#include "camera.h"

Camera::Camera()
    : _screenWidth(CAMERA_DEFAULT_WIDTH),
      _screenHeight(CAMERA_DEFAULT_HEIGHT),
      _fov(CAMERA_FOV),
      _near(CAMERA_NEAR_PLANE),
      _far(CAMERA_FAR_PLANE),
      _right(CAMERA_RIGHT),
      _left(CAMERA_LEFT),
      _top(CAMERA_TOP),
      _bottom(CAMERA_BOTTOM)
{
    reset();
    _perspective.perspective(_fov, (float)_screenWidth / (float)_screenHeight,
                             _near, _far);
    _perspectiveMode = true;
}

Camera::Camera(const int width, const int height)
    : _screenWidth(width),
      _screenHeight(height),
      _fov(CAMERA_FOV),
      _near(CAMERA_NEAR_PLANE),
      _far(CAMERA_FAR_PLANE),
      _right(CAMERA_RIGHT),
      _left(CAMERA_LEFT),
      _top(CAMERA_TOP),
      _bottom(CAMERA_BOTTOM)
{
    reset();
    _perspective.perspective(_fov, (float)_screenWidth / (float)_screenHeight,
                             _near, _far);
    _perspectiveMode = true;
}

void    Camera::setWidth(float w)
{
    _screenWidth = w;
}

void    Camera::setHeight(float h)
{
    _screenHeight = h;
}

void    Camera::resize(int width, int height)
{
    setWidth(width);
    setHeight(height);
    if (_perspectiveMode == true)
        perspective(_fov, _near, _far);
    _normal.setToIdentity();
    _normal = _perspective.inverted().transposed();
}


void    Camera::reset()
{
    _translation.setToIdentity();
    _rotation.setToIdentity();
    _position.setToIdentity();
    _normal.setToIdentity();
    _normal = _perspective.inverted().transposed();
    _arcPoint.setX(0.0);
    _arcPoint.setY(0.0);
    _arcPoint.setZ(0.0);
    _arcRadius = 30.0;
    _arcX = M_PI;
    _arcY = 7.0 * M_PI / 8.0;
    _arcUpdate();
}

void    Camera::perspective(float fov, float n, float f)
{
    _fov = fov;
    _far = f;
    _near = n;
    _perspective.setToIdentity();
    _perspective.perspective(fov, (float)_screenWidth / (float)_screenHeight,
                             n, f);
    _perspectiveMode = true;
}

void    Camera::ortho(float left, float right, float bottom, float top, float n, float f)
{
   _far = f;
   _near = n;
   _left = left;
   _right = right;
   _top = top;
   _bottom = bottom;
   _perspective.setToIdentity();
   _perspective.ortho(left, right, bottom, top, _near, _far);
   _perspectiveMode = false;
}

void    Camera::setFov(float f)
{
    _fov = f;
    _perspective.setToIdentity();
    _perspective.perspective(_fov, (float)_screenWidth / (float)_screenHeight,
                             _near, _far);
    _normal.setToIdentity();
    _normal = _perspective.inverted().transposed();
}

float Camera::getFov() const
{
    return _fov;
}

const QMatrix4x4&  Camera::perspective() const { return _perspective; }
const QMatrix4x4&  Camera::normal() { return _normal; }
int                Camera::width() const { return _screenWidth; }
int                Camera::height() const { return _screenHeight; }


void    Camera::arcRotate(int oldX, int oldY, int newX, int newY)
{

    _arcX -= (oldX - newX) / 4.0 * (M_PI / 180.0);
    _arcY -= (newY - oldY) / 4.0 * (M_PI / 180.0);
    _arcX = fmod(_arcX, M_PI * 2.0);
    _arcY = fmod(_arcY, M_PI * 2.0);
    _arcUpdate();
}

float    Camera::getArcX() const { return _arcX; }
void    Camera::setArcX(float v) { _arcX = v; _arcUpdate(); }
float    Camera::getArcY() const { return _arcY; }
void    Camera::setArcY(float v) { _arcY = v; _arcUpdate(); }
float    Camera::getArcRadius() const { return _arcRadius; }
void    Camera::setArcRadius(float v) { _arcRadius = v; _arcUpdate(); }


void    Camera::_arcUpdate()
{
    float x,y,z;

    //Compute X rotation on XZ plane
    x = _arcRadius * cos(_arcX);
    z = _arcRadius * sin(_arcX);
    y = _arcRadius * sin(_arcY);

    this->move(QVector3D(x, y, z));
}

void    Camera::arcRadius(float degree)
{
  _arcRadius -= degree / 50.0;
  if (_arcRadius < 0.1f)
      _arcRadius = 0.1f;
  _arcUpdate();
}

const QMatrix4x4&  Camera::position()
{
  _position.setToIdentity();
  QMatrix4x4 view;
  view.setToIdentity();
  view.lookAt(Movable::getTranslation() + _arcPoint, _arcPoint, QVector3D(0.0, 1.0, 0.0));
  _position = view * _rotation  * _scale;
  return _position;
}

QVector3D   Camera::getTranslation()
{
    return Movable::getTranslation() + _arcPoint;
}

void       Camera::translatePoint(int camx, int camy, int eventx, int eventy)
{
    float dx = (float)(eventx - camx) / 100.0f * _arcRadius / 2.0f;
    float dy = (float)(eventy - camy) / 100.0f * _arcRadius / 2.0f;
    _arcPoint += QVector3D(-sin(_arcX) * dx, dy, cos(_arcX) * dx);
}

void        Camera::movePoint(const QVector3D& new_pos)
{
    _arcPoint = new_pos;
    _arcUpdate();
}

QVector3D Camera::getPoint() const { return _arcPoint; }

bool Camera::isPerspectiveMode() const
{
    return _perspectiveMode;
}

float Camera::getNear() const
{
    return this->_near;
}

float Camera::getFar() const
{
    return this->_far;
}

float Camera::getLeft() const
{
    return this->_left;
}

float Camera::getRight() const
{
    return this->_right;
}

float Camera::getTop() const
{
    return this->_top;
}

float Camera::getBottom() const
{
    return this->_bottom;
}

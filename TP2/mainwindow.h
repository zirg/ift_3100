#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTreeWidget>
#include <QMainWindow>
#include <QWidget>
#include <iostream>
#include "objectUiSettings/objectcontainer.h"

#include "ieventobject.h"
#include "light.h"
#include "movabletransformationbykey.h"


#include "objectUiSettings/objectDescriptor/lightdescription.h"
#include "objectUiSettings/objectDescriptor/cubedescription.h"
#include "objectUiSettings/objectDescriptor/triangledescription.h"
#include "objectUiSettings/objectDescriptor/spheredescription.h"
#include "objectUiSettings/objectDescriptor/pyramiddescription.h"
#include "objectUiSettings/objectDescriptor/objectloaderdescription.h"
#include "objectUiSettings/objectDescriptor/conedescription.h"
#include "objectUiSettings/objectDescriptor/cylinderdescription.h"
#include "objectUiSettings/objectDescriptor/torusdescription.h"
#include "objectUiSettings/objectDescriptor/kochsnowflakedescription.h"
#include "objectUiSettings/objectDescriptor/planedescription.h"
#include "objectUiSettings/objectDescriptor/bezierdescription.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, private IEventObject
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void refreshContentTabSetting();
    ~MainWindow();
protected:
    void keyPressEvent(QKeyEvent *);

private slots:
    void on_pushButton_clicked();
    void itemChanged();

    void onExportAsClicked();
    void onAddObjectClicked();
    void onAddLightClicked();
    void onNewSceneClicked();
    void onSaveAsSceneClicked();
    void onSaveSceneClicked();
    void onOpenSceneClicked();
    void onQuitClicked();

private:
    void createMenuBar();
    void addObject(QString &);
    void addObject(QString &, Movable *movable);
    void addLight(QString &type);
    void addLight(QString &type, Movable *light);
    void onItemDeleted(objectUiSettings::IWrapper *wrapper);
    void onItemEdited(objectUiSettings::IWrapper *wrapper);
    void onItemAdded(objectUiSettings::IWrapper *wrapper, bool add = true);
    void initObjectSettings();
    void setContentTabSetting(QWidget *);
    void clearContentTabSetting();
    Light::eLight convertStringToLightType(QString const &);
    void initDefaultObject();
private:
    Ui::MainWindow *ui;
    Ui::MovableTransformationByKey transformationByKey;
    objectUiSettings::ObjectContainer _container;
private:
    QString _currentPathSave;
    QMenu   *_fileMenu;
    QAction *_quitAction;
    QAction *_newAction;
    QAction *_saveAction;
    QAction *_saveAsAction;
    QAction *_openAction;
    QMenu   *_newMenu;
    QMenu   *_newObjectMenu;
    QMenu   *_newLightMenu;
    QList<QAction *>    _objects;
    QList<QAction *>    _lights;
};

#endif // MAINWINDOW_H

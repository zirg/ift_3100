#-------------------------------------------------
#
# Project created by QtCreator 2015-01-15T16:12:33
#
#-------------------------------------------------

QT       += core gui opengl multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TP2
TEMPLATE = app
CONFIG += c++11

QMAKE_CXXFLAGS+= -fopenmp -O3
QMAKE_LFLAGS +=  -fopenmp -O3

include(form/form.pri)
include(program/program.pri)
include(objectUiSettings/objectUiSettings.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    widgetGL.cpp \
    camera.cpp \
    material.cpp \
    object3d.cpp \
    geometry.cpp \
    movable.cpp \
    light.cpp \
    scene.cpp \
    renderer.cpp \
    objloader.cpp \
    objexport.cpp \
    video/videosurface.cpp \
    video/videocamera.cpp \
    videotexturer.cpp \
    graphtreeview.cpp \
    movabletransformationbykey.cpp \
    treeserialized.cpp

HEADERS  += mainwindow.h \
    widgetGL.h \
    camera.h \
    material.h \
    object3d.h \
    geometry.h \
    movable.h \
    light.h \
    scene.h \
    renderer.h \
    objloader.h \
    objexport.h \
    video/videosurface.h \
    video/observer.h \
    video/observable.h \
    videotexturer.h \
    video/videocamera.h \
    graphtreeview.h \
    ieventobject.h \
    movabletransformationbykey.h \
    treeserialized.h \
    threadpool.h

FORMS    += mainwindow.ui

RESOURCES += ressource.qrc

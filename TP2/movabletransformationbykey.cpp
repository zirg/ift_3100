#include "movabletransformationbykey.h"

namespace Ui
{
MovableTransformationByKey::MovableTransformationByKey()
{
    this->keyFunctions[Qt::Key_Z] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_S] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_Q] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_D] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_A] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_W] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_R] = &MovableTransformationByKey::translationModifier;
    this->keyFunctions[Qt::Key_F] = &MovableTransformationByKey::translationModifier;

    this->keyFunctions[Qt::Key_Down] = &MovableTransformationByKey::rotationModifier;
    this->keyFunctions[Qt::Key_Up] = &MovableTransformationByKey::rotationModifier;
    this->keyFunctions[Qt::Key_Left] = &MovableTransformationByKey::rotationModifier;
    this->keyFunctions[Qt::Key_Right] = &MovableTransformationByKey::rotationModifier;
    this->keyFunctions[Qt::Key_1] = &MovableTransformationByKey::rotationModifier;
    this->keyFunctions[Qt::Key_2] = &MovableTransformationByKey::rotationModifier;

    this->keyFunctions[Qt::Key_Plus] = &MovableTransformationByKey::scaleModifier;
    this->keyFunctions[Qt::Key_Minus] = &MovableTransformationByKey::scaleModifier;
}

MovableTransformationByKey::~MovableTransformationByKey()
{

}


void MovableTransformationByKey::transformMovableByKey(QKeyEvent *event, Movable *movable)
{
    if (this->keyFunctions[event->key()] && movable != nullptr) {
        (this->*(this->keyFunctions[event->key()]))(event->key(), movable);
    }
}

void MovableTransformationByKey::translationModifier(int key, Movable *movable)
{
    float x, y, z;

    x = 0;
    y = 0;
    z = 0;

    switch (key)
    {
        case Qt::Key_D:
            x += TRANSLATE_STEP;
            break;
        case Qt::Key_A: case Qt::Key_Q:
            x -= TRANSLATE_STEP;
            break;
        case Qt::Key_Z: case Qt::Key_W:
            y += TRANSLATE_STEP;
            break;
        case Qt::Key_S:
            y -= TRANSLATE_STEP;
            break;
        case Qt::Key_R:
            z -= TRANSLATE_STEP;
            break;
        case Qt::Key_F:
            z += TRANSLATE_STEP;
            break;
        default:
            break;
    }
    if (movable != nullptr) {
        movable->translate(QVector3D(x, y, z));
    }
}

void MovableTransformationByKey::rotationModifier(int key, Movable *movable)
{
    QVector3D dir(0.0, 0.0, 0.0);
    switch (key)
    {
        case Qt::Key_Up: case Qt::Key_Down:
            dir[0] = ROTATION_STEP;
            break;
        case Qt::Key_Right: case Qt::Key_Left:
            dir[1] = ROTATION_STEP;
            break;
        case Qt::Key_1: case Qt::Key_2:
            dir[2] = ROTATION_STEP;
            break;
        default:
            break;
    }
    if (key == Qt::Key_Left || key == Qt::Key_2 || key == Qt::Key_Up)
        dir *= -1.0;
    if (movable != nullptr) {
        movable->orient(dir + movable->getRotation());
    }
}

void MovableTransformationByKey::scaleModifier(int key, Movable *movable)
{
    float scale;

    scale = 1.0f;

    switch (key)
    {
        case Qt::Key_Plus:
            scale += SCALE_STEP;
            break;
        case Qt::Key_Minus:
            scale -= SCALE_STEP;
            break;
        default:
            break;
    }
    if (movable != nullptr) {
        movable->scale(QVector3D(scale, scale, scale));
    }
}
}

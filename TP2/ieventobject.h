#ifndef IEVENTOBJECT
#define IEVENTOBJECT

#include "objectUiSettings/iwrapper.h"

class IEventObject {
public:
    ~IEventObject() {}
    virtual void onItemEdited(objectUiSettings::IWrapper *wrapper) = 0;
    virtual void onItemDeleted(objectUiSettings::IWrapper *wrapper) = 0;
    virtual void onItemAdded(objectUiSettings::IWrapper *wrapper, bool add = true) = 0;
};

#endif // IEVENTOBJECT


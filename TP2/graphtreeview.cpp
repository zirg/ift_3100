#include <QMenu>
#include <QHeaderView>
#include "graphtreeview.h"
#include "objectUiSettings/objectwrapper.h"
#include "objectUiSettings/lightwrapper.h"

GraphTreeView::GraphTreeView(QWidget *parent) :
    QTreeWidget(parent),
    TreeSerialized(this),
    _cameraObj(nullptr),
    _sceneObj(nullptr),
    _edit(nullptr),
    _remove(nullptr),
    _eventObject(nullptr),
    _scene(nullptr),
    _camera(nullptr)
{
    this->_remove = new QAction(tr("Remove"), this);
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onCustomContextMenu(QPoint)));
    QObject::connect(this->_remove, SIGNAL(triggered()), this, SLOT(onRemoveObject()));
    QObject::connect(this, SIGNAL(clicked(QModelIndex)), this, SLOT(onClicked(QModelIndex)));
    this->header()->setStretchLastSection(false);
    this->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

GraphTreeView::~GraphTreeView()
{

}

Camera *GraphTreeView::getCamera() const {
    return _cameraObj;
}

Scene *GraphTreeView::getScene() const {
    return _sceneObj;
}

void GraphTreeView::reset() {
    QList<QTreeWidgetItem *> l = this->_scene->takeChildren();
    for (int i = 0; i < this->_scene->childCount(); ++i) {
        qDeleteAll(this->_scene->takeChildren());
    }
    this->_scene->addChild(l.first());
}

QString &GraphTreeView::getObjectLoadPath(objectUiSettings::IWrapper *wrappedObject) {
    return _loaderRef[wrappedObject];
}

void GraphTreeView::addObjectLoadRef(objectUiSettings::IWrapper *wrappedObject, const QString &path) {
    _loaderRef[wrappedObject] = path;
}

void GraphTreeView::setEventObject(IEventObject *event)
{
    this->_eventObject = event;
}

void GraphTreeView::addObject(objectUiSettings::IWrapper *wrappedObject, QString const &name, bool toRoot)
{
    QTreeWidgetItem *current;

    if (this->isObject3D()) {
        current = this->currentItem();
    } else {
        current = this->_scene;
    }
    current->setSelected(false);
    if (toRoot) {
        current = this->_scene;
    }
    QTreeWidgetItem *child = new QTreeWidgetItem();
    if (current == NULL || child == NULL) {
        // TODO error
    }

    if (current != this->_scene) {
        QVariant qvariantOfCurrent = current->data(0, Qt::ItemDataRole::UserRole);
        objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
        if (currentWrappedObject != NULL && currentWrappedObject->getObject() != NULL) {
            ((Object3D *)currentWrappedObject->getObject())->addChild((Object3D *)wrappedObject->getObject());
            if (this->_eventObject != NULL) {
                this->_eventObject->onItemAdded(wrappedObject, false);
            }
        }
    } else {
        if (this->_eventObject != NULL) {
            this->_eventObject->onItemAdded(wrappedObject);
        }
    }

    for (int j = 0; j < current->childCount(); ++j) {
        current->child(j)->setSelected(false);
    }
    child->setText(0, name);
    child->setData(0, Qt::ItemDataRole::UserRole, QVariant::fromValue<objectUiSettings::IWrapper *>(wrappedObject));
    current->addChild(child);
    current->setExpanded(true);
    child->setSelected(true);
    this->setCurrentItem(child);
}

bool GraphTreeView::isObject3D() const
{
    QTreeWidgetItem *current = this->currentItem();
    if (current) {
        if (current == this->_camera) {
            return false;
        }
        QVariant qvariantOfCurrent = current->data(0, Qt::ItemDataRole::UserRole);
        objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
        if (currentWrappedObject != NULL && currentWrappedObject->getObject() != NULL) {
            if (currentWrappedObject->getObjectType() == "Light") {
                return false;
            }
        }
    } else return false;
    return true;
}

Movable *GraphTreeView::getCurrentMovable(QString &type) const
{
    QTreeWidgetItem *current = this->currentItem();
    if (current != nullptr) {
        QVariant qvariantOfCurrent = current->data(0, Qt::ItemDataRole::UserRole);
        objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
        if (currentWrappedObject != nullptr)
        {
            type = currentWrappedObject->getObjectType();
            return currentWrappedObject->getObject();
        }
    }
    return nullptr;
}

void GraphTreeView::init(Scene *scene, Camera *camera)
{
    this->_cameraObj = camera;
    this->_sceneObj = scene;
    this->_scene = new QTreeWidgetItem();
    this->_scene->setText(0, FIRST_ELEMENT_NAME);
    objectUiSettings::IWrapper *sceneWrapper = new objectUiSettings::ObjectWrapper((Movable *)scene, "Scene");
    this->_scene->setData(0, Qt::ItemDataRole::UserRole, QVariant::fromValue<objectUiSettings::IWrapper *>(sceneWrapper));
    this->addTopLevelItem(this->_scene);

    this->_camera = new QTreeWidgetItem();
    this->_camera->setText(0, "Camera");
    this->_camera->setData(0, Qt::ItemDataRole::UserRole, QVariant::fromValue<objectUiSettings::IWrapper *>(new objectUiSettings::ObjectWrapper(camera, "Camera")));
    this->_scene->addChild(this->_camera);
    this->_scene->setExpanded(true);
    this->setCurrentItem(this->_scene);
    if (this->_eventObject != NULL) {
        this->_eventObject->onItemEdited(sceneWrapper);
    }
}

bool GraphTreeView::isEmpty() const {
    return !(this->topLevelItem(0)->childCount() != 2);
}

void GraphTreeView::keyPressEvent(QKeyEvent *event)
{
    parent()->event(reinterpret_cast<QEvent *>(event));
}

void GraphTreeView::keyReleaseEvent(QKeyEvent *event)
{
    parent()->event(reinterpret_cast<QEvent *>(event));
}

void GraphTreeView::onCustomContextMenu(const QPoint &point)
{
    QMenu menu;

    QTreeWidgetItem* item = this->itemAt(point);
    if (item && item != this->_scene && item != this->_camera) {
        menu.addAction(this->_remove);
        menu.exec(mapToGlobal(point));
    }
}

void GraphTreeView::onEditObject()
{
    QTreeWidgetItem* item = this->currentItem();
    if (item == nullptr) {
        return;
    }
    QVariant qvariantOfCurrent = item->data(0, Qt::ItemDataRole::UserRole);
    objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
    if (this->_eventObject != nullptr && currentWrappedObject) {
        this->_eventObject->onItemEdited(currentWrappedObject);
    }
}

void GraphTreeView::onRemoveObject()
{
    QTreeWidgetItem* item = this->currentItem();
    if (item == nullptr) {
        return;
    }
    QVariant qvariantOfCurrent = item->data(0, Qt::ItemDataRole::UserRole);
    objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
    if (this->_eventObject != nullptr) {
        this->_eventObject->onItemDeleted(currentWrappedObject);
    }

    QTreeWidgetItem *parent = item->parent();
    if (parent != NULL) {
        parent->removeChild(item);
        Object3D *obj = dynamic_cast<Object3D *>(currentWrappedObject->getObject());
        if (obj != nullptr && obj->getParent() != nullptr) {
            obj->getParent()->removeChild(obj);
        }
        onEditObject();
        delete item;
        delete currentWrappedObject;
        delete obj;
    }
}

void GraphTreeView::onClicked(QModelIndex index)
{
    (void)index;
    this->onEditObject();
}

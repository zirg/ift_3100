#include <QFile>
#include <QFileInfo>
#include <iostream>
#include <QXmlStreamWriter>

#include "form/form.h"
#include "treeserialized.h"
#include "graphtreeview.h"

#include "objectUiSettings/config.h"

extern int gtotal_material_config;
extern int gtotal_light_config;
extern int gtotal_object_config;
extern objectUiSettings::t_material_config gMaterial_config[];
extern objectUiSettings::t_light_config gLight_config[];
extern objectUiSettings::t_object_config gObjects_config[];

TreeSerialized::TreeSerialized(GraphTreeView *root)
    : _root(root) {
    _objParse["cone"] = objmanip_t(&TreeSerialized::coneToXML, &TreeSerialized::XMLtoCone);
    _objParse["cylinder"] = objmanip_t(&TreeSerialized::cylinderToXML, &TreeSerialized::XMLtoCylinder);
    _objParse["kochsnowflake"] = objmanip_t(&TreeSerialized::kochsnowflakeToXML, &TreeSerialized::XMLtoKochsnowflake);
    _objParse["torus"] = objmanip_t(&TreeSerialized::torusToXML, &TreeSerialized::XMLtoTorus);
    _objParse["plane"] = objmanip_t(&TreeSerialized::planeToXML, &TreeSerialized::XMLtoPlane);
    _objParse["bezier"] = objmanip_t(&TreeSerialized::bezierToXML, &TreeSerialized::XMLtoBezier);
    _objParse["sphere"] = objmanip_t(&TreeSerialized::sphereToXML, &TreeSerialized::XMLtoSphere);

    for (int i = 0; i < objectUiSettings::gtotal_object_config; ++i) {
        this->_container.registerObject(QString(objectUiSettings::gObjects_config[i].type.c_str()),
                                        objectUiSettings::gObjects_config[i].create,
                                        objectUiSettings::gObjects_config[i].etype == objectUiSettings::object_type::OBJECT);
    }
}

const std::list<Movable *> &TreeSerialized::fromXML(const char *path) {
    QFile file(path);

    _path = QString::fromLocal8Bit(path);

    file.open(QIODevice::ReadOnly);

    _scene = _root->topLevelItem(0);
    _listObjLoad.clear();

    _xmlReader = new QXmlStreamReader(&file);
    while (!_xmlReader->atEnd() && !_xmlReader->hasError()) {
        QXmlStreamReader::TokenType token = _xmlReader->readNext();
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement) {
            if(_xmlReader->name() == "scene") {
                _XMLToScene(_root->getScene());
            }
            else if (_xmlReader->name() == "light") {
                _XMLToLight(_scene);
            }
            else if (_xmlReader->name() == "camera") {
                _XMLToCamera(_root->getCamera());
            }
            else {
                _XMLToForm(_scene);
            }
        }
    }
    delete _xmlReader;
    file.close();
    return _listObjLoad;
}

void TreeSerialized::toXML(const char *path) {
    QFile file(path);

    _listObjLoad.clear();
    _path = QString::fromLocal8Bit(path);

    file.open(QIODevice::WriteOnly);

    _xmlWriter = new QXmlStreamWriter(&file);
    _xmlWriter->setAutoFormatting(true);
    _xmlWriter->writeStartDocument();
    _xmlWriter->writeStartElement("scene");

    _sceneToXML(_root->getScene());

    QTreeWidgetItem *scene = _root->topLevelItem(0);
    for (int i = 0; i < scene->childCount(); ++i)
    {
        QTreeWidgetItem *item = scene->child(i);
        if (item != nullptr) _itemToXML(item);
    }

    _xmlWriter->writeEndElement();
    _xmlWriter->writeEndElement();
    _xmlWriter->writeEndDocument();
    delete _xmlWriter;
    file.close();
}

void TreeSerialized::_cameraToXML(Camera *camera) {
    if (camera) {
        _xmlWriter->writeStartElement("position");
        QVector3D pos = camera->getPoint();
        _xmlWriter->writeAttribute("X", QString::number(pos.x()));
        _xmlWriter->writeAttribute("Y", QString::number(pos.y()));
        _xmlWriter->writeAttribute("Z", QString::number(pos.z()));
        _xmlWriter->writeEndElement();
        _xmlWriter->writeStartElement("options");
        _xmlWriter->writeTextElement("mode", QString::number(camera->isPerspectiveMode()));
        _xmlWriter->writeTextElement("far", QString::number(camera->getFar()));
        _xmlWriter->writeTextElement("near", QString::number(camera->getNear()));
        _xmlWriter->writeTextElement("arcRadius", QString::number(camera->getArcRadius()));
        _xmlWriter->writeTextElement("arcX", QString::number(camera->getArcX()));
        _xmlWriter->writeTextElement("arcY", QString::number(camera->getArcY()));
        if (camera->isPerspectiveMode()) {
            _xmlWriter->writeTextElement("fov", QString::number(camera->getFov()));
        } else {
            _xmlWriter->writeStartElement("ortho");
            _xmlWriter->writeTextElement("left", QString::number(camera->getLeft()));
            _xmlWriter->writeTextElement("right", QString::number(camera->getRight()));
            _xmlWriter->writeTextElement("top", QString::number(camera->getTop()));
            _xmlWriter->writeTextElement("bottom", QString::number(camera->getBottom()));
            _xmlWriter->writeEndElement();
        }
        _xmlWriter->writeEndElement();
    }
}

void TreeSerialized::_sceneToXML(Scene *scene) {
    if (scene) {
        _xmlWriter->writeAttribute("cubemap", QString::number(scene->cubeMapIsVisible()));
        _xmlWriter->writeAttribute("grid", QString::number(scene->gridIsVisible()));
        _xmlWriter->writeAttribute("effect", QString::number(scene->getScreenEffect()));
    }
}

void TreeSerialized::_materialToXML(Material *mat) {
    Material::eMaterial type = mat->getType();
    _xmlWriter->writeStartElement("material");
    _xmlWriter->writeAttribute("type", QString::number(type));
    if (type == Material::TEXTURE_MATERIAL || type == Material::NORMAL_MAP_MATERIAL) {
        _xmlWriter->writeStartElement("texture");
        QDir dir(_path.section("/",0,-2));
        QString src = QString::fromStdString(mat->getTexturePath());
        _xmlWriter->writeAttribute("src", QString::fromStdString(dir.relativeFilePath(src).toStdString()));
        _xmlWriter->writeAttribute("repeat", QString::number(mat->getTextureRepeat()));
        _xmlWriter->writeEndElement();
        if (type == Material::NORMAL_MAP_MATERIAL) {
            _xmlWriter->writeStartElement("normalmap");
            src = QString::fromStdString(mat->getNormalMapPath());
            _xmlWriter->writeAttribute("src", QString::fromStdString(dir.relativeFilePath(src).toStdString()));
            _xmlWriter->writeEndElement();
        }
    } else if (type == Material::PHONG_MATERIAL) {
        _xmlWriter->writeTextElement("shineness", QString::number(mat->getShineness()));
    }
    _xmlWriter->writeEndElement();
}

void TreeSerialized::_lightToXML(Light *light) {

    Light::eLight type = light->getType();

    _xmlWriter->writeStartElement("options");
    _xmlWriter->writeTextElement("intensity", QString::number(light->getIntensity()));
    _xmlWriter->writeStartElement("specular");
    QVector4D color = light->getSpecular();
    _xmlWriter->writeAttribute("R", QString::number(color.x()));
    _xmlWriter->writeAttribute("G", QString::number(color.y()));
    _xmlWriter->writeAttribute("B", QString::number(color.z()));
    _xmlWriter->writeAttribute("A", QString::number(color.w()));
    _xmlWriter->writeEndElement();
    _xmlWriter->writeStartElement("diffuse");
    color = light->getDiffuse();
    _xmlWriter->writeAttribute("R", QString::number(color.x()));
    _xmlWriter->writeAttribute("G", QString::number(color.y()));
    _xmlWriter->writeAttribute("B", QString::number(color.z()));
    _xmlWriter->writeAttribute("A", QString::number(color.w()));
    _xmlWriter->writeEndElement();
    _xmlWriter->writeStartElement("ambiant");
    color = light->getAmbiant();
    _xmlWriter->writeAttribute("R", QString::number(color.x()));
    _xmlWriter->writeAttribute("G", QString::number(color.y()));
    _xmlWriter->writeAttribute("B", QString::number(color.z()));
    _xmlWriter->writeAttribute("A", QString::number(color.w()));
    _xmlWriter->writeEndElement();
    if (type == Light::POINT || type == Light::SPOT) {
        _xmlWriter->writeTextElement("constant", QString::number(light->getConstantAtt()));
        _xmlWriter->writeTextElement("linear", QString::number(light->getLinearAtt()));
        _xmlWriter->writeTextElement("quadratic", QString::number(light->getQuadraticAtt()));
        if (type == Light::SPOT) {
            _xmlWriter->writeTextElement("cutoff", QString::number(light->getCutoff()));
            _xmlWriter->writeTextElement("exponent", QString::number(light->getExponent()));
        }
    }
    _xmlWriter->writeEndElement();
}


void TreeSerialized::_itemToXML(QTreeWidgetItem *item) {
    QVariant qvariantOfCurrent = item->data(0, Qt::ItemDataRole::UserRole);
    objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
    Movable *mov = dynamic_cast<Movable *>(currentWrappedObject->getObject());

    _xmlWriter->writeStartElement(currentWrappedObject->getObjectType().toLower());

    if (currentWrappedObject->getObjectType() == "Light") {
        Light *light = dynamic_cast<Light *>(mov);
        if (light) {
            _xmlWriter->writeAttribute("type", QString::number(light->getType()));
        }
    }
    if (currentWrappedObject->getObjectType() == "Loader") {

        QString objPath = _root->getObjectLoadPath(currentWrappedObject);
        QDir dir(_path.section("/",0,-2));
        _xmlWriter->writeAttribute("src", dir.relativeFilePath(objPath));
    }

    if (currentWrappedObject->getObjectType() != "Camera") {
        _xmlWriter->writeStartElement("position");
        QVector3D pos = mov->getTranslation();
        _xmlWriter->writeAttribute("X", QString::number(pos.x()));
        _xmlWriter->writeAttribute("Y", QString::number(pos.y()));
        _xmlWriter->writeAttribute("Z", QString::number(pos.z()));
        _xmlWriter->writeEndElement();
        _xmlWriter->writeStartElement("rotation");
        QVector3D rot = mov->getRotation();
        _xmlWriter->writeAttribute("X", QString::number(rot.x()));
        _xmlWriter->writeAttribute("Y", QString::number(rot.y()));
        _xmlWriter->writeAttribute("Z", QString::number(rot.z()));
        _xmlWriter->writeEndElement();
    }

    if (currentWrappedObject->getObjectType() != "Light" && currentWrappedObject->getObjectType() != "Camera") {
        Object3D *obj = dynamic_cast<Object3D *>(mov);
        QVector3D sca = obj->getScale();
        if (true) {
            _xmlWriter->writeStartElement("scale");
            _xmlWriter->writeAttribute("X", QString::number(sca.x()));
            _xmlWriter->writeAttribute("Y", QString::number(sca.y()));
            _xmlWriter->writeAttribute("Z", QString::number(sca.z()));
        }
        _xmlWriter->writeEndElement();

        Form::Primitive *pri = dynamic_cast<Form::Primitive*>(obj);
        if (pri) {
            _xmlWriter->writeStartElement("ambiant");
            QVector4D color = pri->getAmbiantColor();
            _xmlWriter->writeAttribute("R", QString::number(color.x()));
            _xmlWriter->writeAttribute("G", QString::number(color.y()));
            _xmlWriter->writeAttribute("B", QString::number(color.z()));
            _xmlWriter->writeAttribute("A", QString::number(color.w()));
            _xmlWriter->writeEndElement();

            _xmlWriter->writeStartElement("diffuse");
            color = pri->getDiffuseColor();
            _xmlWriter->writeAttribute("R", QString::number(color.x()));
            _xmlWriter->writeAttribute("G", QString::number(color.y()));
            _xmlWriter->writeAttribute("B", QString::number(color.z()));
            _xmlWriter->writeAttribute("A", QString::number(color.w()));
            _xmlWriter->writeEndElement();

            _xmlWriter->writeStartElement("specular");
            color = pri->getSpecularColor();
            _xmlWriter->writeAttribute("R", QString::number(color.x()));
            _xmlWriter->writeAttribute("G", QString::number(color.y()));
            _xmlWriter->writeAttribute("B", QString::number(color.z()));
            _xmlWriter->writeAttribute("A", QString::number(color.w()));
            _xmlWriter->writeEndElement();
        }

        _xmlWriter->writeStartElement("options");
        QString type = currentWrappedObject->getObjectType().toLower();
        if (_objParse[type].first) (this->*(_objParse[type].first))(obj);
        _xmlWriter->writeEndElement();
        if (type != "loader" || (type == "loader" && obj->mat().front()->getType() != Material::TEXTURE_MATERIAL)) _materialToXML(obj->mat().front());
        if (item->childCount() >= 1) {
            _xmlWriter->writeStartElement("childs");
            for (int i = 0; i < item->childCount(); ++i) {
                _itemToXML(item->child(i));
            }
            _xmlWriter->writeEndElement();
        }
    } else if (currentWrappedObject->getObjectType() == "Light"){
        Light *light = dynamic_cast<Light *>(mov);
        if (light) _lightToXML(light);
    } else {
        _cameraToXML(_root->getCamera());
    }
    _xmlWriter->writeEndElement();
}

void TreeSerialized::_XMLToScene(Scene *scene) {
    QXmlStreamAttributes attributes = _xmlReader->attributes();
    if (attributes.hasAttribute("cubemap") && attributes.hasAttribute("grid")
            && attributes.hasAttribute("effect")) {
        scene->displayCubeMap(attributes.value("cubemap").toInt());
        scene->displayGrid(attributes.value("grid").toInt());
        scene->setScreenEffect(static_cast<Program::AScreenProgram::eProgram>(attributes.value("effect").toInt()));
    }
}

void TreeSerialized::_XMLToLightOptions(Light *light) {
    while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement && _xmlReader->name() == "options")) {
        QStringRef node = _xmlReader->name();
        if (_xmlReader->isStartElement()) {
            if (node == "intensity") {
                _xmlReader->readNext();
                light->setIntensity(_xmlReader->text().toDouble());
            } else if (node == "specular" || node == "diffuse" || node == "ambiant") {
                QXmlStreamAttributes attributes = _xmlReader->attributes();
                if (attributes.hasAttribute("R") && attributes.hasAttribute("G") &&
                    attributes.hasAttribute("B") && attributes.hasAttribute("A")) {
                    QVector4D color(attributes.value("R").toDouble(), attributes.value("G").toDouble(),
                                  attributes.value("B").toDouble(), attributes.value("A").toDouble());
                    if (node == "specular") light->setSpecular(color);
                    else if (node == "ambiant") light->setAmbiant(color);
                    else light->setDiffuse(color);
                }
            } else if (node == "constant" || node == "linear" || node == "quadratic" ||
                       node == "cutoff" || node == "exponent") {
                _xmlReader->readNext();
                double val = _xmlReader->text().toDouble();
              if (node == "constant") light->setConstantAtt(val);
              else if (node == "linear") light->setLinearAtt(val);
              else if (node == "quadratic") light->setQuadraticAtt(val);
              else if (node == "cutoff") light->setCutoff(val);
              else if (node == "exponent") light->setExponent(val);
            }
        }
        _xmlReader->readNext();
    }
}


void TreeSerialized::_XMLToCameraOptions(Camera *camera) {

    int mode = 0;
    double far_val = 0.0f, near_val = 0.0f;
    double fov = 0.0f;

    double orient[4] = {0.0, 0.0, 0.0, 0.0};

    while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement && _xmlReader->name() == "options")) {
        QStringRef node = _xmlReader->name();
        if (_xmlReader->isStartElement()) {
            if (node == "mode") {
                _xmlReader->readNext();
                mode = _xmlReader->text().toInt();
            } else if (node == "far" || node == "near" || node == "fov") {
                _xmlReader->readNext();
                double val = _xmlReader->text().toDouble();
                if (node == "far") far_val = val;
                else if (node == "fov") fov = val;
                else if (node == "near") near_val = val;
            } else if (node == "left" || node == "right" || node == "top" || node == "bottom") {
                _xmlReader->readNext();
                double val = _xmlReader->text().toDouble();
                if (node == "left") orient[0] = val;
                else if (node == "right") orient[1] = val;
                else if (node == "bottom") orient[2] = val;
                else if (node == "top") orient[3] = val;
            } else if (node == "arcRadius" || node == "arcX" || node == "arcY") {
                _xmlReader->readNext();
                double val = _xmlReader->text().toDouble();
                if (node == "arcRadius") camera->setArcRadius(val);
                else if (node == "arcX") camera->setArcX(val);
                else if (node == "arcY") camera->setArcY(val);
            }
        }
        _xmlReader->readNext();
    }
    if (mode) {
        camera->perspective(fov, near_val, far_val);
    } else {
        camera->ortho(orient[0], orient[1], orient[2], orient[3], near_val, far_val);
    }
}

void TreeSerialized::_XMLToCamera(Camera *camera) {

    QVector3D vec;

    QString name = _xmlReader->name().toString();
    _xmlReader->readNext();
    while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement && _xmlReader->name() == name)) {
        QStringRef node = _xmlReader->name();
        if (_xmlReader->isStartElement() && camera) {
            if (node == "position") {
                QXmlStreamAttributes attributes = _xmlReader->attributes();
                if (attributes.hasAttribute("X") && attributes.hasAttribute("Y") && attributes.hasAttribute("Z")) {
                    vec = QVector3D(attributes.value("X").toDouble(), attributes.value("Y").toDouble(), attributes.value("Z").toDouble());
                }
            } else if (node == "options") {
                _xmlReader->readNext();
                if (_xmlReader->isCharacters()) _xmlReader->readNext();
                _XMLToCameraOptions(camera);
            }
        }
        _xmlReader->readNext();
    }
    camera->movePoint(vec);
}

void TreeSerialized::_XMLToLight(QTreeWidgetItem *parent) {
    QString name = _xmlReader->name().toString();
    QTreeWidgetItem *item = new QTreeWidgetItem;

    Light *light = 0;
    objectUiSettings::IWrapper *wrappedObject = 0;
    objectUiSettings::LightDescription *settingView =
            dynamic_cast<objectUiSettings::LightDescription *>(_container.getSettingView(name.left(1).toUpper() + name.mid(1)));
    QXmlStreamAttributes attributes = _xmlReader->attributes();
    if (settingView && attributes.hasAttribute("type")) {
        settingView->setType(static_cast<Light::eLight>(attributes.value("type").toInt()));
        wrappedObject = settingView->getWrappedObject();
        light = dynamic_cast<Light *>(wrappedObject->getObject());
        item->setData(0, Qt::ItemDataRole::UserRole, QVariant::fromValue<objectUiSettings::IWrapper *>(wrappedObject));
        item->setText(0, settingView->getLightType() + " " + name.left(1).toUpper() + name.mid(1));
        parent->addChild(item);
        _listObjLoad.push_back(light);
        _root->getScene()->addLight(light);
    }
    _xmlReader->readNext();
    while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement && _xmlReader->name() == name)) {
        QStringRef node = _xmlReader->name();
        if (_xmlReader->isStartElement() && light) {
            if (node == "position" || node == "rotation") {
                QXmlStreamAttributes attributes = _xmlReader->attributes();
                if (attributes.hasAttribute("X") && attributes.hasAttribute("Y") && attributes.hasAttribute("Z")) {
                    QVector3D vec(attributes.value("X").toDouble(), attributes.value("Y").toDouble(), attributes.value("Z").toDouble());
                    if (node == "position") light->move(vec);
                    else light->orient(vec);
                }
            } else if (node == "options") {
                _xmlReader->readNext();
                if (_xmlReader->isCharacters()) _xmlReader->readNext();
                _XMLToLightOptions(light);
            }
        }
        _xmlReader->readNext();
    }
}

void TreeSerialized::_XMLToForm(QTreeWidgetItem *parent) {

    QString name = _xmlReader->name().toString();
    QTreeWidgetItem *item = new QTreeWidgetItem;
    item->setText(0, name.left(1).toUpper() + name.mid(1));

    Object3D *obj = 0;
    QString src;
    objectUiSettings::IWrapper *wrappedObject = 0;
    objectUiSettings::IObjectDescription *settingView = _container.getSettingView(name.left(1).toUpper() + name.mid(1),
                                                                                  (name == "loader") ? true : false);
    if (name == "loader") {
        QXmlStreamAttributes attributes = _xmlReader->attributes();
        objectUiSettings::ObjectLoaderDescription *dloader = dynamic_cast<objectUiSettings::ObjectLoaderDescription *>(settingView);
        src = attributes.value("src").toString();
        if (dloader && attributes.hasAttribute("src")) {
            QDir dir(_path.section("/",0,-2));
            src = dir.absoluteFilePath(src);
            QFileInfo checkFile(src);
            if (checkFile.exists() && checkFile.isFile()) {
                dloader->setPath(src);
            } else {
                delete item;
                return;
            }
        }
        item->setText(0, src.mid(src.lastIndexOf('/') + 1));
    }
    if (settingView) {
        wrappedObject = settingView->getWrappedObject();
        obj = dynamic_cast<Object3D *>(wrappedObject->getObject());
        _root->repaint();
        _listObjLoad.push_back(obj);
        item->setData(0, Qt::ItemDataRole::UserRole, QVariant::fromValue<objectUiSettings::IWrapper *>(wrappedObject));
        parent->addChild(item);
        parent->setExpanded(true);
        settingView->setCamera(_root->getCamera());
    }
    if (name == "loader") {
        _root->addObjectLoadRef(wrappedObject, src);
    }
    _xmlReader->readNext();
    while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement && _xmlReader->name() == name)) {
           QStringRef node = _xmlReader->name();
           if (_xmlReader->isStartElement() && obj) {
               if (node == "position" || node == "rotation" || node == "scale") {
                   QXmlStreamAttributes attributes = _xmlReader->attributes();
                   if (attributes.hasAttribute("X") && attributes.hasAttribute("Y") && attributes.hasAttribute("Z")) {
                       QVector3D vec(attributes.value("X").toDouble(), attributes.value("Y").toDouble(), attributes.value("Z").toDouble());
                       if (node == "position") obj->move(vec);
                       else if (node == "rotation") obj->orient(vec);
                       else obj->scale(vec);
                   }
               } else if (node == "diffuse" || node == "ambiant" || node == "specular") {
                    QXmlStreamAttributes attributes = _xmlReader->attributes();
                    if (attributes.hasAttribute("R") && attributes.hasAttribute("G") &&
                        attributes.hasAttribute("B") && attributes.hasAttribute("A")) {
                        QVector4D color(attributes.value("R").toDouble(), attributes.value("G").toDouble(),
                                      attributes.value("B").toDouble(), attributes.value("A").toDouble());
                        Form::Primitive *pri = dynamic_cast<Form::Primitive *>(obj);
                        if (pri && node == "diffuse") pri->setDiffuseColor(color);
                        else if (pri && node == "ambiant") pri->setAmbiantColor(color);
                        else if (pri && node == "specular") pri->setSpecularColor(color);
                    }
               } else {
                   QXmlStreamAttributes attributes = _xmlReader->attributes();
                   _xmlReader->readNext();
                   if (_xmlReader->isCharacters()) _xmlReader->readNext();
                   if (node == "options") {
                      if (_objParse[name].second) (this->*(_objParse[name].second))(obj);
                   } else if (node == "material") {
                       Material *mat = obj->mat().front();
                       if (name == "loader") {
                            obj->removeMaterial(mat);
                            delete mat;
                            mat = new Material(static_cast<Material::eMaterial>(attributes.value("type").toInt()), "MATERIAL_XML");
                            obj->addMaterial(mat);
                            auto listGeo = obj->geo();
                            for (auto it = listGeo.begin(); it != listGeo.end(); ++it) {
                                (*it)->setMaterialName("MATERIAL_XML");
                            }
                            mat->setType(static_cast<Material::eMaterial>(attributes.value("type").toInt()));
                       } else {
                            mat->setType(static_cast<Material::eMaterial>(attributes.value("type").toInt()));
                       }
                       _XMLToMaterial(mat);
                   } else if (node == "childs") {
                       while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                                _xmlReader->name() == "childs")) {
                            _XMLToForm(item);
                            _xmlReader->readNext();
                            _xmlReader->readNext();
                       }
                   }
               }
           }
           _xmlReader->readNext();
    }
    if (parent != this->_scene) {
        QVariant qvariantOfCurrent = parent->data(0, Qt::ItemDataRole::UserRole);
        objectUiSettings::IWrapper *currentWrappedObject = qvariantOfCurrent.value<objectUiSettings::IWrapper *>();
        if (currentWrappedObject != NULL && currentWrappedObject->getObject() != NULL) {
            (dynamic_cast<Object3D *>(currentWrappedObject->getObject()))->addChild(obj);
        }
    } else {
        _root->getScene()->addObject3D(obj);
    }
}

void TreeSerialized::_XMLToMaterial(Material *mat) {

    QDir dir(_path.section("/",0,-2));

    while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
             _xmlReader->name() == "material")) {
        QStringRef node = _xmlReader->name();
        if (_xmlReader->isStartElement()) {
            if (node == "texture") {
                QXmlStreamAttributes attributes = _xmlReader->attributes();
                if (attributes.hasAttribute("src")) {
                    std::string src = dir.absoluteFilePath(attributes.value("src").toString()).toStdString();
                    mat->setTexture(src,
                                    attributes.hasAttribute("repeat") ? attributes.value("repeat").toInt() : 1);
                }
            } else if (node == "normalmap") {
                QXmlStreamAttributes attributes = _xmlReader->attributes();
                if (attributes.hasAttribute("src")) {
                    std::string src = dir.absoluteFilePath(attributes.value("src").toString()).toStdString();
                    mat->setNormalMap(src);
                }
            } else if (node == "shineness") {
                _xmlReader->readNext();
                mat->setShineness(_xmlReader->text().toDouble());
            }
        }
        _xmlReader->readNext();
    }
}

void TreeSerialized::XMLtoCone(Object3D *obj) {
    Form::Cone *cone = dynamic_cast<Form::Cone *>(obj);
    if (cone) {
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement()) {
                _xmlReader->readNext();
                if (node == "radius") cone->setRadius(_xmlReader->text().toDouble());
                else if (node == "height") cone->setHeight(_xmlReader->text().toDouble());
                else if (node == "density") cone->setDensity(_xmlReader->text().toDouble());
            }
            _xmlReader->readNext();
        }
    }
}

void TreeSerialized::coneToXML(Object3D *obj) {
    Form::Cone *cone = dynamic_cast<Form::Cone *>(obj);
    if (cone) {
        _xmlWriter->writeTextElement("radius", QString::number(cone->getRadius()));
        _xmlWriter->writeTextElement("height", QString::number(cone->getHeight()));
        _xmlWriter->writeTextElement("density", QString::number(cone->getDensity()));
    }
}

void TreeSerialized::XMLtoCylinder(Object3D *obj) {
    Form::Cylinder *cylinder = dynamic_cast<Form::Cylinder *>(obj);
    if (cylinder) {
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement()) {
                _xmlReader->readNext();
                if (node == "radius") cylinder->setRadius(_xmlReader->text().toDouble());
                else if (node == "height") cylinder->setHeight(_xmlReader->text().toDouble());
            }
            _xmlReader->readNext();
        }
    }
}

void TreeSerialized::cylinderToXML(Object3D *obj) {
    Form::Cylinder *cylinder = dynamic_cast<Form::Cylinder *>(obj);
    if (cylinder) {
        _xmlWriter->writeTextElement("radius", QString::number(cylinder->getRadius()));
        _xmlWriter->writeTextElement("height", QString::number(cylinder->getHeight()));
    }
}

void TreeSerialized::XMLtoKochsnowflake(Object3D *obj) {
    Form::KochSnowflake *kochsnowflake = dynamic_cast<Form::KochSnowflake *>(obj);
    if (kochsnowflake) {
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement()) {
                _xmlReader->readNext();
                if (node == "factor") kochsnowflake->setFactor(_xmlReader->text().toDouble());
                else if (node == "size") kochsnowflake->setSize(_xmlReader->text().toDouble());
            }
            _xmlReader->readNext();
        }
    }
}

void TreeSerialized::kochsnowflakeToXML(Object3D *obj) {
    Form::KochSnowflake *kochsnowflake = dynamic_cast<Form::KochSnowflake *>(obj);
    if (kochsnowflake) {
        _xmlWriter->writeTextElement("factor", QString::number(kochsnowflake->getFactor()));
        _xmlWriter->writeTextElement("size", QString::number(kochsnowflake->getSize()));
    }
}

void TreeSerialized::torusToXML(Object3D *obj) {
    Form::Torus *torus = dynamic_cast<Form::Torus *>(obj);
    if (torus) {
        _xmlWriter->writeTextElement("radius", QString::number(torus->getRadius()));
        _xmlWriter->writeTextElement("radius2", QString::number(torus->getRadius2()));
        _xmlWriter->writeTextElement("density", QString::number(torus->getDensity()));
    }
}

void TreeSerialized::XMLtoTorus(Object3D *obj) {
    Form::Torus *torus = dynamic_cast<Form::Torus *>(obj);
    if (torus) {
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement()) {
                _xmlReader->readNext();
                if (node == "radius") torus->setRadius(_xmlReader->text().toDouble());
                else if (node == "radius2") torus->setRadius2(_xmlReader->text().toDouble());
                else if (node == "density") torus->setDensity(_xmlReader->text().toDouble());
            }
            _xmlReader->readNext();
        }
    }
}

void TreeSerialized::planeToXML(Object3D *obj) {
    Form::Plane *plane = dynamic_cast<Form::Plane *>(obj);
    if (plane) {
        _xmlWriter->writeTextElement("size", QString::number(plane->getSize()));
    }
}

void TreeSerialized::XMLtoPlane(Object3D *obj) {
    Form::Plane *plane = dynamic_cast<Form::Plane *>(obj);
    if (plane) {
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement()) {
                _xmlReader->readNext();
                if (node == "size") plane->setSize(_xmlReader->text().toDouble());
            }
            _xmlReader->readNext();
        }
    }
}

void TreeSerialized::bezierToXML(Object3D *obj) {
    Form::Bezier *bezier = dynamic_cast<Form::Bezier *>(obj);
    if (bezier) {
        _xmlWriter->writeStartElement("points");
         const int nbpoint = bezier->getNbPoints();
         const int size = nbpoint * nbpoint;
         for (int i = 0; i < size; i++) {

             const int row = i / nbpoint;
             const int col = i % nbpoint;

             QVector3D point(bezier->getPoint(row, col));
             _xmlWriter->writeStartElement("point");
             _xmlWriter->writeAttribute("X", QString::number(point.x()));
             _xmlWriter->writeAttribute("Y", QString::number(point.y()));
             _xmlWriter->writeAttribute("Z", QString::number(point.z()));
             _xmlWriter->writeEndElement();
         }
        _xmlWriter->writeEndElement();
    }
}

void TreeSerialized::XMLtoBezier(Object3D *obj) {
    Form::Bezier *bezier = dynamic_cast<Form::Bezier *>(obj);
    if (bezier) {
        const int nbpoint = bezier->getNbPoints();
        const int size = nbpoint * nbpoint;
        int i = 0;
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement() && node == "point" && i < size) {
                QXmlStreamAttributes attributes = _xmlReader->attributes();
                if (attributes.hasAttribute("X") && attributes.hasAttribute("Y") && attributes.hasAttribute("Z")) {
                    QVector3D vec(attributes.value("X").toDouble(), attributes.value("Y").toDouble(), attributes.value("Z").toDouble());
                    const int row = i / nbpoint;
                    const int col = i % nbpoint;
                    QVector3D &point = bezier->getPoint(row, col);
                    point.setX(vec.x());
                    point.setY(vec.y());
                    point.setZ(vec.z());
                }
                i++;
            }
            _xmlReader->readNext();
        }
        bezier->regenerate();
    }
}

void TreeSerialized::XMLtoSphere(Object3D *obj) {
    Form::Sphere *sphere = dynamic_cast<Form::Sphere *>(obj);
    if (sphere) {
        while (!(_xmlReader->tokenType() == QXmlStreamReader::EndElement &&
                 _xmlReader->name() == "options")) {
            QStringRef node = _xmlReader->name();
            if (_xmlReader->isStartElement()) {
                _xmlReader->readNext();
                if (node == "density") sphere->setDensity(_xmlReader->text().toDouble());
            }
            _xmlReader->readNext();
        }
    }
}

void TreeSerialized::sphereToXML(Object3D *obj) {
    Form::Sphere *sphere = dynamic_cast<Form::Sphere *>(obj);
    if (sphere) {
        _xmlWriter->writeTextElement("density", QString::number(sphere->getDensity()));
    }
}

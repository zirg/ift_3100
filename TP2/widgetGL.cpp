#include <QFile>
#include <QLabel>
#include <QLayout>

#include "widgetGL.h"

#include "objloader.h"

#include "videotexturer.h"
#include "video/videocamera.h"

widgetGL::widgetGL(QWidget *parent) :
    QOpenGLWidget(parent)
{
    QSurfaceFormat sf;
    this->setFormat(sf);

    Camera *c = new Camera(width(), height());
    _scene.setCamera(c);

    _cam_x = -1;
    _cam_y = -1;
}

widgetGL::~widgetGL()
{ }

void widgetGL::clear() {
    _scene.clear();
}


bool widgetGL::importObjects(const char *path) {

    _scene.clear();
    _obj.clear();

    ObjLoader loader;
    Object3D *objLoaded = loader.load(path);
    if (objLoaded) {
        _obj.push_front(objLoaded);
        _obj.front()->rotate(90.0f, QVector3D(1.0f, 1.0f, 0.0f));
        _obj.front()->scale(QVector3D(6.0, 6.0, 6.0));
        _scene.addObject3D(_obj.front());
    }
    return true;
}

void widgetGL::resizeGL(int new_width, int new_height)
{
    makeCurrent();
    initializeOpenGLFunctions();
    this->resize(new_width, new_height);
    glViewport(0, 0, new_width, new_height);

    Camera *c = _scene.getCamera();
    c->resize(new_width, new_height);
}

void widgetGL::initializeGL()
{
    makeCurrent();
    initializeOpenGLFunctions();
    _render.init();

    //Timer 60 Fps
    connect(&_timer, SIGNAL(timeout()), this, SLOT(update()));
    if (format().swapInterval() == -1) {
        _timer.setInterval(16);
    }
    else {
        _timer.setInterval(0);
    }
    _timer.start();

     Form::Grid *grid = new Form::Grid(20);
     _scene.setGrid(grid);

     CubeMap *cubemap = new CubeMap();
     cubemap->setTexture(0);
     _scene.setCubeMap(cubemap);
}

void widgetGL::paintGL()
{
    _renderFrame();
}

void widgetGL::addMovableToScene(Movable *movable)
{
    Object3D *obj = dynamic_cast<Object3D *>(movable);
    Light *light;
    if (obj != NULL) {
        this->_scene.addObject3D(obj);
    }
    light = dynamic_cast<Light *>(movable);
    if (light != NULL) {
        this->_scene.addLight(light);
    }
}

void widgetGL::removeMovableToScene(Movable *movable)
{
    Object3D *obj = dynamic_cast<Object3D *>(movable);
    Light *light;
    if (obj != NULL) {
        this->_scene.removeObject3D(obj);
    }
    light = dynamic_cast<Light *>(movable);
    if (light != NULL) {
        this->_scene.removeLight(light);
    }
}

Scene *widgetGL::getScene()
{
    return &this->_scene;
}

bool widgetGL::takeScreenShot(const char *path) {

    std::thread([this, path]{
        _render.capture().save(path);
    }).detach();
    return true;
}

//Camera handle
void widgetGL::mousePressEvent(QMouseEvent *event)
{
    _cam_x = event->x();
    _cam_y = event->y();
}

void widgetGL::mouseMoveEvent(QMouseEvent *event)
{
    if (_cam_x == -1 || _cam_y == -1)
        return;
     if (event->buttons() & Qt::LeftButton)
     {
        _scene.getCamera()->arcRotate(_cam_x, _cam_y, event->x(), event->y());
        if (_scene.cubeMapIsVisible())
            _scene.getCubeMap()->move(_scene.getCamera()->getTranslation());
     }
     else if (event->buttons() & Qt::RightButton)
     {
         _scene.getCamera()->translatePoint(_cam_x, _cam_y, event->x(), event->y());
     }
     _cam_x = event->x();
     _cam_y = event->y();
     QLabel *infoLabel = dynamic_cast<QLabel *>(this->children().front());
     infoLabel->setText("Camera :\n   X: " + QString::number(_cam_x) + "\n   Y: " + QString::number(_cam_y));
     if (!infoLabel->isVisible()) infoLabel->show();
}

void widgetGL::wheelEvent(QWheelEvent *event)
{
    _scene.getCamera()->arcRadius(event->delta() * 0.1);
     if (_scene.cubeMapIsVisible())
        _scene.getCubeMap()->move(_scene.getCamera()->getTranslation());
}

void widgetGL::_renderFrame()
{
    _render.render(&_scene);
}

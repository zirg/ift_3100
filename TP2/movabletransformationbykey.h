#ifndef MOVABLETRANSFORMATIONBYKEY_H
#define MOVABLETRANSFORMATIONBYKEY_H

#include <QtGui>
#include <map>

#include "movable.h"

#define TRANSLATE_STEP 0.5f
#define ROTATION_STEP 5.0f
#define SCALE_STEP 0.1f

namespace Ui {
class MovableTransformationByKey
{
private:
    std::map<int, void (MovableTransformationByKey::*)(int, Movable *)> keyFunctions;

public:
    MovableTransformationByKey();
    ~MovableTransformationByKey();

public:
    void transformMovableByKey(QKeyEvent *, Movable *);

private:
    void translationModifier(int, Movable *);
    void rotationModifier(int, Movable *);
    void scaleModifier(int, Movable *);
};
}
#endif // MOVABLETRANSFORMATIONBYKEY_H

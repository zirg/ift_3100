#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <iostream>
#include <list>
#include <QMatrix4x4>

#include "movable.h"
#include "material.h"
#include "geometry.h"

class Object3D : public Movable
{
public:
    Object3D();
    virtual ~Object3D();

    std::list<Geometry *>& geo();
    std::list<Material *>& mat();
    void                    addChild(Object3D *);
    void                    removeChild(Object3D *);
    std::list<Object3D *>&  getChildren();
    Object3D*               getParent();

    void        addGeometry(const std::string& material_name,
                            Geometry *geometry);
    void        addGeometry(Geometry *geometry);
    std::list<Geometry *>& getGeometry();
    void        removeGeometry(Geometry *g);


    void        addMaterial(Material *material);
    void        removeMaterial(Material *material);
    Material    *getMaterial(const std::string& name);
protected:
    Object3D *              _parent;
    std::list<Object3D *>   _child;
    std::list<Geometry *>   _geometry;
    std::list<Material *>   _material;
};

#endif // OBJECT3D_H

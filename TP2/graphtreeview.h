#ifndef GRAPHTREEVIEW_H
#define GRAPHTREEVIEW_H
#define FIRST_ELEMENT_NAME "Scene"

#include <QTreeWidget>
#include <QAction>

#include "treeserialized.h"
#include "objectUiSettings/iobjectdescriptor.h"
#include "ieventobject.h"
#include "light.h"
#include "scene.h"
#include "camera.h"

class GraphTreeView : public QTreeWidget, public TreeSerialized
{
    Q_OBJECT
public:
    explicit GraphTreeView(QWidget *parent = 0);
    ~GraphTreeView();
public:
    void setEventObject(IEventObject *event);
    void addObject(objectUiSettings::IWrapper *wrappedObject, const QString &name, bool toRoot = false);
    void addObjectLoadRef(objectUiSettings::IWrapper *wrappedObject, const QString &path);
    QString &getObjectLoadPath(objectUiSettings::IWrapper *wrappedObject);
    bool isObject3D() const;
    void reset();
    bool isEmpty() const;
    Movable *getCurrentMovable(QString &type) const;
    Camera *getCamera() const;
    Scene *getScene() const;
    void init(Scene *scene, Camera *camera);

protected:
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

private slots:
    void onCustomContextMenu(const QPoint &point);
    void onEditObject();
    void onRemoveObject();
    void onClicked(QModelIndex);
private:
    Camera *_cameraObj;
    Scene *_sceneObj;
    QAction *_edit;
    QAction *_remove;
    IEventObject *_eventObject;
    QTreeWidgetItem *_scene;
    QTreeWidgetItem *_camera;
    std::map<objectUiSettings::IWrapper *, QString> _loaderRef;
};

#endif // GRAPHTREEVIEW_H

#ifndef IEXPORT
#define IEXPORT

#include <string>

class IExport {
public:
    virtual void save(const std::string &path) = 0;
};

#endif // IEXPORT


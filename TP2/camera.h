#ifndef CAMERA_H
#define CAMERA_H

#include <iostream>
#include <QOpenGLFunctions>
#include <QMatrix4x4>

#include "movable.h"

#define CAMERA_DEFAULT_WIDTH 800
#define CAMERA_DEFAULT_HEIGHT 600
#define CAMERA_FOV 60.0f
#define CAMERA_NEAR_PLANE 0.1f
#define CAMERA_FAR_PLANE 3000.0f
#define CAMERA_RIGHT 1.0
#define CAMERA_LEFT 1.0
#define CAMERA_TOP 1.0
#define CAMERA_BOTTOM 1.0

class Camera : public Movable, protected QOpenGLFunctions
{
public:
    Camera();
    Camera(const int width, const int height);
    ~Camera() = default;

    void    reset();
    const QMatrix4x4&  perspective() const;
    void  perspective(float, float, float);
    void  ortho(float, float, float, float, float, float);
    const QMatrix4x4&  normal();
    void    lookAt(QVector3D& point);
    int     width() const;
    int     height() const;
    void    setFov(float f);
    float   getFov() const;
    void    setWidth(float w);
    void    setHeight(float h);
    void    resize(int width, int height);
    void    arcRotate(int, int, int, int);
    void    arcRadius(float);
    float    getArcX() const;
    void    setArcX(float v);
    float    getArcY() const;
    void    setArcY(float v);
    float    getArcRadius() const;
    void    setArcRadius(float v);
    const QMatrix4x4&  position();
    QVector3D   getTranslation();
    void        translatePoint(int, int, int, int);
    void        movePoint(const QVector3D&);
    QVector3D   getPoint() const;
    bool        isPerspectiveMode() const;
    float   getNear() const;
    float   getFar() const;
    float   getLeft() const;
    float   getRight() const;
    float   getTop() const;
    float   getBottom() const;
private:
    void    _arcUpdate();
private:
    QMatrix4x4  _perspective;
    bool        _perspectiveMode;
    QMatrix4x4  _normal;
    int         _screenWidth;
    int         _screenHeight;
    float       _fov;
    float       _near;
    float       _far;
    float       _arcRadius;
    float       _arcX;
    float       _arcY;
    QVector3D   _arcPoint;
    float       _right;
    float       _left;
    float       _top;
    float       _bottom;
};

#endif // CAMERA_H

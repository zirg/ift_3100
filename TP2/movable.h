#ifndef MOVABLE_H
#define MOVABLE_H

#include <iostream>
#include <QMatrix4x4>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265359
#endif

class Movable
{
public:
    Movable();
    virtual ~Movable();

    virtual void    translate(QVector3D direction);
    virtual void    move(QVector3D position);
    QVector3D       getTranslation() const;
    QVector3D       getRotation() const;
    QVector3D       getScale() const;

    virtual void    rotate(float angle, QVector3D direction);
    virtual void    orient(float angle, QVector3D orientation);
    virtual void    orient(QVector3D orientation);
    virtual void    scale(QVector3D direction);
    virtual void    resize(QVector3D direction);

    virtual const QMatrix4x4&  position();

protected:
    QMatrix4x4  _position;

    QMatrix4x4  _translation;
    QMatrix4x4  _rotation;
    QMatrix4x4  _scale;

    QVector3D   _euler;
};

#endif // MOVABLE_H

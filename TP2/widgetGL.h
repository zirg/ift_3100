#ifndef WIDGETGL_H
#define WIDGETGL_H

#include <iostream>
#include <map>
#include <list>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QTimer>
#include <QMouseEvent>
#include <QWheelEvent>

#include "renderer.h"
#include "scene.h"
#include "camera.h"
#include "object3d.h"
#include "light.h"
#include "form/form.h"
#include "program/program.h"

class widgetGL : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit widgetGL(QWidget *parent = 0);
    ~widgetGL();
    void clear();
    bool takeScreenShot(const char *path);
    bool importObjects(const char *path);
    void resizeGL(int new_width, int new_height);
    void initializeGL();
    void paintGL();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void addMovableToScene(Movable *movable);
    void removeMovableToScene(Movable *movable);
    Scene *getScene();
private:
    void _renderFrame();
signals:
public slots:
private:
   Scene                    _scene;
   int                      _cam_x;
   int                      _cam_y;
   Renderer                 _render;
   std::map<Material::eMaterial,
            Program::AProgram *> _prog;
   QTimer                   _timer;
   std::list<Object3D *>    _obj;
};

#endif // WIDGETGL_H

#ifndef ABASESETTINGVIEW_H
#define ABASESETTINGVIEW_H

#include <QObject>
#include <QPushButton>
#include <QWidget>
#include <QColorDialog>
#include <QComboBox>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>

#include "camera.h"
#include "videotexturer.h"
#include "video/videocamera.h"
#include "iobjectdescriptor.h"
#include "objectDescriptor/clickablelabel.h"

namespace objectUiSettings {
class BaseSettingOptions {
public:
    BaseSettingOptions();
    ~BaseSettingOptions();
public:
    bool Translation;
    bool Rotation;
    bool Scale;
    bool Material;
    bool Color;
    bool Video;
    bool Focus;
};

class ABaseSettingView : public IObjectDescription
{
public:
    explicit ABaseSettingView(QWidget *parent = 0, Movable *movable = 0);
    virtual ~ABaseSettingView();
public:
    virtual void init(bool = false);
    IWrapper *getWrappedObject();
    virtual void setObjectSettingInformation(IWrapper *wrapper);
    virtual void refreshDescription();
    void setCamera(Camera *);

protected:
    void init(BaseSettingOptions const &);
    virtual void onObjectSet() = 0;
    QDoubleSpinBox *createDoubleSpinBoxConfigured();
private:
    void translationView();
    void rotationView();
    void scaleView();
    void materialView();
    void colorView();
private:
    void translationSave();
    void rotationSave();
    void scaleSave();
private:
    void colorEdit();
    void translationEdit();
    void rotationEdit();
    void scaleEdit();
    void materialEdit();
protected slots:
    virtual void colorSave(const QColor &color, int type = 0);
    virtual void materialViewOption();
    virtual void saveEvent();
    virtual void materialSave(int);
protected:
    BaseSettingOptions _options;
    QVBoxLayout *_layout;
    QPushButton *_Save;
    Camera *_view;

    QGroupBox *_gbRot;
    QGroupBox *_gbTrans;

    QDoubleSpinBox *_xTranslationData;
    QDoubleSpinBox *_yTranslationData;
    QDoubleSpinBox *_zTranslationData;

    QDoubleSpinBox *_xRotationData;
    QDoubleSpinBox *_yRotationData;
    QDoubleSpinBox *_zRotationData;

    QDoubleSpinBox *_xScaleData;
    QDoubleSpinBox *_yScaleData;
    QDoubleSpinBox *_zScaleData;

    // Material
    QComboBox *_materialCb;
    QGroupBox *_gbColor;
    QGroupBox *_gbMaterial;
    QLabel *_shinenessLabel;
    QDoubleSpinBox *_shinenessBox;
    QLabel *_textureLabel;
    QFileDialog *_textureFileDialog;
    QPushButton *_textureButton;
    QLabel *_textureNLabel;
    QFileDialog *_textureNFileDialog;
    QPushButton *_textureNButton;
    QCheckBox *_checkVideo;
    QLabel *_textureRLabel;
    QSpinBox *_repeatData;
    ClickableLabel *_colorDiffuseLabel;
    QColorDialog *_colorDiffuseDialog;
    ClickableLabel *_colorAmbianteLabel;
    QColorDialog *_colorAmbianteDialog;
    ClickableLabel *_colorSpecularLabel;
    QColorDialog *_colorSpecularDialog;
    Video::VideoCamera *_camera;
    Movable *_object3d;
};
}
#endif // ABASESETTINGVIEW_H

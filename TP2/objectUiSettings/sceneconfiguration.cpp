#include <QGroupBox>
#include <QCheckBox>
#include <QComboBox>

#include "sceneconfiguration.h"
#include "program/ascreenprogram.h"

namespace objectUiSettings {
SceneConfiguration::SceneConfiguration(QWidget *parent) : QWidget(parent)
{

}

SceneConfiguration::~SceneConfiguration()
{

}

SceneConfiguration *SceneConfiguration::Create()
{
    return new SceneConfiguration();
}

void SceneConfiguration::init(Scene *scene)
{
    this->_scene = scene;
    this->_layout = new QVBoxLayout();

    QGroupBox *gb = new QGroupBox("Grid :");
    QHBoxLayout *gridLayout = new QHBoxLayout();
    gridLayout->setObjectName("gridLayout");
    gridLayout->addWidget(new QLabel("Display :"));
    QCheckBox *cb = new QCheckBox();
    cb->setChecked(scene->gridIsVisible());
    cb->connect(cb, &QCheckBox::stateChanged, [this](int state) {
        this->_scene->displayGrid(state);
    });

    gridLayout->addWidget(cb);
    gb->setLayout(gridLayout);
    this->_layout->addWidget(gb);

    QGroupBox *gbEffect = new QGroupBox("Screen :");
    QHBoxLayout *effectLayout = new QHBoxLayout();
    effectLayout->setObjectName("effectLayout");
    effectLayout->addWidget(new QLabel("Effect :"));

    _cbScreen = new QComboBox;

    _cbScreen->addItem("Default", Program::AScreenProgram::DEFAULT);
    _cbScreen->addItem("Black and White", Program::AScreenProgram::BLACKnWHITE);
    _cbScreen->addItem("Blur", Program::AScreenProgram::BLUR);
    _cbScreen->addItem("Bloom", Program::AScreenProgram::BLOOM);
    _cbScreen->addItem("Fisheye", Program::AScreenProgram::FISHEYE);
    _cbScreen->setCurrentIndex(scene->getScreenEffect());

    _cbScreen->connect(_cbScreen, SIGNAL(currentIndexChanged(int)), this, SLOT(screenTypeChanged(int)));


    effectLayout->addWidget(_cbScreen);
    gbEffect->setLayout(effectLayout);
    this->_layout->addWidget(gbEffect);

    QGroupBox *gbCubemap = new QGroupBox("Cubemap :");
    QHBoxLayout *cubemapLayout = new QHBoxLayout();
    cubemapLayout->setObjectName("scaleLayout");
    cubemapLayout->addWidget(new QLabel("Display :"));
    QCheckBox *cbCubemap = new QCheckBox();
    cbCubemap->setChecked(scene->cubeMapIsVisible());
    cbCubemap->connect(cbCubemap, &QCheckBox::stateChanged, [this](int state) {
        this->_scene->displayCubeMap(state);
    });

    cubemapLayout->addWidget(cbCubemap);
    gbCubemap->setLayout(cubemapLayout);
    this->_layout->addWidget(gbCubemap);

    this->setLayout(this->_layout);
}

void  SceneConfiguration::screenTypeChanged(int index) {
    QVariant variant = _cbScreen->itemData(index);
    Program::AScreenProgram::eProgram type = static_cast<Program::AScreenProgram::eProgram>(variant.toInt());
    this->_scene->setScreenEffect(type);
}

}

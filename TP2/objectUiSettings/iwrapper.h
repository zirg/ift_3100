#ifndef IWRAPPER
#define IWRAPPER
#include <QString>
#include "movable.h"

namespace objectUiSettings {
class IWrapper {
public:
    virtual ~IWrapper() {}
public:
    virtual QString const &getObjectType() const = 0;
    virtual Movable *getObject() const = 0;
};
}
#endif // IWRAPPER


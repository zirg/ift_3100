#include "cylinderdescription.h"
#include "form/cylinder.h"

namespace objectUiSettings {
CylinderDescription::CylinderDescription(QWidget *parent, Movable *movable): ABaseSettingView(parent, movable),
    _name("Cylinder"), _type(_name)
{

}

CylinderDescription::~CylinderDescription()
{

}

void CylinderDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = true;
    ABaseSettingView::init(options);
    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    otherLayout->setObjectName("otherLayout");

    QHBoxLayout *radiusLayout = new QHBoxLayout();
    radiusLayout->addWidget(new QLabel("Radius :"));
    this->_radiusData = new QDoubleSpinBox();
    radiusLayout->addWidget(this->_radiusData);
    otherLayout->addLayout(radiusLayout);

    QHBoxLayout *heightLayout = new QHBoxLayout();
    heightLayout->addWidget(new QLabel("Height :"));
    this->_heightData = new QDoubleSpinBox();
    heightLayout->addWidget(this->_heightData);
    otherLayout->addLayout(heightLayout);

   /* QHBoxLayout *densityLayout = new QHBoxLayout();
    densityLayout->addWidget(new QLabel("Density :"));
    this->_densityData = new QDoubleSpinBox();
    this->_densityData->setValue(50);
    this->_densityData->connect(this->_densityData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    densityLayout->addWidget(this->_densityData);
    otherLayout->addLayout(densityLayout);*/

    gb->setLayout(otherLayout);
    this->_layout->addWidget(gb);
}

QWidget *CylinderDescription::Create()
{
    return new CylinderDescription();
}

const QString &CylinderDescription::getName() const
{
    return this->_name;
}

const QString &CylinderDescription::getType() const
{
    return this->_type;
}

Movable *CylinderDescription::createObject()
{
    return new Form::Cylinder(1,7, 64);
}

IWrapper *CylinderDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void CylinderDescription::onObjectSet()
{
    Form::Cylinder *cylinder = dynamic_cast<Form::Cylinder *>(this->_object3d);
    if (cylinder) {
        //this->_densityData->setValue(cylinder->getDensity());
        this->_heightData->setValue(cylinder->getHeight());
        this->_radiusData->setValue(cylinder->getRadius());
    }
    this->_heightData->connect(this->_heightData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    this->_radiusData->connect(this->_radiusData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    //this->_densityData->connect(this->_densityData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
}

void CylinderDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void CylinderDescription::saveSpecialEvent() {
    Form::Cylinder *cylinder = dynamic_cast<Form::Cylinder *>(this->_object3d);
    if (cylinder) {
        //cylinder->setDensity(this->_densityData->value());
        cylinder->setHeight(this->_heightData->value());
        cylinder->setRadius(this->_radiusData->value());
    }
}

void CylinderDescription::materialSave(int index)
{
    ABaseSettingView::materialSave(index);
}

void CylinderDescription::materialViewOption()
{
    ABaseSettingView::materialViewOption();
}

void CylinderDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}
}


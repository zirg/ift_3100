HEADERS += $$PWD/objectloaderdescription.h \
    $$PWD/cubedescription.h \
    $$PWD/triangledescription.h \
    $$PWD/lightdescription.h \
    $$PWD/spheredescription.h \
    $$PWD/pyramiddescription.h \
    $$PWD/descriptor.h \
    $$PWD/clickablelabel.h \
    $$PWD/conedescription.h \
    $$PWD/cylinderdescription.h \
    $$PWD/torusdescription.h \
    $$PWD/kochsnowflakedescription.h \
    $$PWD/planedescription.h \
    $$PWD/cameradescription.h \
    $$PWD/bezierdescription.h \
    $$PWD/earthdescription.h


SOURCES += $$PWD/objectloaderdescription.cpp \
    $$PWD/cubedescription.cpp \
    $$PWD/triangledescription.cpp \
    $$PWD/lightdescription.cpp \
    $$PWD/spheredescription.cpp \
    $$PWD/pyramiddescription.cpp \
    $$PWD/clickablelabel.cpp \
    $$PWD/conedescription.cpp \
    $$PWD/cylinderdescription.cpp \
    $$PWD/torusdescription.cpp \
    $$PWD/kochsnowflakedescription.cpp \
    $$PWD/planedescription.cpp \
    $$PWD/cameradescription.cpp \
    $$PWD/bezierdescription.cpp \
    $$PWD/earthdescription.cpp


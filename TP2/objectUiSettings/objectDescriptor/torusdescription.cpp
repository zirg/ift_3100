#include "torusdescription.h"
#include "form/torus.h"

namespace objectUiSettings {
TorusDescription::TorusDescription(QWidget *parent, Movable *movable): ABaseSettingView(parent, movable),
    _name("Torus"), _type(_name)
{

}

TorusDescription::~TorusDescription()
{

}

void TorusDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = true;
    ABaseSettingView::init(options);
    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    otherLayout->setObjectName("otherLayout");

    QHBoxLayout *radiusLayout = new QHBoxLayout();
    radiusLayout->addWidget(new QLabel("Radius :"));
    this->_radiusData = new QDoubleSpinBox();
    this->_radiusData->setValue(7);
    radiusLayout->addWidget(this->_radiusData);
    otherLayout->addLayout(radiusLayout);

    QHBoxLayout *radius2Layout = new QHBoxLayout();
    radius2Layout->addWidget(new QLabel("Radius 2 :"));
    this->_radius2Data = new QDoubleSpinBox();
    this->_radius2Data->setValue(2);
    radius2Layout->addWidget(this->_radius2Data);
    otherLayout->addLayout(radius2Layout);

    QHBoxLayout *densityLayout = new QHBoxLayout();
    densityLayout->addWidget(new QLabel("Density :"));
    this->_densityData = new QDoubleSpinBox();
    this->_densityData->setValue(50);
    densityLayout->addWidget(this->_densityData);
    otherLayout->addLayout(densityLayout);

    gb->setLayout(otherLayout);
    this->_layout->addWidget(gb);
}

QWidget *TorusDescription::Create()
{
    return new TorusDescription();
}

const QString &TorusDescription::getName() const
{
    return this->_name;
}

const QString &TorusDescription::getType() const
{
    return this->_type;
}

Movable *TorusDescription::createObject()
{
    return new Form::Torus(7,2,64);
}

IWrapper *TorusDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void TorusDescription::onObjectSet()
{
    Form::Torus *torus = reinterpret_cast<Form::Torus *>(this->_object3d);
    if (torus) {
        this->_radiusData->setValue(torus->getRadius());
        this->_radius2Data->setValue(torus->getRadius2());
        this->_densityData->setValue(torus->getDensity());
    }
    this->_radiusData->connect(this->_radiusData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    this->_radius2Data->connect(this->_radius2Data, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    this->_densityData->connect(this->_densityData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
}

void TorusDescription::saveSpecialEvent() {
    Form::Torus *torus = reinterpret_cast<Form::Torus *>(this->_object3d);
    if (torus) {
        torus->setDensity(this->_densityData->value());
        torus->setRadius2(this->_radius2Data->value());
        torus->setRadius(this->_radiusData->value());
    }
}

void TorusDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void TorusDescription::materialSave(int index)
{
ABaseSettingView::materialSave(index);
}

void TorusDescription::materialViewOption()
{
ABaseSettingView::materialViewOption();
}
void TorusDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}
}

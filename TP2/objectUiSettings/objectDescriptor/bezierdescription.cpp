#include "bezierdescription.h"
#include "form/bezier.h"

namespace objectUiSettings {
BezierDescription::BezierDescription(QWidget *parent, Movable *movable): ABaseSettingView(parent, movable),
    _name("Bezier"), _type(_name)
{

}

BezierDescription::~BezierDescription()
{

}

void BezierDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = false;
    ABaseSettingView::init(options);

    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    _st = new QTabWidget;
    for (int i = 0; i < 9; ++i) {
        QGroupBox *gbPoint = new QGroupBox("Position");

        QVBoxLayout *positionLayout = new QVBoxLayout();
        positionLayout->setObjectName("positionLayout" + QString::number(i));

        QHBoxLayout *positionXLayout = new QHBoxLayout();
        positionXLayout->addWidget(new QLabel("X :"));
        QDoubleSpinBox *xPositionData = new QDoubleSpinBox();
        xPositionData->setValue(0);
        xPositionData->setRange((-(std::numeric_limits<double>::max() - (double)1)), std::numeric_limits<double>::max());
        positionXLayout->addWidget(xPositionData);
        positionLayout->addLayout(positionXLayout);

        QHBoxLayout *positionYLayout = new QHBoxLayout();
        positionYLayout->addWidget(new QLabel("Y :"));
        QDoubleSpinBox *yPositionData = new QDoubleSpinBox();
        yPositionData->setValue(0);
        yPositionData->setRange((-(std::numeric_limits<double>::max() - (double)1)), std::numeric_limits<double>::max());
        positionYLayout->addWidget(yPositionData);
        positionLayout->addLayout(positionYLayout);

        QHBoxLayout *positionZLayout = new QHBoxLayout();
        positionZLayout->addWidget(new QLabel("Z :"));
        QDoubleSpinBox *zPositionData = new QDoubleSpinBox();
        zPositionData->setValue(0);
        zPositionData->setRange((-(std::numeric_limits<double>::max() - (double)1)), std::numeric_limits<double>::max());
        positionZLayout->addWidget(zPositionData);
        positionLayout->addLayout(positionZLayout);

        std::vector<QDoubleSpinBox *> lsp;
        lsp.push_back(xPositionData);
        lsp.push_back(yPositionData);
        lsp.push_back(zPositionData);
        _lPoint.push_back(lsp);


        gbPoint->setLayout(positionLayout);
        _st->addTab(gbPoint, "Point " + QString::number(i + 1));

    }

    otherLayout->addWidget(_st);
    gb->setLayout(otherLayout);

    this->_layout->addWidget(gb);
}

QWidget *BezierDescription::Create()
{
    return new BezierDescription();
}

const QString &BezierDescription::getName() const
{
    return this->_name;
}

const QString &BezierDescription::getType() const
{
    return this->_type;
}

Movable *BezierDescription::createObject()
{
    return new Form::Bezier();
}

IWrapper *BezierDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_name);
}

void BezierDescription::onObjectSet()
{
    Form::Bezier *bezier = dynamic_cast<Form::Bezier *>(this->_object3d);
    if (bezier) {
        const int nbpoint = bezier->getNbPoints();
        const int size = nbpoint * nbpoint;
        for (int i = 0; i < size; i++) {

            const int row = i / nbpoint;
            const int col = i % nbpoint;

            QVector3D point(bezier->getPoint(row, col));

            _lPoint[i][0]->setValue(point.x());
            _lPoint[i][1]->setValue(point.y());
            _lPoint[i][2]->setValue(point.z());
            _lPoint[i][0]->connect(_lPoint[i][0], SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
            _lPoint[i][1]->connect(_lPoint[i][1], SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
            _lPoint[i][2]->connect(_lPoint[i][2], SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
        }
    }
}

void BezierDescription::saveSpecialEvent() {
    Form::Bezier *bezier = dynamic_cast<Form::Bezier *>(this->_object3d);
    if (bezier) {
        const int nbpoint = bezier->getNbPoints();
        const int size = nbpoint * nbpoint;
        for (int i = 0; i < size; i++) {

            const int row = i / nbpoint;
            const int col = i % nbpoint;

            QVector3D &point = bezier->getPoint(row, col);
            point.setX(_lPoint[i][0]->value());
            point.setY(_lPoint[i][1]->value());
            point.setZ(_lPoint[i][2]->value());
        }
        bezier->regenerate();
    }
}

void BezierDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void BezierDescription::materialSave(int index)
{
ABaseSettingView::materialSave(index);
}

void BezierDescription::materialViewOption()
{
ABaseSettingView::materialViewOption();
}

void BezierDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}

}


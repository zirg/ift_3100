#include "pyramiddescription.h"
#include "form/pyramid.h"

namespace objectUiSettings {
PyramidDescription::PyramidDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Pyramid"), _type(_name)
{

}

PyramidDescription::~PyramidDescription()
{

}

QWidget *PyramidDescription::Create()
{
    return new PyramidDescription();
}

const QString &PyramidDescription::getName() const
{
return this->_name;
}

const QString &PyramidDescription::getType() const
{
return this->_type;
}

Movable *PyramidDescription::createObject()
{
    return new Form::Pyramid();
}

IWrapper *PyramidDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void PyramidDescription::onObjectSet()
{

}

void PyramidDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

void PyramidDescription::materialSave(int i) {
    ABaseSettingView::materialSave(i);
}

void PyramidDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void PyramidDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}

}

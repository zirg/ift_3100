#ifndef OBJECTLOADERDESCRIPTION_H
#define OBJECTLOADERDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class ObjectLoaderDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit ObjectLoaderDescription(QWidget *parent = 0, Movable *movable = 0);
    ~ObjectLoaderDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    QString const &getPath() const;
    void setPath(const QString &);
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialViewOption();
    void materialSave(int);
    void colorSave(const QColor &color);
private:
    QString _name;
    QString _path;
    QString _type;
};
}
#endif // OBJECTLOADERDESCRIPTION_H

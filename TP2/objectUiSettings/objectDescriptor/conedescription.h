#ifndef CONEDESCRIPTOR_H
#define CONEDESCRIPTOR_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class ConeDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit ConeDescription(QWidget *parent = 0, Movable *movable = 0);
    ~ConeDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
    void saveSpecialEvent();
    void colorSave(const QColor &color);
private:
    QDoubleSpinBox *_densityData;
    QDoubleSpinBox *_radiusData;
    QDoubleSpinBox *_heightData;
    QString _name;
    QString _type;
};
}

#endif // CONEDESCRIPTOR_H


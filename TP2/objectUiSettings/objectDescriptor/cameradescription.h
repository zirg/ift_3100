#ifndef CAMERADESCRIPTION_H
#define CAMERADESCRIPTION_H

#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QDoubleSpinBox>
#include <QLabel>
#include "objectUiSettings/iobjectdescriptor.h"

namespace objectUiSettings {
class CameraDescription : public IObjectDescription
{
    Q_OBJECT
public:
    explicit CameraDescription(QWidget *parent = 0);
    ~CameraDescription();
public:
    static QWidget *Create();
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *getWrappedObject();
    void setObjectSettingInformation(IWrapper *wrapper);
    void init(bool init = false);
    IWrapper *CreateWrapper(Movable *obj);
    virtual void refreshDescription();
    virtual void setCamera(Camera *);
protected:
    Movable *createObject();

private:
    QDoubleSpinBox *createDoubleSpinBoxConfigured();
    void enableFieldByMode();
private:
    QString _type;
    QString _name;
    Camera *_camera;

    QVBoxLayout *_layout;

    QDoubleSpinBox *_xTranslationData;
    QDoubleSpinBox *_yTranslationData;
    QDoubleSpinBox *_zTranslationData;

    QRadioButton *_perspectiveMode;
    QRadioButton *_orthoMode;
    QDoubleSpinBox *_fovData;
    QDoubleSpinBox *_farData;
    QDoubleSpinBox *_nearData;
    QDoubleSpinBox *_leftData;
    QDoubleSpinBox *_rightData;
    QDoubleSpinBox *_topData;
    QDoubleSpinBox *_bottomData;
    bool _saveDisabled;
private slots:
    void saveEvent();
    void modeChangedEvent(bool checked);
};
}
#endif // CAMERADESCRIPTION_H

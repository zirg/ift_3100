#ifndef PYRAMIDDESCRIPTION_H
#define PYRAMIDDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {

class PyramidDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit PyramidDescription(QWidget *parent = 0, Movable *movable = 0);
    ~PyramidDescription();
public:
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
    void colorSave(const QColor &color);
private:
    QString _name;
    QString _type;
};
}

#endif // PYRAMIDDESCRIPTION_H

#include "conedescription.h"
#include "form/cone.h"

namespace objectUiSettings {
ConeDescription::ConeDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Cone"), _type(_name)
{

}

ConeDescription::~ConeDescription()
{

}

void ConeDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = true;
    ABaseSettingView::init(options);
    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    otherLayout->setObjectName("otherLayout");

    QHBoxLayout *radiusLayout = new QHBoxLayout();
    radiusLayout->addWidget(new QLabel("Radius :"));
    this->_radiusData = new QDoubleSpinBox();
    radiusLayout->addWidget(this->_radiusData);
    otherLayout->addLayout(radiusLayout);

    QHBoxLayout *heightLayout = new QHBoxLayout();
    heightLayout->addWidget(new QLabel("Height :"));
    this->_heightData = new QDoubleSpinBox();
    heightLayout->addWidget(this->_heightData);
    otherLayout->addLayout(heightLayout);

    QHBoxLayout *densityLayout = new QHBoxLayout();
    densityLayout->addWidget(new QLabel("Density :"));
    this->_densityData = new QDoubleSpinBox();
    densityLayout->addWidget(this->_densityData);
    otherLayout->addLayout(densityLayout);

    gb->setLayout(otherLayout);
    this->_layout->addWidget(gb);
}

QWidget *ConeDescription::Create()
{
    return new ConeDescription();
}

const QString &ConeDescription::getName() const
{
    return this->_name;
}

const QString &ConeDescription::getType() const
{
    return this->_type;
}

Movable *ConeDescription::createObject()
{
    return new Form::Cone(1, 7, 64);
}

IWrapper *ConeDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void ConeDescription::onObjectSet()
{
    Form::Cone *cone = dynamic_cast<Form::Cone *>(this->_object3d);
    if (cone) {
        this->_densityData->setValue(cone->getDensity());
        this->_heightData->setValue(cone->getHeight());
        this->_radiusData->setValue(cone->getRadius());
    }
    this->_radiusData->connect(this->_radiusData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    this->_heightData->connect(this->_heightData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    this->_densityData->connect(this->_densityData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
}

void ConeDescription::materialSave(int index) {
    ABaseSettingView::materialSave(index);
}

void ConeDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

void ConeDescription::saveSpecialEvent() {
    Form::Cone *cone = dynamic_cast<Form::Cone *>(this->_object3d);
    if (cone) {
        cone->setDensity(this->_densityData->value());
        cone->setHeight(this->_heightData->value());
        cone->setRadius(this->_radiusData->value());
    }
}

void ConeDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void ConeDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}

}




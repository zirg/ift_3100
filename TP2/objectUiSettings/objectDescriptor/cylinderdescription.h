#ifndef CYLINDERDESCRIPTION_H
#define CYLINDERDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class CylinderDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit CylinderDescription(QWidget *parent = 0, Movable *movable = 0);
    ~CylinderDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
    void saveSpecialEvent();
    void colorSave(const QColor &color);
private:
    QDoubleSpinBox *_densityData;
    QDoubleSpinBox *_radiusData;
    QDoubleSpinBox *_heightData;
    QString _name;
    QString _type;
};
}

#endif // CYLINDERDESCRIPTION_H

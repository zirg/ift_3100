#ifndef EARTHDESCRIPTION_H
#define EARTHDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class EarthDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit EarthDescription(QWidget *parent = 0, Movable *movable = 0);
    ~EarthDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
    void saveSpecialEvent();
    void colorSave(const QColor &color);
private:
    QString _name;
    QString _type;
};
}

#endif // EARTHDESCRIPTION_H


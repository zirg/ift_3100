#include "planedescription.h"
#include "form/plane.h"

namespace objectUiSettings {
PlaneDescription::PlaneDescription(QWidget *parent, Movable *movable): ABaseSettingView(parent, movable),
    _name("Plane"), _type(_name)
{

}

PlaneDescription::~PlaneDescription()
{

}

void PlaneDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = false;
    ABaseSettingView::init(options);
    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    otherLayout->setObjectName("otherLayout");

    QHBoxLayout *sizeLayout = new QHBoxLayout();
    sizeLayout->addWidget(new QLabel("Size :"));
    this->_sizeData = new QDoubleSpinBox();
    this->_sizeData->setRange((-(std::numeric_limits<double>::max() - (double)1)), std::numeric_limits<double>::max());
    this->_sizeData->setMaximumWidth(200);
    sizeLayout->addWidget(this->_sizeData);
    otherLayout->addLayout(sizeLayout);

    gb->setLayout(otherLayout);
    this->_layout->addWidget(gb);
}

QWidget *PlaneDescription::Create()
{
    return new PlaneDescription();
}

const QString &PlaneDescription::getName() const
{
    return this->_name;
}

const QString &PlaneDescription::getType() const
{
    return this->_type;
}

Movable *PlaneDescription::createObject()
{
    return new Form::Plane(5.0);
}

IWrapper *PlaneDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_name);
}

void PlaneDescription::onObjectSet()
{
    Form::Plane *plane = dynamic_cast<Form::Plane *>(this->_object3d);
    if (plane) {
        this->_sizeData->setValue(plane->getSize());
    }
    this->_sizeData->connect(this->_sizeData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
}

void PlaneDescription::saveSpecialEvent() {
    Form::Plane *plane = dynamic_cast<Form::Plane *>(this->_object3d);
    if (plane) {
         plane->setSize(this->_sizeData->value());
    }
}

void PlaneDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void PlaneDescription::materialSave(int index)
{
ABaseSettingView::materialSave(index);
}

void PlaneDescription::materialViewOption()
{
ABaseSettingView::materialViewOption();
}

void PlaneDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}
}

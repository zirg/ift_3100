#include "earthdescription.h"
#include "form/earth.h"

namespace objectUiSettings {
EarthDescription::EarthDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Earth"), _type(_name)
{

}

EarthDescription::~EarthDescription()
{

}

void EarthDescription::init(bool)
{
    BaseSettingOptions options;
    options.Material = false;
    ABaseSettingView::init(options);
}

QWidget *EarthDescription::Create()
{
    return new EarthDescription();
}

const QString &EarthDescription::getName() const
{
    return this->_name;
}

const QString &EarthDescription::getType() const
{
    return this->_type;
}

Movable *EarthDescription::createObject()
{
    return new Form::Earth;
}

IWrapper *EarthDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void EarthDescription::onObjectSet()
{

}

void EarthDescription::materialSave(int index) {
    ABaseSettingView::materialSave(index);
}

void EarthDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

void EarthDescription::saveSpecialEvent() {

}

void EarthDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void EarthDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}

}





#include "cameradescription.h"

namespace objectUiSettings {
CameraDescription::CameraDescription(QWidget *parent) : IObjectDescription(parent),
    _type("Camera"), _name(_type)
{
    _saveDisabled = false;
}

CameraDescription::~CameraDescription()
{

}

void CameraDescription::setCamera(Camera *camera) {
    _camera = camera;
}

QWidget *CameraDescription::Create()
{
    return new CameraDescription();
}

const QString &CameraDescription::getName() const
{
    return this->_name;
}

const QString &CameraDescription::getType() const
{
    return this->_type;
}

IWrapper *CameraDescription::getWrappedObject()
{
    return this->CreateWrapper(this->_camera);
}

void CameraDescription::setObjectSettingInformation(IWrapper *wrapper)
{
    this->_saveDisabled = true;
    this->_camera = (Camera *)wrapper->getObject();
    this->_fovData->setValue(this->_camera->getFov());
    this->_farData->setValue(this->_camera->getFar());
    this->_nearData->setValue(this->_camera->getNear());

    this->_leftData->setValue(this->_camera->getLeft());
    this->_rightData->setValue(this->_camera->getRight());
    this->_topData->setValue(this->_camera->getTop());
    this->_bottomData->setValue(this->_camera->getBottom());
    this->enableFieldByMode();

    QVector3D translation = this->_camera->getTranslation();
    this->_xTranslationData->setValue(translation.x());
    this->_yTranslationData->setValue(translation.y());
    this->_zTranslationData->setValue(translation.z());

    this->_saveDisabled = false;
}

QDoubleSpinBox *CameraDescription::createDoubleSpinBoxConfigured()
{
    QDoubleSpinBox *spinBox = new QDoubleSpinBox();
    spinBox->setValue(0);
    spinBox->setRange((-(std::numeric_limits<double>::max() - (double)1)), std::numeric_limits<double>::max());
    spinBox->setMaximumWidth(200);
    return spinBox;
}

void CameraDescription::init(bool init)
{
    QHBoxLayout *hLayout;
    this->_layout = new QVBoxLayout();

    QGroupBox *gbTrans = new QGroupBox("Translation :");
    QVBoxLayout *translationLayout = new QVBoxLayout();
    translationLayout->setObjectName("translationLayout");

    QHBoxLayout *translationXLayout = new QHBoxLayout();
    translationXLayout->addWidget(new QLabel("X :"));
    this->_xTranslationData = this->createDoubleSpinBoxConfigured();
    translationXLayout->addWidget(this->_xTranslationData);
    translationLayout->addLayout(translationXLayout);

    QHBoxLayout *translationYLayout = new QHBoxLayout();
    translationYLayout->addWidget(new QLabel("Y :"));
    this->_yTranslationData = this->createDoubleSpinBoxConfigured();
    translationYLayout->addWidget(this->_yTranslationData);
    translationLayout->addLayout(translationYLayout);

    QHBoxLayout *translationZLayout = new QHBoxLayout();
    translationZLayout->addWidget(new QLabel("Z :"));
    this->_zTranslationData = this->createDoubleSpinBoxConfigured();
    translationZLayout->addWidget(this->_zTranslationData);
    translationLayout->addLayout(translationZLayout);

    this->_xTranslationData->connect(this->_xTranslationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    this->_yTranslationData->connect(this->_yTranslationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    this->_zTranslationData->connect(this->_zTranslationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));

    gbTrans->setLayout(translationLayout);
    this->_layout->addWidget(gbTrans);

    QGroupBox *modegb = new QGroupBox("Mode :");
    QHBoxLayout *modeLayout = new QHBoxLayout();
    modeLayout->setObjectName("ModeLayout");

    this->_perspectiveMode = new QRadioButton("Perspective");
    QObject::connect(this->_perspectiveMode, SIGNAL(toggled(bool)), this, SLOT(modeChangedEvent(bool)));
    modeLayout->addWidget(this->_perspectiveMode);

    this->_orthoMode = new QRadioButton("Ortho");
    QObject::connect(this->_orthoMode, SIGNAL(toggled(bool)), this, SLOT(modeChangedEvent(bool)));
    modeLayout->addWidget(this->_orthoMode);

    modegb->setLayout(modeLayout);

    this->_layout->addWidget(modegb);

    QGroupBox *globalgb = new QGroupBox("Commun :");
    QVBoxLayout *globalLayout = new QVBoxLayout();

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Far :"));
    this->_farData = new QDoubleSpinBox();
    this->_farData->setMaximum(100000.0);
    this->_farData->setMinimum(-100000.0);
    hLayout->addWidget(this->_farData);
    QObject::connect(this->_farData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    globalLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Near :"));
    this->_nearData = new QDoubleSpinBox();
    this->_nearData->setMaximum(100000.0);
    this->_nearData->setMinimum(-100000.0);
    hLayout->addWidget(this->_nearData);
    QObject::connect(this->_nearData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    globalLayout->addLayout(hLayout);

    globalgb->setLayout(globalLayout);
    this->_layout->addWidget(globalgb);

    QGroupBox *perspectiveModegb = new QGroupBox("Perspective :");
    QVBoxLayout *perspectiveModeLayout = new QVBoxLayout();
    perspectiveModeLayout->setObjectName("perspectiveModeLayout");

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Fov :"));
    this->_fovData = new QDoubleSpinBox();
    hLayout->addWidget(this->_fovData);
    QObject::connect(this->_fovData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    perspectiveModeLayout->addLayout(hLayout);

    perspectiveModegb->setLayout(perspectiveModeLayout);
    this->_layout->addWidget(perspectiveModegb);

    QGroupBox *orthoModegb = new QGroupBox("Ortho :");
    QVBoxLayout *orthoModeLayout = new QVBoxLayout();
    orthoModeLayout->setObjectName("orthoModeLayout");

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Left :"));
    this->_leftData = new QDoubleSpinBox();
    hLayout->addWidget(this->_leftData);
    QObject::connect(this->_leftData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    orthoModeLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Right :"));
    this->_rightData = new QDoubleSpinBox();
    hLayout->addWidget(this->_rightData);
    QObject::connect(this->_rightData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    orthoModeLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Top :"));
    this->_topData = new QDoubleSpinBox();
    hLayout->addWidget(this->_topData);
    QObject::connect(this->_topData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    orthoModeLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Bottom :"));
    this->_bottomData = new QDoubleSpinBox();
    hLayout->addWidget(this->_bottomData);
    QObject::connect(this->_bottomData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    orthoModeLayout->addLayout(hLayout);

    orthoModegb->setLayout(orthoModeLayout);
    this->_layout->addWidget(orthoModegb);

    this->setLayout(this->_layout);
}

Movable *CameraDescription::createObject()
{
    return NULL;
}

void CameraDescription::enableFieldByMode()
{
    if (this->_camera->isPerspectiveMode()) {
        this->_perspectiveMode->setChecked(true);
        this->_orthoMode->setChecked(false);
        this->_fovData->setEnabled(true);
        this->_rightData->setEnabled(false);
        this->_leftData->setEnabled(false);
        this->_topData->setEnabled(false);
        this->_bottomData->setEnabled(false);
    } else {
        this->_perspectiveMode->setChecked(false);
        this->_orthoMode->setChecked(true);
        this->_fovData->setEnabled(false);
        this->_rightData->setEnabled(true);
        this->_leftData->setEnabled(true);
        this->_topData->setEnabled(true);
        this->_bottomData->setEnabled(true);
    }
}

void CameraDescription::saveEvent()
{
    if (_saveDisabled) {
        return;
    }

    QVector3D translationVector;

    translationVector.setX(this->_xTranslationData->value());
    translationVector.setY(this->_yTranslationData->value());
    translationVector.setZ(this->_zTranslationData->value());

    this->_camera->movePoint(translationVector);

    if (this->_camera->isPerspectiveMode()) {
        this->_camera->perspective(this->_fovData->value(),
                                   this->_nearData->value(),
                                   this->_farData->value());
    } else {
        this->_camera->ortho(this->_leftData->value(),
                             this->_rightData->value(),
                             this->_bottomData->value(),
                             this->_topData->value(),
                             this->_nearData->value(),
                             this->_farData->value());
    }
}

void CameraDescription::modeChangedEvent(bool checked)
{
    QRadioButton *sender = (QRadioButton *)QObject::sender();

    if (sender == this->_perspectiveMode && checked == true) {
       this->_camera->perspective(this->_fovData->value(),
                                  this->_nearData->value(),
                                  this->_farData->value());
        this->enableFieldByMode();
    } else if (sender == this->_orthoMode && checked == true){
        this->_camera->ortho(this->_leftData->value(),
                             this->_rightData->value(),
                             this->_bottomData->value(),
                             this->_topData->value(),
                             this->_nearData->value(),
                             this->_farData->value());
        this->enableFieldByMode();
    }
}

IWrapper *CameraDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void CameraDescription::refreshDescription()
{
    QVector3D translation = this->_camera->getTranslation();
    this->_xTranslationData->setValue(translation.x());
    this->_yTranslationData->setValue(translation.y());
    this->_zTranslationData->setValue(translation.z());
}
}

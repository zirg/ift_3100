#include "spheredescription.h"
#include "form/sphere.h"

namespace objectUiSettings {
SphereDescription::SphereDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Sphere"), _type(_name)
{

}

SphereDescription::~SphereDescription()
{

}

QWidget *SphereDescription::Create()
{
    return new SphereDescription();
}

const QString &SphereDescription::getName() const
{
    return this->_name;
}

const QString &SphereDescription::getType() const
{
    return this->_type;
}

void SphereDescription::materialSave(int i) {
    ABaseSettingView::materialSave(i);
}

Movable *SphereDescription::createObject()
{
    return new Form::Sphere();
}

void SphereDescription::init(bool)
{
    BaseSettingOptions options;
    ABaseSettingView::init(options);
    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    otherLayout->setObjectName("otherLayout");

    QHBoxLayout *densityLayout = new QHBoxLayout();
    densityLayout->addWidget(new QLabel("Density :"));
    this->_densityData = new QDoubleSpinBox();
    this->_densityData->setValue(50);
    densityLayout->addWidget(this->_densityData);
    otherLayout->addLayout(densityLayout);

    gb->setLayout(otherLayout);
    this->_layout->addWidget(gb);
}


IWrapper *SphereDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void SphereDescription::onObjectSet()
{
    Form::Sphere *sphere = reinterpret_cast<Form::Sphere *>(this->_object3d);
    if (sphere) {
        this->_densityData->setValue(sphere->getDensity());
    }
    this->_densityData->connect(this->_densityData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
}

void SphereDescription::saveSpecialEvent() {
    Form::Sphere *sphere = reinterpret_cast<Form::Sphere *>(this->_object3d);
    if (sphere) {
        sphere->setDensity(this->_densityData->value());
    }
}

void SphereDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

void SphereDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void SphereDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}
}

#ifndef BEZIERDESCRIPTION
#define BEZIERDESCRIPTION

#include <vector>
#include <QTabWidget>

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class BezierDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit BezierDescription(QWidget *parent = 0, Movable *movable = 0);
    ~BezierDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void saveSpecialEvent();
    void materialSave(int);
    void materialViewOption();
    void colorSave(const QColor &color);
private:
    QString _name;
    QString _type;
    QTabWidget *_st;
    std::vector< std::vector<QDoubleSpinBox *>> _lPoint;
};
}

#endif // BEZIERDESCRIPTION


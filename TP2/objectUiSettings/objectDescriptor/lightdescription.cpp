#include "lightdescription.h"
#include "objectUiSettings/lightwrapper.h"

namespace objectUiSettings {
LightDescription::LightDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Light"), _lightType(Light::AMBIANT), _type(_name)
{
    _lightTypes.push_back("Ambient");
    _lightTypes.push_back("Directional");
    _lightTypes.push_back("Point");
    _lightTypes.push_back("Spot");
    _saveDisabled = false;
}

LightDescription::~LightDescription()
{

}

QWidget *LightDescription::Create()
{
    return new LightDescription();
}

void LightDescription::init(bool)
{
    BaseSettingOptions options;
    options.Color = false;
    options.Scale = false;
    options.Material = false;
    options.Focus = false;
    ABaseSettingView::init(options);

    this->_colorDialog = new QColorDialog(QColor(255, 255, 255));

    QGroupBox *colorGb = new QGroupBox("Colors :");
    QVBoxLayout *colorLayout = new QVBoxLayout();
    colorLayout->setObjectName("colorLayout");
    QHBoxLayout *hLayout = new QHBoxLayout();

    hLayout->addWidget(new QLabel("Ambient :"));
    this->_ambientColorLabel = new ClickableLabel();
    this->_ambientColorLabel->setAutoFillBackground(true);
    this->_ambientColorLabel->connect(this->_ambientColorLabel , SIGNAL(clicked()), this, SLOT(openColorDialog()));
    this->_ambientColorLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;"));
    hLayout->addWidget(this->_ambientColorLabel);
    colorLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Diffuse :"));
    this->_diffuseColorLabel = new ClickableLabel();
    this->_diffuseColorLabel->setAutoFillBackground(true);
    this->_diffuseColorLabel->connect(this->_diffuseColorLabel , SIGNAL(clicked()), this, SLOT(openColorDialog()));
    this->_diffuseColorLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;"));
    hLayout->addWidget(this->_diffuseColorLabel);
    colorLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel("Specular :"));
    this->_specularColorLabel = new ClickableLabel();
    this->_specularColorLabel->setAutoFillBackground(true);
    this->_specularColorLabel->connect(this->_specularColorLabel , SIGNAL(clicked()), this, SLOT(openColorDialog()));
    this->_specularColorLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;"));
    hLayout->addWidget(this->_specularColorLabel);
    colorLayout->addLayout(hLayout);

    colorGb->setLayout(colorLayout);
    this->_layout->addWidget(colorGb);

    QGroupBox *gb = new QGroupBox("Others :");
    this->_otherLayout = new QVBoxLayout();
    this->_otherLayout->setObjectName("otherLayout");

    QHBoxLayout *intensityLayout = new QHBoxLayout();
    intensityLayout->addWidget(new QLabel("Intensity :"));
    this->_intensityData = new QDoubleSpinBox();
    this->_intensityData->setRange(0, 1);
    this->_intensityData->setSingleStep(0.1);    
    QObject::connect(this->_intensityData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    intensityLayout->addWidget(this->_intensityData);
    this->_otherLayout->addLayout(intensityLayout);

    //this->initCustomField();

    gb->setLayout(this->_otherLayout);
    this->_layout->addWidget(gb);
}

void LightDescription::setType(Light::eLight type)
{
    this->_lightType = type;
}

const QString &LightDescription::getName() const
{
    return this->_name;
}

const QString &LightDescription::getType() const
{
    return this->_type;
}

const QString &LightDescription::getLightType() const {
    return _lightTypes[this->_lightType];
}

Movable *LightDescription::createObject()
{
    Light *light = new Light(this->_lightType);
    QVector4D vc(1.0, 1.0, 1.0, 0);
    light->setAmbiant(vc);
    light->setDiffuse(vc);
    light->setSpecular(vc);
    light->setIntensity(1.0);
    return light;
}

IWrapper *LightDescription::CreateWrapper(Movable *obj)
{
    return new LightWrapper(obj);
}

void LightDescription::onObjectSet()
{
    _saveDisabled = true;
    Light *light = reinterpret_cast<Light *>(this->_object3d);
    this->setType(light->getType());
    QVector4D vc4Ambient(light->getAmbiant());
    QVector4D vc4Diffuse(light->getDiffuse());
    QVector4D vc4Specular(light->getSpecular());

    QColor colorAmbient(vc4Ambient.x() * 255.0, vc4Ambient.y() * 255.0, vc4Ambient.z() * 255.0, 1.0);
    QColor colorDiffuse(vc4Diffuse.x() * 255.0, vc4Diffuse.y() * 255.0, vc4Diffuse.z() * 255.0, 1.0);
    QColor colorSpecular(vc4Specular.x() * 255.0, vc4Specular.y() * 255.0, vc4Specular.z() * 255.0, 1.0);
    this->_ambientColor = colorAmbient;
    this->_ambientColorLabel->setStyleSheet(QString::fromStdString("border: 2px solid black; background-color: " + _ambientColor.name().toStdString() + ";"));
    this->_diffuseColor = colorDiffuse;
    this->_diffuseColorLabel->setStyleSheet(QString::fromStdString("border: 2px solid black; background-color: " + _diffuseColor.name().toStdString() + ";"));
    this->_specularColor = colorSpecular;
    this->_specularColorLabel->setStyleSheet(QString::fromStdString("border: 2px solid black; background-color: " + _specularColor.name().toStdString() + ";"));

    this->_intensityData->setValue(light->getIntensity());

    if (this->_lightType == Light::AMBIANT || this->_lightType == Light::POINT) {
        _gbRot->hide();
        if (this->_lightType == Light::AMBIANT) {
            _gbTrans->hide();
        }
    }

    this->initCustomField();
    if (this->_lightType == Light::POINT || this->_lightType == Light::SPOT) {
        if (this->_lightType == Light::SPOT) {
            this->_cutoffData->setValue(light->getCutoff());
            this->_exponentData->setValue(light->getExponent());
        }
        this->_constant_attData->setValue(light->getConstantAtt());
        this->_linear_attData->setValue(light->getLinearAtt());
        this->_quadratic_attData->setValue(light->getQuadraticAtt());
    }
    _saveDisabled = false;
}

void LightDescription::openColorDialog() {
    ClickableLabel *sender = (ClickableLabel *)QObject::sender();
    QColor *currentColor;
    if (sender == this->_ambientColorLabel) {
        currentColor = &(this->_ambientColor);
    }else if (sender == this->_diffuseColorLabel) {
        currentColor = &(this->_diffuseColor);
    } else if (sender == this->_specularColorLabel) {
        currentColor = &(this->_specularColor);
    } else {
        QMessageBox::information(this, tr("Information"), tr("property not found"));
        return ;
    }
    QColor result = this->_colorDialog->getColor(*currentColor);
    if (!result.isValid()) {
        return;
    }
    sender->setStyleSheet(QString::fromStdString("border: 2px solid black;  background-color: " + result.name().toStdString() + ";"));
    *(currentColor) = result;
    this->saveEvent();
}

void LightDescription::initCustomField()
{
    QHBoxLayout *hLayout;
    if (this->_lightType == Light::POINT || this->_lightType == Light::SPOT) {
        hLayout = new QHBoxLayout();
        hLayout->addWidget(new QLabel("Constant att :"));
        this->_constant_attData = new QDoubleSpinBox();
        this->_constant_attData->setRange(0,1);
        this->_constant_attData->setSingleStep(0.1);
        QObject::connect(this->_constant_attData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        hLayout->addWidget(this->_constant_attData);
        this->_otherLayout->addLayout(hLayout);

        hLayout = new QHBoxLayout();
        hLayout->addWidget(new QLabel("Linear att :"));
        this->_linear_attData = new QDoubleSpinBox();
        this->_linear_attData->setRange(0,1);
        this->_linear_attData->setSingleStep(0.1);
        QObject::connect(this->_linear_attData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        hLayout->addWidget(this->_linear_attData);
        this->_otherLayout->addLayout(hLayout);

        hLayout = new QHBoxLayout();
        hLayout->addWidget(new QLabel("Quadratic att :"));
        this->_quadratic_attData = new QDoubleSpinBox();
        this->_quadratic_attData->setRange(0,1);
        this->_quadratic_attData->setSingleStep(0.1);
        QObject::connect(this->_quadratic_attData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        hLayout->addWidget(this->_quadratic_attData);
        this->_otherLayout->addLayout(hLayout);

        if (this->_lightType == Light::SPOT) {
            hLayout = new QHBoxLayout();
            hLayout->addWidget(new QLabel("Cutoff :"));
            this->_cutoffData = new QDoubleSpinBox();
            this->_cutoffData->setRange(0,90);
            this->_cutoffData->setSingleStep(1);
            QObject::connect(this->_cutoffData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
            hLayout->addWidget(this->_cutoffData);
            this->_otherLayout->addLayout(hLayout);

            hLayout = new QHBoxLayout();
            hLayout->addWidget(new QLabel("Exponent :"));
            this->_exponentData = new QDoubleSpinBox();
            this->_exponentData->setRange(0, std::numeric_limits<double>::max());
            this->_exponentData->setSingleStep(1);
            QObject::connect(this->_exponentData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
            hLayout->addWidget(this->_exponentData);
            this->_otherLayout->addLayout(hLayout);

        }
    }
}

void LightDescription::saveEvent()
{
    if (_saveDisabled) {
        return;
    }
    Light *light = reinterpret_cast<Light *>(this->_object3d);

    QVector4D colorAmbient(_ambientColor.red() / 255.0, _ambientColor.green() / 255.0, _ambientColor.blue() / 255.0, 1.0);
    light->setAmbiant(colorAmbient);
    QVector4D colorDiffuse(_diffuseColor.red() / 255.0, _diffuseColor.green() / 255.0, _diffuseColor.blue() / 255.0, 1.0);
    light->setDiffuse(colorDiffuse);
    QVector4D colorSpecular(_specularColor.red() / 255.0, _specularColor.green() / 255.0, _specularColor.blue() / 255.0, 1.0);
    light->setSpecular(colorSpecular);

    light->setIntensity(_intensityData->value());

    if (this->_lightType == Light::POINT || this->_lightType == Light::SPOT) {
        if (this->_lightType == Light::SPOT) {
            light->setExponent(_exponentData->value());
            light->setCutoff(_cutoffData->value());
        }
        light->setQuadraticAtt(_quadratic_attData->value());
        light->setLinearAtt(_linear_attData->value());
        light->setConstantAtt(_constant_attData->value());
    }

    ABaseSettingView::saveEvent();
}

}

#include "triangledescription.h"
#include "form/triangle.h"

namespace objectUiSettings {
TriangleDescription::TriangleDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Triangle"), _type(_name)
{

}

TriangleDescription::~TriangleDescription()
{

}

void TriangleDescription::init()
{
    ABaseSettingView::init();
}

QWidget *TriangleDescription::Create()
{
    return new TriangleDescription();
}

const QString &TriangleDescription::getName() const
{
    return this->_name;
}

const QString &TriangleDescription::getType() const
{
    return this->_type;
}

Movable *TriangleDescription::createObject()
{
    return new Form::Triangle();
}

IWrapper *TriangleDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void TriangleDescription::onObjectSet()
{

}

void TriangleDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

void TriangleDescription::materialSave(int i) {
    ABaseSettingView::materialSave(i);
}

void TriangleDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void TriangleDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}

}

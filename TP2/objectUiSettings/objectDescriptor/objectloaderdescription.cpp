#include "objectloaderdescription.h"
#include <QFileDialog>
#include "objloader.h"

namespace objectUiSettings {
ObjectLoaderDescription::ObjectLoaderDescription(QWidget *parent, Movable *movable): ABaseSettingView(parent, movable),
    _name("Loader"), _type(_name)
{

}

ObjectLoaderDescription::~ObjectLoaderDescription()
{

}

void ObjectLoaderDescription::init(bool state)
{
    if (!state) {
        this->_path = QFileDialog::getOpenFileName(NULL, tr("Object file"), ".", "Obj Files(*.obj)");
        this->_name = this->_path.mid(this->_path.lastIndexOf('/') + 1);
    }
    BaseSettingOptions options;
    options.Color = false;
    options.Video = false;
    ABaseSettingView::init(options);
}

const QString &ObjectLoaderDescription::getPath() const {
    return this->_path;
}

QWidget *ObjectLoaderDescription::Create()
{
    return new ObjectLoaderDescription();
}

const QString &ObjectLoaderDescription::getName() const
{
    return this->_name;
}

const QString &ObjectLoaderDescription::getType() const
{
    return this->_type;
}

IWrapper *ObjectLoaderDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void ObjectLoaderDescription::onObjectSet()
{
}

void ObjectLoaderDescription::materialSave(int) {
    ABaseSettingView::materialSave(0);
}

void ObjectLoaderDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

void ObjectLoaderDescription::setPath(const QString &path) {
    _path = path;
}

void ObjectLoaderDescription::saveEvent()
{
   ABaseSettingView::saveEvent();
}

Movable *ObjectLoaderDescription::createObject() {
    ObjLoader loader;
    Object3D *objLoaded = loader.load(this->_path.toStdString());
    return objLoaded;
}

void ObjectLoaderDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}
}



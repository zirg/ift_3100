#ifndef PLANEDESCRIPTION_H
#define PLANEDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class PlaneDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit PlaneDescription(QWidget *parent = 0, Movable *movable = 0);
    ~PlaneDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void saveSpecialEvent();
    void materialSave(int);
    void materialViewOption();
    void colorSave(const QColor &color);
private:
    QDoubleSpinBox *_sizeData;
    QString _name;
    QString _type;
};
}
#endif // PLANEDESCRIPTION_H

#include "cubedescription.h"
#include "form/cube.h"

namespace objectUiSettings {
CubeDescription::CubeDescription(QWidget *parent, Movable *movable) : ABaseSettingView(parent, movable),
    _name("Cube"), _type(_name)
{
}

CubeDescription::~CubeDescription()
{

}

void CubeDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = false;
    ABaseSettingView::init(options);

    QGroupBox *gb = new QGroupBox("Others :");
    QHBoxLayout *scaleLayout = new QHBoxLayout();
    scaleLayout->setObjectName("scaleLayout");
    scaleLayout->addWidget(new QLabel("Scale :"));
    this->_scaleData = new QDoubleSpinBox();
    QObject::connect(this->_scaleData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    scaleLayout->addWidget(this->_scaleData);
    gb->setLayout(scaleLayout);

    this->_layout->addWidget(gb);
}

void CubeDescription::materialSave(int index) {
    ABaseSettingView::materialSave(index);
}

QWidget *CubeDescription::Create()
{
    return new CubeDescription();
}

Movable *CubeDescription::createObject()
{
    return new Form::Cube();
}

IWrapper *CubeDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void CubeDescription::refreshDescription()
{
    ABaseSettingView::refreshDescription();
    QVector3D scale = this->_object3d->getScale();

    this->_scaleData->setValue(scale.x());
}

void CubeDescription::onObjectSet()
{
    QVector3D scale = this->_object3d->getScale();

    this->_scaleData->setValue(scale.x());
}

void CubeDescription::colorSave(const QColor &color, int /*type*/) {
    ABaseSettingView::colorSave(color);
}

void CubeDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
    if (this->_scaleData->value() > 0.0f) {
        QVector3D scaleVector;

        scaleVector.setX(this->_scaleData->value());
        scaleVector.setY(this->_scaleData->value());
        scaleVector.setZ(this->_scaleData->value());

        this->_object3d->resize(scaleVector);
    }
}

void CubeDescription::materialViewOption() {
    ABaseSettingView::materialViewOption();
}

const QString &CubeDescription::getName() const
{
    return this->_name;
}

const QString &CubeDescription::getType() const
{
    return this->_type;
}
}

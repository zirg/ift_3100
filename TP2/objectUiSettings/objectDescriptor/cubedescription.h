#ifndef CUBEDESCRIPTION_H
#define CUBEDESCRIPTION_H

#include <QWidget>
#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class CubeDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit CubeDescription(QWidget *parent = 0, Movable *movable = 0);
    ~CubeDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
    virtual void refreshDescription();

protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void colorSave(const QColor &color, int type);
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
private:
    QString _name;
    QString _type;
    QDoubleSpinBox *_scaleData;
};
}

#endif // CUBEDESCRIPTION_H

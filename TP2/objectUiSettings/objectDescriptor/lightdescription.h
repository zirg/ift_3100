#ifndef LIGHTDESCRIPTION_H
#define LIGHTDESCRIPTION_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QColorDialog>
#include <QMessageBox>
#include "clickablelabel.h"
#include "light.h"
#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class LightDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit LightDescription(QWidget *parent = 0, Movable *movable = 0);
    ~LightDescription();
public:
    static QWidget *Create();
    void init(bool);
    void setType(Light::eLight type);
    QString const &getLightType() const;
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void openColorDialog();
private:
    void initCustomField();
private:
    QString         _name;
    Light::eLight   _lightType;
    QStringList     _lightTypes;
    QString         _type;

    QColor          _ambientColor;
    QColor          _diffuseColor;
    QColor          _specularColor;
    ClickableLabel  *_ambientColorLabel;
    ClickableLabel  *_diffuseColorLabel;
    ClickableLabel  *_specularColorLabel;

    // ----
    QDoubleSpinBox  *_constant_attData; // [0.0, 1.0] Point + Spot
    QDoubleSpinBox  *_linear_attData; // [0.0, 1.0] Point + Spot
    QDoubleSpinBox  *_quadratic_attData; // [0.0, 1.0] Point + Spot
    QDoubleSpinBox  *_cutoffData; // [0.0, 90.0] Spot
    QDoubleSpinBox  *_exponentData; // [0.0, +inf] Spot
    // ----

    QVBoxLayout     *_otherLayout;
    QDoubleSpinBox  *_intensityData;
    QColorDialog    *_colorDialog;
    bool            _saveDisabled;
};
}
#endif // LIGHTDESCRIPTION_H

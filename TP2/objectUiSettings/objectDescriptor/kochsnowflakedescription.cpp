#include "kochsnowflakedescription.h"
#include "form/kochsnowflake.h"

namespace objectUiSettings {
KochsnowflakeDescription::KochsnowflakeDescription(QWidget *parent, Movable *movable): ABaseSettingView(parent, movable),
    _name("Kochsnowflake"), _type(_name)
{

}

KochsnowflakeDescription::~KochsnowflakeDescription()
{

}

void KochsnowflakeDescription::init(bool)
{
    BaseSettingOptions options;
    options.Scale = true;
    ABaseSettingView::init(options);
    QGroupBox *gb = new QGroupBox("Others :");
    QVBoxLayout *otherLayout = new QVBoxLayout();
    otherLayout->setObjectName("otherLayout");

    QHBoxLayout *sizeLayout = new QHBoxLayout();
    sizeLayout->addWidget(new QLabel("Size :"));
    this->_sizeData = new QDoubleSpinBox();
    sizeLayout->addWidget(this->_sizeData);
    otherLayout->addLayout(sizeLayout);

    QHBoxLayout *gLayout = new QHBoxLayout();
    gLayout->addWidget(new QLabel("Factor :"));
    this->_gData = new QSpinBox();
    this->_gData->setMaximum(9);
    gLayout->addWidget(this->_gData);
    otherLayout->addLayout(gLayout);

    gb->setLayout(otherLayout);
    this->_layout->addWidget(gb);
}

QWidget *KochsnowflakeDescription::Create()
{
    return new KochsnowflakeDescription();
}

const QString &KochsnowflakeDescription::getName() const
{
    return this->_name;
}

const QString &KochsnowflakeDescription::getType() const
{
    return this->_type;
}

Movable *KochsnowflakeDescription::createObject()
{
    return new Form::KochSnowflake(3, 3);
}

IWrapper *KochsnowflakeDescription::CreateWrapper(Movable *obj)
{
    return new ObjectWrapper(obj, this->_type);
}

void KochsnowflakeDescription::onObjectSet()
{
    Form::KochSnowflake *kock = dynamic_cast<Form::KochSnowflake *>(this->_object3d);
    if (kock) {
        this->_sizeData->setValue(kock->getSize());
        this->_gData->setValue(kock->getFactor());
    }
    this->_sizeData->connect(this->_sizeData, SIGNAL(valueChanged(double)), this, SLOT(saveSpecialEvent()));
    this->_gData->connect(this->_gData, SIGNAL(valueChanged(int)), this, SLOT(saveSpecialEvent()));
}

void KochsnowflakeDescription::saveEvent()
{
    ABaseSettingView::saveEvent();
}

void KochsnowflakeDescription::saveSpecialEvent() {
    Form::KochSnowflake *kock = dynamic_cast<Form::KochSnowflake *>(this->_object3d);
    if (kock) {
        kock->setFactor(this->_gData->value());
        kock->setSize(this->_sizeData->value());
    }
}

void KochsnowflakeDescription::materialSave(int index)
{
ABaseSettingView::materialSave(index);
}

void KochsnowflakeDescription::materialViewOption()
{
ABaseSettingView::materialViewOption();
}

void KochsnowflakeDescription::colorSave(const QColor &color) {
    ABaseSettingView::colorSave(color);
}
}


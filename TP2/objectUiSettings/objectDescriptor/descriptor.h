#ifndef DESCRIPTOR
#define DESCRIPTOR

#include "cubedescription.h"
#include "lightdescription.h"
#include "triangledescription.h"
#include "spheredescription.h"
#include "pyramiddescription.h"
#include "objectloaderdescription.h"
#include "conedescription.h"
#include "cylinderdescription.h"
#include "torusdescription.h"
#include "kochsnowflakedescription.h"
#include "planedescription.h"
#include "cameradescription.h"
#include "bezierdescription.h"
#include "earthdescription.h"
#include "../sceneconfiguration.h"

#endif // DESCRIPTOR


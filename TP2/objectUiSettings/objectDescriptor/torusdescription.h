#ifndef TORUSDESCRIPTION_H
#define TORUSDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class TorusDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit TorusDescription(QWidget *parent = 0, Movable *movable = 0);
    ~TorusDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void saveSpecialEvent();
    void materialSave(int);
    void materialViewOption();
    void colorSave(const QColor &color);
private:
    QDoubleSpinBox *_densityData;
    QDoubleSpinBox *_radiusData;
    QDoubleSpinBox *_radius2Data;
    QString _name;
    QString _type;
};
}

#endif // TORUSDESCRIPTION_H

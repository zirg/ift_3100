#ifndef KOCHSNOWFLAKEDESCRIPTION_H
#define KOCHSNOWFLAKEDESCRIPTION_H

#include <QSpinBox>
#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class KochsnowflakeDescription: public ABaseSettingView
{
    Q_OBJECT
public:
    explicit KochsnowflakeDescription(QWidget *parent = 0, Movable *movable = 0);
    ~KochsnowflakeDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
    void saveSpecialEvent();
    void colorSave(const QColor &color);
private:
    QDoubleSpinBox *_sizeData;
    QSpinBox *_gData;
    QString _name;
    QString _type;
};
}
#endif // KOCHSNOWFLAKEDESCRIPTION_H

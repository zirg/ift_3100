#ifndef SPHEREDESCRIPTION_H
#define SPHEREDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class SphereDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit SphereDescription(QWidget *parent = 0, Movable *movable = 0);
    ~SphereDescription();
public:
    void init(bool);
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void saveSpecialEvent();
    void materialSave(int);
    void materialViewOption();
    void colorSave(const QColor &color);
private:
    QDoubleSpinBox *_densityData;
    QString _name;
    QString _type;
};
}

#endif // SPHEREDESCRIPTION_H

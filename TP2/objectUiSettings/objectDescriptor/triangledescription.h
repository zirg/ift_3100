#ifndef TRIANGLEDESCRIPTION_H
#define TRIANGLEDESCRIPTION_H

#include "objectUiSettings/abasesettingview.h"

namespace objectUiSettings {
class TriangleDescription : public ABaseSettingView
{
    Q_OBJECT
public:
    explicit TriangleDescription(QWidget *parent = 0, Movable *movable = 0);
    ~TriangleDescription();
public:
    void init();
    static QWidget *Create();
public:
    QString const &getName() const;
    QString const &getType() const;
    IWrapper *CreateWrapper(Movable *obj);
protected:
    Movable *createObject();
    void onObjectSet();
protected slots:
    void saveEvent();
    void materialSave(int);
    void materialViewOption();
    void colorSave(const QColor &color);
private:
    QString _name;
    QString _type;
};
}
#endif // TRIANGLEDESCRIPTION_H

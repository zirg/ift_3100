include(objectDescriptor/objectDescriptor.pri)

HEADERS += $$PWD/objectcontainer.h \
    $$PWD/iobjectdescriptor.h \
    $$PWD/abasesettingview.h \
    $$PWD/objectwrapper.h \
    $$PWD/lightwrapper.h \
    $$PWD/iwrapper.h \
    $$PWD/config.h \
    $$PWD/sceneconfiguration.h \
    $$PWD/config.d.h

SOURCES += $$PWD/objectcontainer.cpp \
    $$PWD/abasesettingview.cpp \
    $$PWD/objectwrapper.cpp \
    $$PWD/lightwrapper.cpp \
    $$PWD/sceneconfiguration.cpp

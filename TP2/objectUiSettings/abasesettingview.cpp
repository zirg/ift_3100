#include <QFileDialog>
#include <QCheckBox>
#include <QPushButton>
#include <QSignalMapper>

#include "form/primitive.h"
#include "abasesettingview.h"

namespace objectUiSettings {
ABaseSettingView::ABaseSettingView(QWidget *parent, Movable *movable) : IObjectDescription(parent), _view(0), _camera(0), _object3d(movable)
{

}

ABaseSettingView::~ABaseSettingView()
{
}

void ABaseSettingView::init(bool)
{
    this->init(this->_options);
}

void ABaseSettingView::setCamera(Camera *camera) {
    _view = camera;
}

IWrapper *ABaseSettingView::getWrappedObject()
{
    if (this->_object3d == NULL) {
        this->_object3d = this->createObject();
    }
    return this->CreateWrapper(this->_object3d);
}

void ABaseSettingView::setObjectSettingInformation(IWrapper *wrapper)
{
    this->_object3d = wrapper->getObject();
    if (this->_options.Translation) {
        this->translationEdit();
        this->_xTranslationData->connect(this->_xTranslationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        this->_yTranslationData->connect(this->_yTranslationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        this->_zTranslationData->connect(this->_zTranslationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    }
    if (this->_options.Rotation) {
        this->rotationEdit();
        this->_xRotationData->connect(this->_xRotationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        this->_yRotationData ->connect(this->_yRotationData , SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        this->_zRotationData ->connect(this->_zRotationData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    }
    if (this->_options.Scale) {
        this->scaleEdit();
        this->_xScaleData->connect(this->_xScaleData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        this->_yScaleData->connect(this->_yScaleData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
        this->_zScaleData->connect(this->_zScaleData, SIGNAL(valueChanged(double)), this, SLOT(saveEvent()));
    }
    if (this->_options.Color) {
        this->colorEdit();
        this->_colorAmbianteLabel->connect(this->_colorAmbianteLabel , SIGNAL(clicked()), this->_colorAmbianteDialog , SLOT(open()));
        this->connect(this->_colorAmbianteDialog, &QColorDialog::colorSelected, [this](const QColor& arg) {
            ABaseSettingView::colorSave(arg, 1);
        });
        this->_colorDiffuseLabel->connect(this->_colorDiffuseLabel , SIGNAL(clicked()), this->_colorDiffuseDialog , SLOT(open()));
        this->connect(this->_colorDiffuseDialog, &QColorDialog::colorSelected, [this](const QColor& arg) {
            ABaseSettingView::colorSave(arg, 2);
        });
        this->_colorSpecularLabel->connect(this->_colorSpecularLabel , SIGNAL(clicked()), this->_colorSpecularDialog , SLOT(open()));
        this->connect(this->_colorSpecularDialog, &QColorDialog::colorSelected, [this](const QColor& arg) {
            ABaseSettingView::colorSave(arg, 3);
        });
    }
    if (this->_options.Material) {
        this->materialEdit();
        this->materialViewOption();
        _materialCb->connect(_materialCb, SIGNAL(currentIndexChanged(int)), this, SLOT(materialViewOption()));
        _materialCb->connect(_materialCb, SIGNAL(activated(int)), this, SLOT(materialSave(int)));
        _textureButton->connect(_textureButton, SIGNAL(clicked()), _textureFileDialog, SLOT(show()));
        _textureFileDialog->connect(_textureFileDialog, SIGNAL(finished(int)), this, SLOT(materialSave(int)));
        _textureNButton->connect(_textureNButton, SIGNAL(clicked()), _textureNFileDialog, SLOT(show()));
        _textureNFileDialog->connect(_textureNFileDialog, SIGNAL(finished(int)), this, SLOT(materialSave(int)));
        _shinenessBox->connect(_shinenessBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this, &ABaseSettingView::materialSave);
    }
    this->onObjectSet();
}

void ABaseSettingView::refreshDescription()
{
    if (this->_options.Translation)
    {
        QVector3D translation = this->_object3d->getTranslation();
        this->_xTranslationData->setValue(translation.x());
        this->_yTranslationData->setValue(translation.y());
        this->_zTranslationData->setValue(translation.z());
    }

    if (this->_options.Rotation)
    {
        QVector3D rotation = this->_object3d->getRotation();
        this->_xRotationData->setValue(rotation.x());
        this->_yRotationData->setValue(rotation.y());
        this->_zRotationData->setValue(rotation.z());
    }

    if (this->_options.Scale)
    {
        QVector3D scale = this->_object3d->getScale();
        this->_xScaleData->setValue(scale.x());
        this->_yScaleData->setValue(scale.y());
        this->_zScaleData->setValue(scale.z());
    }
}

QDoubleSpinBox *ABaseSettingView::createDoubleSpinBoxConfigured()
{
    QDoubleSpinBox *spinBox = new QDoubleSpinBox();
    spinBox->setValue(0);
    spinBox->setRange((-(std::numeric_limits<double>::max() - (double)1)), std::numeric_limits<double>::max());
    spinBox->setMaximumWidth(200);
    return spinBox;
}

void ABaseSettingView::init(const BaseSettingOptions &options)
{
    this->_options = options;
    this->_layout = new QVBoxLayout();

    if (this->_options.Translation) { this->translationView(); }
    if (this->_options.Rotation) { this->rotationView(); }
    if (this->_options.Scale) { this->scaleView(); }
    if (this->_options.Color) { this->colorView(); }
    if (this->_options.Material) { this->materialView(); }

    this->setLayout(this->_layout);
}

void ABaseSettingView::colorView() {
    _gbColor = new QGroupBox("Color :");
    QVBoxLayout *colorLayout = new QVBoxLayout();

    QHBoxLayout *ambianteLayout = new QHBoxLayout();
    this->_colorAmbianteDialog = new QColorDialog(QColor(255, 255, 255));
    this->_colorAmbianteLabel = new ClickableLabel();
    ambianteLayout->addWidget(new QLabel("Ambient :"));
    this->_colorAmbianteLabel->setAutoFillBackground(true);
    this->_colorAmbianteLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;"));
    ambianteLayout->addWidget(this->_colorAmbianteLabel);
    colorLayout->addLayout(ambianteLayout);


    QHBoxLayout *diffuseLayout = new QHBoxLayout();
    this->_colorDiffuseDialog = new QColorDialog(QColor(255, 255, 255));
    this->_colorDiffuseLabel = new ClickableLabel();
    diffuseLayout->addWidget(new QLabel("Diffuse :"));
    this->_colorDiffuseLabel->setAutoFillBackground(true);
    this->_colorDiffuseLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;"));
    diffuseLayout->addWidget(this->_colorDiffuseLabel);
    colorLayout->addLayout(diffuseLayout);


    QHBoxLayout *specularLayout = new QHBoxLayout();
    this->_colorSpecularDialog = new QColorDialog(QColor(255, 255, 255));
    this->_colorSpecularLabel = new ClickableLabel();
    specularLayout->addWidget(new QLabel("Specular :"));
    this->_colorSpecularLabel->setAutoFillBackground(true);
    this->_colorSpecularLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;"));
    specularLayout->addWidget(this->_colorSpecularLabel);
    colorLayout->addLayout(specularLayout);


    _gbColor->setLayout(colorLayout);
    this->_layout->addWidget(_gbColor);
}

void ABaseSettingView::materialView() {
    _gbMaterial = new QGroupBox("Material :");


    QVBoxLayout *materialLayout = new QVBoxLayout();
    materialLayout->setObjectName("materialLayout");

    if (_options.Video)  {
        QHBoxLayout *videoLayout = new QHBoxLayout();
        videoLayout->addWidget(new QLabel("Video :"));
        _checkVideo = new QCheckBox();
        _checkVideo->connect(_checkVideo, SIGNAL(stateChanged(int)), this, SLOT(materialSave(int)));
        videoLayout->addWidget(_checkVideo);
        materialLayout->addLayout(videoLayout);
    }


    QHBoxLayout *typeLayout = new QHBoxLayout();
    typeLayout->addWidget(new QLabel("Type :"));
    _materialCb = new QComboBox();

    _materialCb->addItem("None", QVariant::fromValue(Material::BASE_MATERIAL));
    _materialCb->addItem("Point", QVariant::fromValue(Material::POINT_MATERIAL));
    _materialCb->addItem("Line", QVariant::fromValue(Material::LINE_MATERIAL));
    _materialCb->addItem("Lambert", QVariant::fromValue(Material::LAMBERT_MATERIAL));
    _materialCb->addItem("Phong", QVariant::fromValue(Material::PHONG_MATERIAL));
    _materialCb->addItem("Toon", QVariant::fromValue(Material::TOON_MATERIAL));
    _materialCb->addItem("Texture", QVariant::fromValue(Material::TEXTURE_MATERIAL));
    _materialCb->addItem("Normal Mapping", QVariant::fromValue(Material::NORMAL_MAP_MATERIAL));
    _materialCb->addItem("Reflection", QVariant::fromValue(Material::CUBE_MAP_REFLECTION));
    typeLayout->addWidget(_materialCb);
    materialLayout->addLayout(typeLayout);

    QHBoxLayout *shinenessLayout = new QHBoxLayout();
    _shinenessLabel = new QLabel("Shineness :");
    shinenessLayout->addWidget(_shinenessLabel);
    _shinenessBox= new QDoubleSpinBox();
    _shinenessBox->setRange(1.0, 128.0);
    _shinenessBox->setSingleStep(0.1);
    shinenessLayout->addWidget(_shinenessBox);
    materialLayout->addLayout(shinenessLayout);


    QHBoxLayout *textureLayout = new QHBoxLayout();
    _textureLabel = new QLabel("Texture :");
    textureLayout->addWidget(_textureLabel);
    _textureButton = new QPushButton("Load");
    _textureFileDialog = new QFileDialog(0, "Texture", ".", tr("Images (*.png *.jpg)"));
    textureLayout->addWidget(_textureButton);
    materialLayout->addLayout(textureLayout);


    QHBoxLayout *textureNLayout = new QHBoxLayout();
    _textureNLabel = new QLabel("Normal :");
    textureNLayout->addWidget(_textureNLabel);
    _textureNButton = new QPushButton("Load");
    _textureNFileDialog = new QFileDialog(0, "Normal Texture", ".", tr("Images (*.png *.jpg)"));
    textureNLayout->addWidget(_textureNButton);
    materialLayout->addLayout(textureNLayout);

    QHBoxLayout *textureRLayout = new QHBoxLayout();
    _textureRLabel = new QLabel("Repeat :");
    textureRLayout->addWidget(_textureRLabel);
    this->_repeatData = new QSpinBox();
    this->_repeatData->setValue(1.0);
    textureRLayout->addWidget(this->_repeatData);
    materialLayout->addLayout(textureRLayout);

    _gbMaterial->setLayout(materialLayout);
    this->_layout->addWidget(_gbMaterial);

    _materialCb->setCurrentIndex(Material::PHONG_MATERIAL);
}

void ABaseSettingView::translationView()
{
    // Translation UI Begin

    if (this->_options.Focus) {
        QVBoxLayout *focusLayout = new QVBoxLayout();
        focusLayout->setObjectName("fousLayout");
        QHBoxLayout *focusBtnLayout = new QHBoxLayout();
        focusBtnLayout->addWidget(new QLabel("Focus :"));
        QPushButton *pb = new QPushButton;
        pb->connect(pb, &QPushButton::clicked, [this](bool) {
            _view->movePoint(_object3d->getTranslation());
        });
        pb->setText("Focus");
        focusBtnLayout->addWidget(pb);
        focusLayout->addLayout(focusBtnLayout);
        _layout->addLayout(focusLayout);
    }

    _gbTrans = new QGroupBox("Translation :");
    QVBoxLayout *translationLayout = new QVBoxLayout();
    translationLayout->setObjectName("translationLayout");

    QHBoxLayout *translationXLayout = new QHBoxLayout();
    translationXLayout->addWidget(new QLabel("X :"));
    this->_xTranslationData = this->createDoubleSpinBoxConfigured();
    translationXLayout->addWidget(this->_xTranslationData);
    translationLayout->addLayout(translationXLayout);

    QHBoxLayout *translationYLayout = new QHBoxLayout();
    translationYLayout->addWidget(new QLabel("Y :"));
    this->_yTranslationData = this->createDoubleSpinBoxConfigured();
    translationYLayout->addWidget(this->_yTranslationData);
    translationLayout->addLayout(translationYLayout);

    QHBoxLayout *translationZLayout = new QHBoxLayout();
    translationZLayout->addWidget(new QLabel("Z :"));
    this->_zTranslationData = this->createDoubleSpinBoxConfigured();
    translationZLayout->addWidget(this->_zTranslationData);
    translationLayout->addLayout(translationZLayout);

    _gbTrans->setLayout(translationLayout);
    this->_layout->addWidget(_gbTrans);
    // Translation UI End
}

void ABaseSettingView::rotationView()
{
    // Rotation UI Begin
    _gbRot = new QGroupBox("Rotation :");

    QVBoxLayout *rotationLayout = new QVBoxLayout();
    rotationLayout->setObjectName("rotationLayout");

    QHBoxLayout *rotXLayout = new QHBoxLayout();
    rotXLayout->addWidget(new QLabel("X :"));
    this->_xRotationData = this->createDoubleSpinBoxConfigured();
    rotXLayout->addWidget(this->_xRotationData);
    rotationLayout->addLayout(rotXLayout);

    QHBoxLayout *rotYLayout = new QHBoxLayout();
    rotYLayout->addWidget(new QLabel("Y :"));
    this->_yRotationData = this->createDoubleSpinBoxConfigured();
    rotYLayout->addWidget(this->_yRotationData);
    rotationLayout->addLayout(rotYLayout);

    QHBoxLayout *rotZLayout = new QHBoxLayout();
    rotZLayout->addWidget(new QLabel("Z :"));
    this->_zRotationData = this->createDoubleSpinBoxConfigured();
    rotZLayout->addWidget(this->_zRotationData);
    rotationLayout->addLayout(rotZLayout);

    _gbRot->setLayout(rotationLayout);
    this->_layout->addWidget(_gbRot);
    // Rotation UI End
}

void ABaseSettingView::scaleView()
{
    // Scale UI Begin

    QGroupBox *gb = new QGroupBox("Scale :");

    QVBoxLayout *scaleLayout = new QVBoxLayout();
    scaleLayout->setObjectName("scaleLayout");


    QHBoxLayout *scaleXLayout = new QHBoxLayout();
    scaleXLayout->addWidget(new QLabel("X :"));
    this->_xScaleData = this->createDoubleSpinBoxConfigured();
    scaleXLayout->addWidget(this->_xScaleData);
    scaleLayout->addLayout(scaleXLayout);

    QHBoxLayout *scaleYLayout = new QHBoxLayout();
    scaleYLayout->addWidget(new QLabel("Y :"));
    this->_yScaleData = this->createDoubleSpinBoxConfigured();
    scaleYLayout->addWidget(this->_yScaleData);
    scaleLayout->addLayout(scaleYLayout);


    QHBoxLayout *scaleZLayout = new QHBoxLayout();
    scaleZLayout->addWidget(new QLabel("Z :"));
    this->_zScaleData = this->createDoubleSpinBoxConfigured();
    scaleZLayout->addWidget(this->_zScaleData);
    scaleLayout->addLayout(scaleZLayout);

    gb->setLayout(scaleLayout);
    this->_layout->addWidget(gb);
    // Scale UI End
}

void ABaseSettingView::translationSave()
{
    QVector3D translationVector;

    translationVector.setX(this->_xTranslationData->value());
    translationVector.setY(this->_yTranslationData->value());
    translationVector.setZ(this->_zTranslationData->value());

    if (this->_object3d) this->_object3d->move(translationVector);
}

void ABaseSettingView::colorSave(const QColor &color, int type) {
    Form::Primitive *obj = dynamic_cast<Form::Primitive *>(this->_object3d);
    if (obj) {
        if (type == 1) {
            this->_colorAmbianteLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;  background-color: " + color.name().toStdString() + ";"));
            QVector4D vec(color.red() / 255.0, color.green() / 255.0, color.blue() / 255.0, 1.0);
            obj->setAmbiantColor(vec);
        } else if (type == 2) {
            this->_colorDiffuseLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;  background-color: " + color.name().toStdString() + ";"));
            QVector4D vec(color.red() / 255.0, color.green() / 255.0, color.blue() / 255.0, 1.0);
            obj->setDiffuseColor(vec);
        } else if (type == 3) {
            this->_colorSpecularLabel->setStyleSheet(QString::fromStdString("border: 2px solid black;  background-color: " + color.name().toStdString() + ";"));
            QVector4D vec(color.red() / 255.0, color.green() / 255.0, color.blue() / 255.0, 1.0);
            obj->setSpecularColor(vec);
        }
    }
}

void ABaseSettingView::rotationSave()
{
    QVector3D rotateVector;

    rotateVector.setX(this->_xRotationData->value());
    rotateVector.setY(this->_yRotationData->value());
    rotateVector.setZ(this->_zRotationData->value());

    if (this->_object3d) {
        this->_object3d->orient(rotateVector);
    }
}

void ABaseSettingView::materialViewOption() {
    Material::eMaterial type = _materialCb->currentData().value<Material::eMaterial>();
    _textureLabel->hide();
    _textureButton->hide();
    _textureNLabel->hide();
    _textureNButton->hide();
    _textureRLabel->hide();
    _repeatData->hide();
    _shinenessBox->hide();
    _shinenessLabel->hide();
    if (type == Material::TEXTURE_MATERIAL || type == Material::NORMAL_MAP_MATERIAL) {
        _textureLabel->show();
        _textureButton->show();
        _textureRLabel->show();
        _repeatData->show();
        if (type == Material::NORMAL_MAP_MATERIAL) {
            _textureNLabel->show();
            _textureNButton->show();
        }
    } else if (type == Material::PHONG_MATERIAL) {
        _shinenessBox->show();
        _shinenessLabel->show();
    }
}

void ABaseSettingView::materialSave(int) {
    Material::eMaterial type = _materialCb->currentData().value<Material::eMaterial>();
    Object3D *obj = dynamic_cast<Object3D *>(this->_object3d);

     if (_options.Video) {
         if (_checkVideo->isChecked()) {
             type = Material::TEXTURE_MATERIAL;
         } else {
             if (_camera) {
                 _camera->stop();
                 delete _camera;
             }
             _camera = 0;
         }
     }
     for (auto it = obj->mat().begin(); it != obj->mat().end(); ++it)
     {
         Material *material = (*it);
         if (type == Material::TEXTURE_MATERIAL || type == Material::NORMAL_MAP_MATERIAL) {
             if (_options.Video && _checkVideo->isChecked() && !_camera) {
                    VideoTexturer *texturer = new VideoTexturer(*(*it));
                    _camera = new Video::VideoCamera();
                    _camera->start();
                    _camera->addObserver(texturer);
                    material->setType(type);
             }
             if (_textureFileDialog->selectedFiles().size() != 0 && _textureFileDialog->selectedFiles().at(0) != QDir::currentPath()) {
                 material->setTexture(_textureFileDialog->selectedFiles().at(0).toStdString(), _repeatData->value());
             }

             if (_textureNFileDialog->selectedFiles().size() != 0 && _textureNFileDialog->selectedFiles().at(0) != QDir::currentPath() && type == Material::NORMAL_MAP_MATERIAL) {
                material->setNormalMap(_textureNFileDialog->selectedFiles().front().toStdString());
             }

             material->setTextureRepeat(_repeatData->value());
             material->setType(type);
         }
         else {
             if (type == Material::PHONG_MATERIAL) {
                 material->setShineness(_shinenessBox->value());
             }
             material->setType(type);
         }
     }
}

void ABaseSettingView::scaleSave()
{
    QVector3D scaleVector;

    scaleVector.setX(this->_xScaleData->value());
    scaleVector.setY(this->_yScaleData->value());
    scaleVector.setZ(this->_zScaleData->value());

    if (this->_object3d) this->_object3d->resize(scaleVector);
}

void ABaseSettingView::colorEdit() {
    Form::Primitive *obj = dynamic_cast<Form::Primitive *>(this->_object3d);
    if (obj) {
        QVector4D vc(obj->getDiffuseColor());
        QColor color(vc.x() * 255.0, vc.y() * 255.0, vc.z() * 255.0, 1.0);
        this->_colorDiffuseDialog->setCurrentColor(color);
        this->_colorDiffuseLabel->setStyleSheet(QString::fromStdString("border: 2px solid black; background-color: " + color.name().toStdString() + ";"));

        QVector4D vca(obj->getAmbiantColor());
        QColor colora(vca.x() * 255.0, vca.y() * 255.0, vca.z() * 255.0, 1.0);
        this->_colorAmbianteDialog->setCurrentColor(colora);
        this->_colorAmbianteLabel->setStyleSheet(QString::fromStdString("border: 2px solid black; background-color: " + colora.name().toStdString() + ";"));

        QVector4D vcs(obj->getSpecularColor());
        QColor colors(vcs.x() * 255.0, vcs.y() * 255.0, vcs.z() * 255.0, 1.0);
        this->_colorSpecularDialog->setCurrentColor(color);
        this->_colorSpecularLabel->setStyleSheet(QString::fromStdString("border: 2px solid black; background-color: " + colors.name().toStdString() + ";"));
    }
}

void ABaseSettingView::translationEdit()
{
    QVector3D translationVector = this->_object3d->getTranslation();
    this->_xTranslationData->setValue(translationVector.x());
    this->_yTranslationData->setValue(translationVector.y());
    this->_zTranslationData->setValue(translationVector.z());
}

void ABaseSettingView::rotationEdit()
{
    QVector3D rotationVector = this->_object3d->getRotation();
    this->_xRotationData->setValue(rotationVector.x());
    this->_yRotationData->setValue(rotationVector.y());
    this->_zRotationData->setValue(rotationVector.z());
}

void ABaseSettingView::scaleEdit()
{
    QVector3D scaleVector = this->_object3d->getScale();

    this->_xScaleData->setValue(scaleVector.x());
    this->_yScaleData->setValue(scaleVector.y());
    this->_zScaleData->setValue(scaleVector.z());
}

void ABaseSettingView::materialEdit() {

    Object3D *obj = dynamic_cast<Object3D *>(this->_object3d);
    if (obj) {
        Material *mt = obj->mat().front();
        if (mt) {
            _shinenessBox->setValue(mt->getShineness());
            _repeatData->setValue(mt->getTextureRepeat());
            this->_materialCb->setCurrentIndex(mt->getType());
            this->_repeatData->connect(this->_repeatData, SIGNAL(valueChanged(int)), this, SLOT(materialSave(int)));
        }
    }
}

void ABaseSettingView::saveEvent()
{
   if (this->_options.Translation) { this->translationSave(); }
   if (this->_options.Rotation) { this->rotationSave(); }
   if (this->_options.Scale) { this->scaleSave(); }
}

BaseSettingOptions::BaseSettingOptions() : Translation(true),
    Rotation(true), Scale(true), Material(true), Color(true), Video(true), Focus(true)
{}

BaseSettingOptions::~BaseSettingOptions() {}
}

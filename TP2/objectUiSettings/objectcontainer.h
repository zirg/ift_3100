#ifndef OBJECTCONTAINER_H
#define OBJECTCONTAINER_H

#include <QWidget>
#include <map>
#include <QComboBox>
#include "iobjectdescriptor.h"
#include "abasesettingview.h"
#include <QStringList>

using namespace std;

namespace objectUiSettings {
typedef QWidget *(*ptr_func)();
typedef struct s_container {
    ptr_func Function;
    bool isObject;
} t_container;

class ObjectContainer
{
public:
    ObjectContainer();
    ~ObjectContainer();
public:
    void registerObject(QString const &, ptr_func, bool isObject = true);
    IObjectDescription *getSettingView(QString const &, bool init = false) const;
    const QStringList &getObjectType();
    IWrapper *getObjectWrapper(QString const &name, Movable *movable) const;
private:
    map<QString, t_container *> *_list;
    QStringList _objectType;
};
}

#endif // OBJECTCONTAINER_H

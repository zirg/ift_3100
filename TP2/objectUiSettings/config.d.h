#ifndef CONFIG_D_H
#define CONFIG_D_H
#include "objectUiSettings/objectcontainer.h"
#include "objectDescriptor/descriptor.h"

namespace objectUiSettings {
enum object_type {
    OBJECT,
    LIGHT,
    CAMERA,
    SCENE
};

typedef struct s_object_config {
    s_object_config(std::string const &type, ptr_func function) {
        this->type = type;
        this->name = this->type;
        this->create = function;
        this->etype = object_type::OBJECT;
    }

    s_object_config(std::string const &type, ptr_func function, object_type etype) {
        this->type = type;
        this->name = this->type;
        this->create = function;
        this->etype = etype;
    }
    s_object_config(std::string const &type, std::string const &name, ptr_func function) {
        this->type = type;
        this->name = name;
        this->create = function;
        this->etype = object_type::OBJECT;
    }
    std::string type;
    std::string name;
    ptr_func create;
    object_type etype;
} t_object_config;

typedef struct s_light_config {
    QString name;
    Light::eLight type;
} t_light_config;

typedef struct s_material_config {
    QString name;
    Material::eMaterial type;
} t_material_config;


extern int gtotal_material_config;
extern int gtotal_light_config;
extern int gtotal_object_config;
extern int gTotal_default_object;
extern t_material_config gMaterial_config[];
extern t_light_config gLight_config[];
extern t_object_config gObjects_config[];
extern std::pair<QString, Movable *> gDefault_object[];
}
#endif // CONFIG_D_H


#ifndef OBJECTWRAPPER_H
#define OBJECTWRAPPER_H

#include <QWidget>
#include <QString>
#include "object3d.h"
#include "iwrapper.h"

namespace objectUiSettings {
class ObjectWrapper : public IWrapper
{
public:
    ObjectWrapper(Movable *obj, QString const &type);
    ~ObjectWrapper();
public:
    Movable *getObject() const;
    QString const &getObjectType() const;
private:
    Movable *_object;
    QString _type;
};
}
Q_DECLARE_METATYPE(objectUiSettings::IWrapper *)

#endif // OBJECTWRAPPER_H

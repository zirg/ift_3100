#ifndef LIGHTWRAPPER_H
#define LIGHTWRAPPER_H

#include <QWidget>
#include "iwrapper.h"
#include "light.h"

namespace objectUiSettings {
class LightWrapper : public IWrapper
{
public:
    LightWrapper(Movable *light);
    ~LightWrapper();
public:
    QString const &getObjectType() const;
    Movable *getObject() const;
private:
    Movable  *_light;
    QString _name;
};
}

Q_DECLARE_METATYPE(objectUiSettings::LightWrapper *)
#endif // LIGHTWRAPPER_H

#include "objectwrapper.h"

namespace objectUiSettings {
ObjectWrapper::ObjectWrapper(Movable *obj, const QString &type) :  _object(obj), _type(type)
{

}

ObjectWrapper::~ObjectWrapper()
{

}

const QString &ObjectWrapper::getObjectType() const {
    return this->_type;
}

Movable *ObjectWrapper::getObject() const {
    return this->_object;
}
}

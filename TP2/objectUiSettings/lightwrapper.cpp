#include "lightwrapper.h"
namespace objectUiSettings {
LightWrapper::LightWrapper(Movable *light) : _light(light), _name("Light")
{

}

LightWrapper::~LightWrapper()
{

}

const QString &LightWrapper::getObjectType() const
{
    return this->_name;
}

Movable *LightWrapper::getObject() const
{
    return this->_light;
}
}

#ifndef SCENECONFIGURATION_H
#define SCENECONFIGURATION_H

#include <QObject>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QWidget>
#include "scene.h"

namespace objectUiSettings {
class SceneConfiguration : public QWidget
{
     Q_OBJECT
public:
    explicit SceneConfiguration(QWidget *parent = 0);
    virtual ~SceneConfiguration();
public slots:
    void screenTypeChanged(int index);
public:
    static SceneConfiguration *Create();
    void init(Scene *);
private:
    CubeMap *_cubemap;
    QComboBox *_cbScreen;
    QVBoxLayout *_layout;
    Scene *_scene;
};
}
#endif // SCENECONFIGURATION_H

#include "objectcontainer.h"
#include "objectUiSettings/objectDescriptor/descriptor.h"

namespace objectUiSettings {
ObjectContainer::ObjectContainer()
{
    this->_list = new map<QString, t_container *>();
}

ObjectContainer::~ObjectContainer()
{
    for (map<QString, t_container *>::const_iterator it = this->_list->begin(); it != this->_list->end(); ++it) {
        delete it->second;
    }
    delete this->_list;
}


void ObjectContainer::registerObject(const QString &name, ptr_func element, bool isObject)
{
    t_container *container = new t_container();
    container->Function = element;
    container->isObject = isObject;
    this->_list->insert({name, container});
}

IObjectDescription *ObjectContainer::getSettingView(const QString &name, bool init) const
{
    for (map<QString, t_container *>::const_iterator it = this->_list->begin(); it != this->_list->end(); ++it) {
        if (it->first == name) {
            IObjectDescription *base = (IObjectDescription *)it->second->Function();
            base->init(init);
            return base;
        }
    }
    return NULL;
}

QStringList const &ObjectContainer::getObjectType()
{
    if (this->_objectType.length() == 0) {
        for (map<QString, t_container *>::const_iterator it = this->_list->begin(); it != this->_list->end(); ++it) {
            if (it->second->isObject)
                this->_objectType.push_back(it->first);
        }
    }
    return this->_objectType;
}

IWrapper *ObjectContainer::getObjectWrapper(const QString &name, Movable *movable) const
{
    for (map<QString, t_container *>::const_iterator it = this->_list->begin(); it != this->_list->end(); ++it) {
        if (it->first == name) {
            IObjectDescription *base = (IObjectDescription *)it->second->Function();
            IWrapper *wrapper = base->CreateWrapper(movable);
            delete base;
            return wrapper;
        }
    }
    return NULL;
}
}

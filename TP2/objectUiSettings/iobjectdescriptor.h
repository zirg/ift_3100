#ifndef IOBJECTDESCRIPTOR
#define IOBJECTDESCRIPTOR

#include <QWidget>
#include <string>
#include "object3d.h"
#include "objectwrapper.h"
#include "camera.h"

namespace objectUiSettings {
class IObjectDescription : public QWidget {
public:
    explicit IObjectDescription(QWidget *parent = 0) : QWidget(parent) {}
    virtual ~IObjectDescription() {}
    virtual QString const &getName() const = 0;
    virtual QString const &getType() const = 0;
    virtual IWrapper *getWrappedObject() = 0;
    virtual void setObjectSettingInformation(IWrapper *wrapper) = 0;
    virtual void init(bool = false) = 0;
    virtual IWrapper *CreateWrapper(Movable *obj) = 0;
    virtual void refreshDescription() = 0;
    virtual void setCamera(Camera *) = 0;
protected:
    virtual Movable *createObject() = 0;
};
}



#endif // IOBJECTDESCRIPTOR


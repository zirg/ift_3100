#ifndef CONFIG
#define CONFIG

#include "config.d.h"

namespace objectUiSettings {

t_object_config gObjects_config[] = {
    {"Cube", CubeDescription::Create},
    {"Triangle", TriangleDescription::Create},
    {"Sphere", SphereDescription::Create},
    {"Pyramid", PyramidDescription::Create},
    {"Loader", ObjectLoaderDescription::Create},
    {"Cone", ConeDescription::Create},
    {"Cylinder", CylinderDescription::Create},
    {"Torus", TorusDescription::Create},
    {"Kochsnowflake", KochsnowflakeDescription::Create},
    {"Plane", PlaneDescription::Create},
    {"Bezier", BezierDescription::Create},
    {"Earth", EarthDescription::Create},
    {"Light", LightDescription::Create, object_type::LIGHT},
    {"Camera", CameraDescription::Create, object_type::CAMERA}
};

int gtotal_object_config = 14;

t_light_config gLight_config[] = {
    {"Ambient", Light::AMBIANT},
    {"Directional", Light::DIRECTIONAL},
    {"Point", Light::POINT},
    {"Spot", Light::SPOT}
};

int gtotal_light_config = 4;

t_material_config gMaterial_config[] = {
    {"None", Material::BASE_MATERIAL},
    {"Point", Material::POINT_MATERIAL},
    {"Line", Material::LINE_MATERIAL},
    {"Lambert", Material::LAMBERT_MATERIAL},
    {"Phong", Material::PHONG_MATERIAL},
    {"Toon", Material::TOON_MATERIAL},
    {"Texture", Material::TEXTURE_MATERIAL},
    {"Normal Mapping", Material::NORMAL_MAP_MATERIAL}
};

int gtotal_material_config = 8;

std::pair<QString, Movable *> gDefault_object[] = {
    {"Light", new Light(Light::AMBIANT)}
};

int gTotal_default_object = 1;
}

#endif // CONFIG


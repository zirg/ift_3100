#include "movable.h"

Movable::Movable()
{
    _position.setToIdentity();
    _translation.setToIdentity();
    _rotation.setToIdentity();
    _scale.setToIdentity();
    orient(QVector3D(180.0, 0.0, 0.0));
}

Movable::~Movable()
{

}

void    Movable::translate(QVector3D direction)
{
    _translation.translate(direction);
}

void    Movable::move(QVector3D position)
{
    _translation.setToIdentity();
    _translation.translate(position);
}

QVector3D Movable::getTranslation() const
{
    QVector3D res;

    res[0] = _translation.row(0)[3];
    res[1] = _translation.row(1)[3];
    res[2] = _translation.row(2)[3];
    return res;
}

void    Movable::rotate(float angle, QVector3D direction)
{
    QQuaternion q = QQuaternion::fromAxisAndAngle(direction, angle);
    _rotation.rotate(q);

    QVector3D tmp;
    tmp[0] = atan2(_rotation.row(0)[2], _rotation.row(1)[2]);
    tmp[1] = acos(_rotation.row(2)[2]);
    tmp[2] = atan2(_rotation.row(2)[0],  - _rotation.row(2)[1]);

    if (sin(tmp[1]) != 0)
    {
    _euler[0] = atan2(cos(tmp[0]) * sin(tmp[1]), cos(tmp[1]));
    _euler[1] = -asin(sin(tmp[0]) * sin(tmp[1]));
    _euler[2] = atan2(cos(tmp[1]) * sin(tmp[2]) + sin(tmp[0]) * cos(tmp[1]) * cos(tmp[2]),
                cos(tmp[0]) * cos(tmp[2]) - sin(tmp[0]) * cos(tmp[1]) * sin(tmp[2]));

    _euler[0] = _euler[0] * (180.0 / M_PI);
    _euler[1] = _euler[1] * (180.0 / M_PI);
    _euler[2] = _euler[2] * (180.0 / M_PI);
    }
}

void    Movable::orient(float angle, QVector3D orientation)
{
    QQuaternion q = QQuaternion::fromAxisAndAngle(orientation, angle);
    _rotation.setToIdentity();
    _rotation.rotate(q);

    _euler[0] = orientation[0] * angle;
    _euler[1] = orientation[1] * angle;
    _euler[2] = orientation[2] * angle;
}

void    Movable::orient(QVector3D orientation)
{
    orientation.setX(fmod(orientation.x(), 360.0));
    orientation.setY(fmod(orientation.y(), 360.0));
    orientation.setZ(fmod(orientation.z(), 360.0));
    float x = orientation.x() * (M_PI / 180.0);
    float y = orientation.y() * (M_PI / 180.0);
    float z = orientation.z() * (M_PI / 180.0);

    QQuaternion q;

    q.setX(cos(z/2.0) * cos(y/2.0) * cos(x/2.0) + sin(z/2.0) * sin(y/2.0) * sin(x/2.0));
    q.setY(sin(z/2.0) * cos(y/2.0) * cos(x/2.0) + cos(z/2.0) * sin(y/2.0) * sin(x/2.0));
    q.setZ(cos(z/2.0) * sin(y/2.0) * cos(x/2.0) + sin(z/2.0) * cos(y/2.0) * sin(x/2.0));
    q.setScalar(cos(z/2.0) * cos(y/2.0) * sin(x/2.0) + sin(z/2.0) * sin(y/2.0) * cos(x/2.0));
    _rotation.setToIdentity();
    _rotation.rotate(q);
    _euler = orientation;
}

QVector3D Movable::getRotation() const
{
    return _euler;
}

void    Movable::scale(QVector3D direction)
{
    _scale.scale(direction);
}

void    Movable::resize(QVector3D direction)
{
    _scale.setToIdentity();
    _scale.scale(direction);
}

QVector3D Movable::getScale() const
{
    QVector3D res;

    res[0] = _scale.row(0)[0];
    res[1] = _scale.row(1)[1];
    res[2] = _scale.row(2)[2];
    return res;
}
const QMatrix4x4&  Movable::position()
{
    _position.setToIdentity();
    _position = _translation * _rotation  * _scale;
    return _position;
}

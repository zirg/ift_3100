#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>
#include <QFileInfo>
#include <QtGlobal>
#include <QStringList>
#include <QtGui/QOpenGLTexture>
#include <QPixmap>

#include "objloader.h"

ObjLoader::ObjLoader() {

    _defaultMaterial = new Material(Material::PHONG_MATERIAL, "DEFAULT_MATERIAL");
    _defaultMaterial->setAmbiant(QVector4D(0.2, 0.5, 0.2, 0.0));
    _defaultMaterial->setDiffuse(QVector4D(0.1, 0.5, 0.1, 0.0));
   _defaultMaterial->setSpecular(QVector4D(0.6, 0.6, 0.6, 0.0));

    _components['a'] = &Material::setAmbiant;
    _components['d'] = &Material::setDiffuse;
    _components['s'] = &Material::setSpecular;
}

ObjLoader::~ObjLoader() {
}

void ObjLoader::_addTri(Geometry *g, QVector3D& v1, QVector3D& v2, QVector3D& v3, bool addNormal)
{
    int face_count = g->vertices().size() / 3;

    //Creating triangle face
    g->addVertex(v1);
    g->addVertex(v2);
    g->addVertex(v3);
    g->addFace(face_count, face_count + 1, face_count + 2);

    if (addNormal) {
        g->addNormal(QVector3D::crossProduct(v1 - v2, v2 - v3).normalized());
        g->addNormal(QVector3D::crossProduct(v1 - v2, v2 - v3).normalized());
        g->addNormal(QVector3D::crossProduct(v1 - v2, v2 - v3).normalized());
    }
}

void ObjLoader::_addQuad(Geometry *g, QVector3D& v1, QVector3D& v2, QVector3D& v3, QVector3D& v4, bool addNormal)
{
    int face_count = g->vertices().size() / 3;

    //Creating triangles faces
    g->addVertex(v1);
    g->addVertex(v2);
    g->addVertex(v3);
    g->addVertex(v4);
    g->addFace(face_count, face_count + 1, face_count + 2);
    g->addFace(face_count, face_count + 2, face_count + 3);

    if (addNormal) {
        g->addNormal(QVector3D::crossProduct(v2 - v1, v3 - v1).normalized());
        g->addNormal(QVector3D::crossProduct(v2 - v1, v3 - v1).normalized());
        g->addNormal(QVector3D::crossProduct(v3 - v1, v4 - v1).normalized());
        g->addNormal(QVector3D::crossProduct(v3 - v1, v4 - v1).normalized());
    }
}

bool ObjLoader::_parseLine(Geometry *geo, const std::string &line) {

    std::string header = line.substr(0, 2);
    std::string value = line.substr(2);

    // Extract normals/UV's/vertices
    if (header[0] == 'v') {
        std::istringstream iss(value);
        float vx, vy, vz;
        if (!(iss >> vx >> vy)) return false;
        if (header[1] != 't' && !(iss >> vz)) return false;
        if (header[1] != 't') (header[1] == 'n') ? _normals.push_back(QVector3D(vx, vy, vz)) : _vertices.push_back(QVector3D(vx, vy, vz));
        else _uvs.push_back(QVector2D(vx, vy));
    } else if (header[0] == 'f') {

        std::vector<int> faces;
        // Just split function used QT
        QRegExp rx("(\\ |\\/)");
        QStringList query = QString::fromStdString(value).simplified().split(rx);
        for (QStringList::const_iterator constIterator = query.constBegin(); constIterator != query.constEnd(); ++constIterator) {
            QByteArray ba = (*constIterator).toLocal8Bit();
            faces.push_back(ba.toStdString().empty() ? - 1 : qAbs(ba.toInt()));
        }

        unsigned int count_faces = faces.size();
        std::vector<int> _indexVertices, _indexUvs, _indexNormals;

        bool isTriangle = (count_faces % 2) || count_faces == 6;
        int incr = 0;
        if (isTriangle) incr = ((count_faces % 2) ? ((count_faces == 3) ? 1 : 3) : 2);
        else incr = (count_faces == 12 || count_faces == 4) ? ((count_faces == 4) ? 1 : 3) : 2;
        for (unsigned int i = 0; i < count_faces; i += incr) {
            _indexVertices.push_back(faces[i] - 1);
            if (faces[1] != -1 && (count_faces != 3 && count_faces != 4)) {
                _indexUvs.push_back(faces[i + 1] - 1);
            }
            if (incr == 3) _indexNormals.push_back(faces[i + 2] - 1);
        }

        for (int i = 0; i < (isTriangle ? 3 : 4); i++) {
            if (!_indexUvs.empty()) {
                QVector2D uv = _uvs[_indexUvs[i]];
                geo->addUV(uv.x(), uv.y());
            }
            if (!_indexNormals.empty()) geo->addNormal(_normals[_indexNormals[i]]);
        }

        if (isTriangle) _addTri(geo, _vertices[_indexVertices[0]], _vertices[_indexVertices[1]], _vertices[_indexVertices[2]], _indexNormals.empty());
        else _addQuad(geo, _vertices[_indexVertices[0]], _vertices[_indexVertices[1]], _vertices[_indexVertices[2]], _vertices[_indexVertices[3]], _indexNormals.empty());

    }
    return true;
}

bool ObjLoader::_parseMtl(const std::string &name) {

    QFileInfo info(QString::fromLocal8Bit(_pathObj.c_str()));

    std::string pathMtl = info.absolutePath().toStdString() + "/" + name;
    std::ifstream infile(pathMtl);
    if (infile.fail()) {
        std::cout << "Failed to open \"" << pathMtl << "\"" << std::endl;
        return false;
    }

    std::string line;
    float density = 1.0;

    Material *currentMaterial = 0;
    while(std::getline(infile, line)) {
        line = QString::fromStdString(line).simplified().toStdString();
        if (line.size() > 7 && line.substr(0, 6) == "newmtl") {
            density = 1.0;
            currentMaterial = new Material(Material::PHONG_MATERIAL, line.substr(7));
            _materials[line.substr(7)] = currentMaterial;
        }
        if (line.size() > 1 && line[0] == 'K' && line[1] != 'e') {
            std::istringstream iss(line.substr(2));
            float r, g, b;
            if ((iss >> r >> g >> b)) {

                (currentMaterial->*_components[line[1]])(QVector4D(r, g, b, density));
            }
        } else if (line[0] == 'd' || line.substr(0, 2) == "Tr") {
            density = QString::fromStdString(line.substr(2)).toDouble();
            QVector4D vdiffuse(currentMaterial->getDiffuse());
            vdiffuse.setW(density);
            currentMaterial->setDiffuse(vdiffuse);
            QVector4D vambiante(currentMaterial->getAmbiant());
            vambiante.setW(density);
            currentMaterial->setAmbiant(vambiante);
            QVector4D vspecular(currentMaterial->getSpecular());
            vspecular.setW(density);
            currentMaterial->setSpecular(vspecular);
        } else if (line.size() > 7 && line.substr(0, 6) == "map_Kd") {
            std::string path = info.absolutePath().toStdString() + "/" + line.substr(7);
            currentMaterial->setType(Material::TEXTURE_MATERIAL);
            currentMaterial->setTexture(path);
        }

    }
    return true;
}

Geometry *ObjLoader::_createGeometry() {
    Geometry *geo =  new Geometry();
    geo->setMaterialName("DEFAULT_MATERIAL");
    return geo;
}

void ObjLoader::_cleanMaterials() {
    for (auto it = _materials.begin(); it != _materials.end(); ++it) {
        delete (*it).second;
    }
    _materials.clear();
}

Object3D* ObjLoader::load(const std::string &pathObj) {
    _pathObj = pathObj;
    std::ifstream infile(_pathObj);
    if (infile.fail()) {
        std::cout << "Failed to open \"" << _pathObj << "\"" << std::endl;
        return nullptr;
    }

    std::string line;

    Object3D *obj = new Object3D;
    obj->addMaterial(_defaultMaterial);

    Geometry *currentGeo = 0;

    while(std::getline(infile, line)) {
        if (line.size() >= 1) {
            std::string header = line.substr(0, 2);
            if (line.size() > 2) {
                std::string value = line.substr(2);
                if (header[0] == 'o' || header[0] == 'g') {
                   currentGeo = _createGeometry();
                   obj->addGeometry(currentGeo);
                } else if (line.size() > 5 && line.substr(0, 6) == "mtllib") {
                    _parseMtl(QString::fromStdString(line.substr(7)).simplified().toStdString());
                } else if (line.size() > 5 && line.substr(0, 6) == "usemtl") {
                    line = QString::fromStdString(line.substr(7)).simplified().toStdString();
                    Material *material = _materials[line];
                    if (material) {
                        obj->removeMaterial(obj->getMaterial("DEFAULT_MATERIAL"));
                        obj->addMaterial(material);
                        currentGeo->setMaterialName(material->getName());
                    }
                } else {
                    if (!currentGeo) {
                        currentGeo = _createGeometry();
                        obj->addGeometry(currentGeo);
                    }
                    if(!_parseLine(currentGeo, line)) {
                        delete obj;
                        _cleanMaterials();
                        return nullptr;
                    }
                }
            }
        }
    }

    std::list<Geometry *> lgeo =  obj->getGeometry();
    for (auto it = lgeo.begin(); it != lgeo.end(); ++it) {
        (*it)->compile();
    }

    infile.close();
    return obj;
}

#ifndef MATERIAL_H
#define MATERIAL_H

#include <iostream>
#include <QMatrix4x4>
#include <QtGui/QOpenGLFunctions>
#include <QtGui/QOpenGLTexture>
#include <QPixmap>

class Material
{
    Q_ENUMS(eMaterial)
public:
    enum eMaterial
    {
        UNDEFINED = -1,
        BASE_MATERIAL = 0,
        POINT_MATERIAL,
        LINE_MATERIAL,
        LAMBERT_MATERIAL,
        PHONG_MATERIAL,
        TOON_MATERIAL,
        TEXTURE_MATERIAL,
        NORMAL_MAP_MATERIAL,
        CUBE_MAP_REFLECTION,
        CUBE_MAP_MATERIAL,
        EARTH_MATERIAL
    };
public:
    Material(eMaterial type, const std::string& name);
    ~Material();

    Material::eMaterial getType() const;
    void                setType(Material::eMaterial type);

    const std::string&  getName();
    void                setAmbiant(QVector4D color);
    const QVector4D&    getAmbiant();
    void                setDiffuse(QVector4D color);
    const QVector4D&    getDiffuse();
    void                setSpecular(QVector4D color);
    const QVector4D&    getSpecular();
    void                setShineness(float shine);
    float               getShineness() const;
    void                setTexture(const std::string &path, const unsigned int repeat = 1, const float alpha = 1.0);
    void                setTexture(QOpenGLTexture *texture, const unsigned int repeat = 1, const float alpha = 1.0);
    QOpenGLTexture*     getTexture();
    unsigned int        getTextureRepeat() const;
    void                setTextureRepeat(const unsigned int);
    float               getTextureAlpha() const;
    void                setNormalMap(const std::string &path);
    QOpenGLTexture*     getNormalMap();
    const std::string   &getTexturePath() const;
    const std::string   &getNormalMapPath() const;
    void                addTexture(const std::string& path);
    std::list<QOpenGLTexture*>  &getTextures();
    std::list<std::string>      &getTexturesPath();

private:
    Material::eMaterial _type;
    std::string         _name;
    QVector4D           _ambiant;
    QVector4D           _diffuse;
    QVector4D           _specular;
    float               _shineness;
    QOpenGLTexture      *_texture;
    QOpenGLTexture      *_normalMap;
    std::string          _texturePath;
    std::string          _normalMapPath;
    unsigned int         _textureRepeat;
    float                _textureAlpha;

    std::list<QOpenGLTexture*>  _textures;
    std::list<std::string>      _texturesPath;

};

Q_DECLARE_METATYPE(Material::eMaterial)

#endif // MATERIAL_H

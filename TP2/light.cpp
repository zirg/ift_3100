#include "light.h"

Light::Light(Light::eLight type)
{
    _type = type;
    _intensity = 1.0f;
    _ambiant = QVector4D(1.0f, 1.0f, 1.0f, 1.0f); // Default white
    _diffuse = QVector4D(1.0f, 1.0f, 1.0f, 1.0f); // Default white
    _specular = QVector4D(1.0f, 1.0f, 1.0f, 1.0f); // Default white
    _constant_att = 0.0f;
    _linear_att = 0.05f;
    _quadratic_att = 0.0f;
    _cutoff = 10.0f;
    _coscutoff = cos(_cutoff * (M_PI / 180.0f));
    _exponent = 2.0f;
}

Light::~Light()
{ }

void                Light::setType(eLight type) { _type = type; }
Light::eLight       Light::getType() const { return _type; }

void                Light::setIntensity(float i) { _intensity = i; }
float               Light::getIntensity() const { return _intensity; }
void                Light::setAmbiant(QVector4D color) { _ambiant = color; }
const QVector4D&    Light::getAmbiant() const { return _ambiant; }

void                Light::setDiffuse(QVector4D color) { _diffuse = color; }
const QVector4D&    Light::getDiffuse() const { return _diffuse; }

void                Light::setSpecular(QVector4D color) { _specular = color; }
const QVector4D&    Light::getSpecular() const { return _specular; }

void                Light::setConstantAtt(float value) { _constant_att = value; }
float               Light::getConstantAtt() const {return _constant_att; }
void                Light::setLinearAtt(float value) { _linear_att = value; }
float               Light::getLinearAtt() const { return _linear_att; }
void                Light::setQuadraticAtt(float value) { _quadratic_att = value; }
float               Light::getQuadraticAtt() const { return _quadratic_att; }

void                Light::setCutoff(float value) { _cutoff = value; _coscutoff = cos(_cutoff * (M_PI / 180.0f)); }
float               Light::getCutoff() const { return _cutoff; }
float               Light::getCosCutOff() const { return _coscutoff; }

void                Light::setExponent(float value) { _exponent = value; }
float               Light::getExponent() const { return _exponent; }

QVector3D           Light::getDirection() const
{
    QVector3D dir(0.0f, -1.0f, 0.0f);

    return dir * _rotation;
}

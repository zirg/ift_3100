#ifndef OBJEXPORT_H
#define OBJEXPORT_H

#include "object3d.h"
#include "iexport.h"

class ObjExport : public IExport {
private:
    std::list<Object3D *> _lobj;
    int _countFaces;
public:
    ObjExport(Object3D *obj);
    ObjExport(const std::list<Object3D *> &lobj);
    virtual void save(const std::string &path);
private:
    void _exportObject(std::ostream &, Object3D *obj);
};


#endif // OBJEXPORT_H


#include "scene.h"

Scene::Scene()
    : _displayGrid(true),
      _displayCubemap(false),
      _grid(nullptr),
      _camera(nullptr),
      _cubemap(nullptr),
      _screenEffect(Program::AScreenProgram::DEFAULT)
{

}

bool Scene::empty() const {
    return _obj.size();
}

//Public usage interface
void                Scene::setCamera(Camera * c)
{
    _camera = c;
}

Camera*             Scene::getCamera()
{
    return _camera;
}

void                Scene::addObject3D(Object3D *obj)
{
    _obj.push_back(obj);
}

void                Scene::removeObject3D(Object3D *obj)
{
    _obj.remove(obj);
}

std::list<Object3D *>& Scene::getObject()
{
    return _obj;
}

void                Scene::addLight(Light *light)
{
    _light.push_back(light);
}

std::list<Light *>& Scene::getLight()
{
    return _light;
}

void                Scene::removeLight(Light *l)
{
    _light.remove(l);
}

void                Scene::reset() {
    setScreenEffect(Program::AScreenProgram::DEFAULT);
    displayCubeMap(false);
    displayGrid(true);
}

void                Scene::clear() {

   _light.clear();
   _obj.clear();

}

void                Scene::setGrid(Form::Grid *grid) {
    if (_grid != nullptr) delete _grid;
    _grid = grid;
}

void                Scene::setCubeMap(CubeMap *cubemap) {
    if (_cubemap != nullptr) delete _cubemap;
    _cubemap = cubemap;
}

CubeMap*            Scene::getCubeMap() const {
    return _cubemap;
}

void Scene::displayCubeMap(const bool state) {
    _displayCubemap = state;
}

bool Scene::cubeMapIsVisible() const {
    return _displayCubemap;
}

void Scene::displayGrid(const bool state) {
    _displayGrid = state;
}

bool Scene::gridIsVisible() const {
    return _displayGrid;
}

Form::Grid *Scene::getGrid() const {
    return _grid;
}

void Scene::setScreenEffect(const Program::AScreenProgram::eProgram &effect)
{
    _screenEffect = effect;
}

Program::AScreenProgram::eProgram Scene::getScreenEffect() const
{
    return _screenEffect;
}

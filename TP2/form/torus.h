#ifndef TORUS_H
#define TORUS_H

#include <cmath>
#include <QVector3D>

#include "primitive.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

namespace Form {

class Torus: public Primitive
{
public:
    Torus();
    Torus(float radius, float radius2, int density);

    float getRadius() const;
    float getRadius2() const;
    int getDensity() const;

    void setRadius(float radius);
    void setRadius2(float radius2);
    void setDensity(int density);

    ~Torus() = default;
private:
    float _radius;
    float _radius2;
    int _density;
    void _generateGeometry(float radius, float inradius,int density);
    QVector3D _toCarthesianCoord(float radius, float theta, float azimuth);
};
}
#endif // TORUS_H

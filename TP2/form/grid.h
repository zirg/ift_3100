#ifndef GRID
#define GRID

#include "form.h"

namespace Form {

class Grid : public Primitive
{
public:
    Grid(const int size = 10);
    ~Grid();
};

}

#endif // GRID


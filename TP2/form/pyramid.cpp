#include "pyramid.h"

Form::Pyramid::Pyramid(float size)
{
    Geometry *g = _geometry.front();
    float mid = size / 2.0;

    //Front face
    g->addVertex(0.0, size, -mid);
    g->addVertex(-mid, 0.0, mid);
    g->addVertex(mid, 0.0, mid);
    g->addFace(0, 1, 2);
    g->addNormal(0.0, size, mid);
    g->addNormal(0.0, size, mid);
    g->addNormal(0.0, size, mid);

    g->addUV(0,0);
    g->addUV(0,1);
    g->addUV(1,1);

    //Bottom face
    g->addVertex(0.0, 0.0, -mid);
    g->addVertex(-mid, 0.0, mid);
    g->addVertex(mid, 0.0, mid);
    g->addFace(3, 4, 5);
    g->addNormal(0.0, -1.0, 0.0);
    g->addNormal(0.0, -1.0, 0.0);
    g->addNormal(0.0, -1.0, 0.0);

    g->addUV(0,0);
    g->addUV(0,1);
    g->addUV(1,1);

    //Left face
    g->addVertex(0.0, size, -mid);
    g->addVertex(-mid, 0.0, mid);
    g->addVertex(0.0, 0.0, -mid);
    g->addFace(6, 7, 8);
    g->addNormal(-mid, size, -mid);
    g->addNormal(-mid, size, -mid);
    g->addNormal(-mid, size, -mid);

    g->addUV(0,0);
    g->addUV(0,1);
    g->addUV(1,1);

    //Right face
    g->addVertex(0.0, size, -mid);
    g->addVertex(mid, 0.0, mid);
    g->addVertex(0.0, 0.0, -mid);
    g->addFace(9, 10, 11);
    g->addNormal(mid, size, -mid);
    g->addNormal(mid, size, -mid);
    g->addNormal(mid, size, -mid);

    g->addUV(0,0);
    g->addUV(0,1);
    g->addUV(1,1);
    g->compile();
}

Form::Pyramid::~Pyramid()
{

}


HEADERS += $$PWD/primitive.h \
           $$PWD/triangle.h \
           $$PWD/sphere.h \
           $$PWD/cube.h \
           $$PWD/torus.h \
           $$PWD/form.h \
           $$PWD/pyramid.h \
           $$PWD/kochsnowflake.h \
           $$PWD/plane.h \
           $$PWD/cone.h \
           $$PWD/cylinder.h \
           $$PWD/cubemap.h \
           $$PWD/bezier.h \
           $$PWD/grid.h \
           $$PWD/earth.h

SOURCES +=  $$PWD/primitive.cpp \
            $$PWD/triangle.cpp \
            $$PWD/sphere.cpp \
            $$PWD/cube.cpp \
            $$PWD/torus.cpp \
            $$PWD/pyramid.cpp \
            $$PWD/kochsnowflake.cpp \
            $$PWD/plane.cpp \
            $$PWD/cone.cpp \
            $$PWD/cylinder.cpp \
            $$PWD/cubemap.cpp \
            $$PWD/bezier.cpp \
            $$PWD/grid.cpp \
            $$PWD/earth.cpp

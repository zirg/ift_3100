#ifndef CUBEMAP
#define CUBEMAP

#include "form/cube.h"

class CubeMap : public Form::Cube {
public:
     CubeMap();
     void setTexture(const QOpenGLTexture *texture);
     void setSize(const unsigned int size);
     unsigned int getSize() const;
     ~CubeMap();
};

#endif // CUBEMAP


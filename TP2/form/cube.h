#ifndef CUBE_H
#define CUBE_H

#include "primitive.h"

namespace Form {

class Cube : public Primitive
{
public:
    Cube(float size = 5.0);
    ~Cube();
};

}
#endif // CUBE_H

#include <QtMath>
#include <QVector3D>

#include "torus.h"

Form::Torus::Torus()
    : Torus(2.5, 0.7, 32)
{
}

Form::Torus::Torus(float radius, float radius2, int density)
 : _radius(radius), _radius2(radius2), _density(density)
{
    _generateGeometry(radius, radius2, density);
}

void Form::Torus::setDensity(int density) {
    _density = density;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _radius2, _density);
}

void Form::Torus::setRadius(float radius) {
    _radius = radius;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _radius2, _density);
}

void Form::Torus::setRadius2(float radius2) {
    _radius2 = radius2;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _radius2, _density);
}

float Form::Torus::getRadius() const {
    return _radius;
}

float Form::Torus::getRadius2() const {
 return _radius2;
}

int Form::Torus::getDensity() const {
    return _density;
}

void Form::Torus::_generateGeometry(float radius1, float radius2, int density)
{
    float theta1 = 0.0;
    float theta2 = 0.0;
    float azimuth1 = 0.0;
    float azimuth2 = 0.0;
    QVector3D v1, v2, v3, v4, r1, r2;
    Geometry *geo = _geometry.front();

    for (int a = 0; a < density * 2; a++) //Iterate over azimuth to cover angle 2PI
    {
        azimuth1 = ((float)a / (float)((density - 1) * 2)) * 2.0 * M_PI;
        azimuth2 = ((float)(a + 1) / (float)((density - 1) * 2)) * 2.0 * M_PI;
        for (int t = 0; t < density; t++) //Iterate over theta to cover angle 2PI
        {
            theta1 = ((float)t / (float)(density - 1)) * 2.0 * M_PI;
            theta2 = ((float)(t + 1) / (float)(density - 1)) * 2.0 * M_PI;
            v1 = _toCarthesianCoord(radius2, theta1, azimuth1);
            v2 = _toCarthesianCoord(radius2, theta1, azimuth2);
            v3 = _toCarthesianCoord(radius2, theta2, azimuth2);
            v4 = _toCarthesianCoord(radius2, theta2, azimuth1);
            r1 = _toCarthesianCoord(radius1, M_PI / 4.0, azimuth1);
            r2 = _toCarthesianCoord(radius1, M_PI / 4.0, azimuth2);

            //Revolution axe y = 0;
            r1.setY(0.0);
            r2.setY(0.0);
            //Padding with revolution axe
            v1 += r1;
            v2 += r2;
            v3 += r2;
            v4 += r1;
            // Add quad poly
            _addQuad(v1, v2, v3, v4);
            //Add UVs
            geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta1 / (2.0 * M_PI));
            geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta1 / (2.0 * M_PI));
            geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta2 / (2.0 * M_PI));
            geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta2 / (2.0 * M_PI));
        }
     }
    geo->averageNormals();
    geo->compile();
}

QVector3D Form::Torus::_toCarthesianCoord(float radius, float theta, float azimuth)
{
    //Conversion according to :
    //http://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
    azimuth += M_PI / 2.0;
    float x = radius * sin(theta) * cos(azimuth);
    float y = radius * cos(theta);
    float z = radius * sin(theta) * sin(azimuth);
    return QVector3D(x, y, z);
}


#include "plane.h"

namespace Form {

Plane::Plane(float size)
 : _size(size)
{
    _generateGeometry();
}

float Plane::getSize() const {
    return _size;
}

void Plane::setSize(float size) {
    _size = size;
    Geometry * g = _geometry.front();
    g->clear();
    _generateGeometry();
}

void Plane::_generateGeometry() {
    float mid = _size / 2.0;
    QVector3D v1, v2, v3, v4;

    v1 = QVector3D(mid, 0, -mid);
    v2 = QVector3D(-mid, 0, -mid);
    v3 = QVector3D(-mid, 0, mid);
    v4 = QVector3D(mid, 0, mid);
    _addQuad(v1, v2, v3, v4);

    Geometry * g = _geometry.front();
    g->normals().clear();
    g->addNormal(0.0, 1.0, 0.0);
    g->addNormal(0.0, 1.0, 0.0);
    g->addNormal(0.0, 1.0, 0.0);
    g->addNormal(0.0, 1.0, 0.0);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);
    g->compile();
}

}

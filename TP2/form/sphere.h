#ifndef SPHERE_H
#define SPHERE_H

#include <cmath>
#include <QVector3D>
#include "primitive.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

namespace Form {

    class Sphere : public Primitive
    {
    public:
        Sphere();
        Sphere(float radius, int density);
        ~Sphere() = default;
        void setDensity(int density);
        float getRadius() const;
        void setRadius(float radius);
        int getDensity() const;
    protected:
        float _radius;
        int _density;
        void _generateGeometry(float radius, int density);
        QVector3D _toCarthesianCoord(float radius, float theta, float azimuth);
    };
}
#endif // SPHERE_H

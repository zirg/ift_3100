#ifndef PRIMITIVE_H
#define PRIMITIVE_H

#include <QMatrix4x4>
#include "../object3d.h"

namespace Form {

    class Primitive : public Object3D
    {
    public:
        Primitive();
        ~Primitive();
        virtual void setAmbiantColor(QVector4D color);
        virtual void setDiffuseColor(QVector4D color);
        virtual void setSpecularColor(QVector4D color);
        virtual const QVector4D& getAmbiantColor();
        virtual const QVector4D& getSpecularColor();
        virtual const QVector4D& getDiffuseColor();
        virtual void _addTri(QVector3D& v1, QVector3D& v2, QVector3D& v3);
        virtual void _addQuad(QVector3D& v1, QVector3D& v2, QVector3D& v3, QVector3D & v4);
    };
}
#endif // PRIMITIVE_H

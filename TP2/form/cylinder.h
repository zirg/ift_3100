#ifndef CYLINDER_H
#define CYLINDER_H

#include <cmath>
#include <QVector3D>
#include "primitive.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

namespace Form {

class Cylinder : public Primitive
{
public:
    Cylinder(float radius, float height, int density);
    float getRadius() const;
    float getHeight() const;
    void setRadius(float radius);
    void setHeight(float height);
    ~Cylinder();
private:
    float _radius;
    float _height;
    int _density;
    void _generateGeometry(float radius, float height, int density);
    QVector3D _toCarthesianCoord(float radius, float theta, float height);
};
}
#endif // CYLINDER_H

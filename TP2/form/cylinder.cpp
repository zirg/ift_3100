#include "cylinder.h"

Form::Cylinder::Cylinder(float radius, float height, int density)
    : _radius(radius), _height(height), _density(density)
{
    _generateGeometry(radius, height, density);
}

Form::Cylinder::~Cylinder()
{

}

float Form::Cylinder::getRadius() const {
    return _radius;
}

float Form::Cylinder::getHeight() const {
    return _height;
}

void Form::Cylinder::setHeight(float height) {
    _height = height;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _height, _density);
}

void Form::Cylinder::setRadius(float radius) {
    _radius = radius;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _height, _density);
}


void Form::Cylinder::_generateGeometry(float radius, float height, int density)
{
    float theta1, theta2;
    QVector3D vt, vb, v1, v2, v3, v4;
    Geometry *geo = _geometry.front();

    vt = QVector3D(0.0, height / 2.0, 0.0);
    vb = QVector3D(0.0, -height / 2.0, 0.0);
    for (int i = 0; i < density - 1; i++)
    {
        theta1 = ((float)i / (float)(density - 1)) * 2.0 * M_PI;
        theta2 = ((float)(i + 1) / (float)(density - 1)) * 2.0 * M_PI;

        v1 = _toCarthesianCoord(radius, theta1, height / 2.0);
        v2 = _toCarthesianCoord(radius, theta2, height / 2.0);
        v3 = _toCarthesianCoord(radius, theta1, -height / 2.0);
        v4 = _toCarthesianCoord(radius, theta2, -height / 2.0);

        _addTri(vb, v3, v4);
        geo->addUV(0.0, 0.0);
        geo->addUV(0.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta2 / (2.0 * M_PI));

        _addTri(vt, v2, v1);
        geo->addUV(1.0, 0.0);
        geo->addUV(1.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(1.0, 1.0 - theta2 / (2.0 * M_PI));

        _addTri(v1, v4, v3);
        geo->addUV(1.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta2 / (2.0 * M_PI));

        _addTri(v1, v2, v4);
        geo->addUV(1.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(1.0, 1.0 - theta2 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta2 / (2.0 * M_PI));

    }
    geo->averageNormals();
    geo->compile();
}

QVector3D Form::Cylinder::_toCarthesianCoord(float radius, float theta, float height)
{
    return QVector3D(radius * cos(theta), height, radius * sin(theta));
}

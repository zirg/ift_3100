#ifndef CONE_H
#define CONE_H

#include <cmath>
#include <QVector3D>
#include "primitive.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

namespace Form {

class Cone : public Primitive
{
public:
    Cone(float radius, float height, int density);
    virtual ~Cone();
    void setRadius(float radius);
    void setHeight(float height);
    void setDensity(int density);
    float getRadius() const;
    float getHeight() const;
    int getDensity() const;
private:
    float _radius;
    float _height;
    int _density;
    void _generateGeometry(float radius, float height, int density);
    QVector3D _toCarthesianCoord(float radius, float theta);
};

}
#endif // CONE_H

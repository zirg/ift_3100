#ifndef KOCHSNOWFLAKE_H
#define KOCHSNOWFLAKE_H

#include "primitive.h"
#include "pyramid.h"

namespace Form {

class KochSnowflake : public Pyramid
{
public:
    KochSnowflake(float size = 1.0, int g = 0);
    ~KochSnowflake();
    float getSize() const;
    int getFactor() const;
    void setSize(float size);
    void setFactor(int factor);
private:
    void _clear();
    void _generateRec(Object3D *parent, int current);
    int     _factor;
    float   _size;
};
}
#endif // KOCHSNOWFLAKE_H

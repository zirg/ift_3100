#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <QVector3D>
#include "primitive.h"

namespace Form {

    class Triangle : public Primitive
    {
    public:
        Triangle(float size = 1.0);
        ~Triangle();
    };
}// namespace

#endif // TRIANGLE_H

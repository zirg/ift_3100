#include "cone.h"

Form::Cone::    Cone(float radius, float height, int density)
    : _radius(radius), _height(height), _density(density)
{
    _generateGeometry(radius, height, density);
}

Form::Cone::~Cone()
{

}

void Form::Cone::setRadius(float radius) {
    _radius = radius;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _height, _density);
}

void Form::Cone::setHeight(float height) {
    _height = height;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _height, _density);
}

void Form::Cone::setDensity(int density) {
    _density = density;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _height, _density);
}

float Form::Cone::getRadius() const {
    return _radius;
}

float Form::Cone::getHeight() const {
 return _height;
}

int Form::Cone::getDensity() const {
    return _density;
}

void Form::Cone::_generateGeometry(float radius, float height, int density)
{
    float theta1, theta2;
    QVector3D vt, vb, v2, v3;

    Geometry *geo = _geometry.front();

    vt = QVector3D(0.0, height, 0.0);
    vb = QVector3D(0.0, 0.0, 0.0);
    for (int i = 0; i < density - 1; i++)
    {
        theta1 = ((float)i / (float)(density - 1)) * 2.0 * M_PI;
        theta2 = ((float)(i + 1) / (float)(density - 1)) * 2.0 * M_PI;

        v2 = _toCarthesianCoord(radius, theta1);
        v3 = _toCarthesianCoord(radius, theta2);

        _addTri(vt, v3, v2);
        geo->addUV(1.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta2 / (2.0 * M_PI));
        _addTri(vb, v2, v3);
        geo->addUV(0.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta1 / (2.0 * M_PI));
        geo->addUV(0.0, 1.0 - theta2 / (2.0 * M_PI));

    }
    geo->averageNormals();
    geo->compile();
}

QVector3D Form::Cone::_toCarthesianCoord(float radius, float theta)
{
    return QVector3D(radius * cos(theta), 0.0, radius * sin(theta));
}

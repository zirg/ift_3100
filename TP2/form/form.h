#ifndef FORM
#define FORM

namespace Form {
    class Primitive;
    class Triangle;
    class Cube;
    class Sphere;
    class Torus;
    class Pyramid;
    class KochSnowflake;
    class Plane;
    class Cone;
    class Cylinder;
    class Bezier;
    class Grid;
    class Earth;
}

#include "triangle.h"
#include "sphere.h"
#include "torus.h"
#include "cube.h"
#include "pyramid.h"
#include "material.h"
#include "kochsnowflake.h"
#include "plane.h"
#include "cone.h"
#include "cylinder.h"
#include "cubemap.h"
#include "bezier.h"
#include "grid.h"
#include "earth.h"

#endif // FORM


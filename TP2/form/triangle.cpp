#include "triangle.h"

Form::Triangle::Triangle(float size)
{
    float mid = size / 2.0;
    Geometry *g = _geometry.front();
    g->addVertex(mid, mid, 0.0);
    g->addVertex(-mid, -mid, 0.0);
    g->addVertex(mid, -mid, 0.0);
    g->addFace(0, 1, 2);

    g->addUV(0,0);
    g->addUV(0,1);
    g->addUV(1,1);

    //Computing normals
    float *d = g->vertices().data();
    QVector3D v = QVector3D(d[0], d[1], d[2]) - QVector3D(d[3], d[4], d[5]);
    QVector3D w = QVector3D(d[3], d[4], d[5]) - QVector3D(d[6],d[7], d[8]);
    g->addNormal(QVector3D::crossProduct(v, w).normalized());
    g->addNormal(QVector3D::crossProduct(v, w).normalized());
    g->addNormal(QVector3D::crossProduct(v, w).normalized());

    g->compile();
}

Form::Triangle::~Triangle()
{
    Geometry *g = _geometry.front();
    g->clear();
}


#include "earth.h"

namespace Form {

Earth::Earth()
    : Earth(1.5, 42)
{}

Earth::Earth(float radius, int density)
    : Sphere(radius, density)
{
    _material.front()->setType(Material::EARTH_MATERIAL);
    _material.front()->addTexture("../assets/earth/map.jpg");
    _material.front()->addTexture("../assets/earth/lights.jpg");
    _material.front()->addTexture("../assets/earth/spec.jpg");
    _material.front()->addTexture("../assets/earth/clouds.jpg");
    _material.front()->addTexture("../assets/earth/bump.jpg");
    _material.front()->addTexture("../assets/earth/normal.jpg");
}

}

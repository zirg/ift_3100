#ifndef SURFACE_H
#define SURFACE_H

#include "primitive.h"

namespace Form {

class Bezier : public Primitive
{
public:
    Bezier(const int density = 32);

    ~Bezier();

    size_t getNbPoints() const;

    QVector3D &getPoint(const size_t row, const size_t col);

    void    regenerate();

private:

    std::vector<QVector3D> _generateCurve(
            const QVector3D& p1, const QVector3D& p2, const QVector3D& p3) const;

    void _generateGeometry();

private:

    double _theta;

    const size_t _points;

    QVector3D **_controlPoints;

};

}

#endif // SURFACE_H

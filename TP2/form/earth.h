#ifndef EARTH_H
#define EARTH_H

#include "sphere.h"

namespace Form {

class Earth : public Sphere
{
public:
    Earth();

    Earth(float radius, int density);

    ~Earth() = default;

protected:

    using Object3D::addMaterial;
    using Object3D::removeMaterial;
    using Object3D::getMaterial;
};

}

#endif // EARTH_H

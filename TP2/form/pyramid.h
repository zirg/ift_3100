#ifndef PYRAMID_H
#define PYRAMID_H

#include "primitive.h"

namespace Form {

class Pyramid : public Primitive
{
    friend class KochSnowflake;
public:
    Pyramid(float size = 1.0);
    ~Pyramid();
};
}
#endif // PYRAMID_H

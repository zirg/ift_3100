#include "primitive.h"

#include <QVector2D>

Form::Primitive::Primitive()
{
    Geometry *g = new Geometry();
    Material *m = new Material(Material::PHONG_MATERIAL, "BASE_MATERIAL");

    //Default Grey
    m->setAmbiant(QVector4D(0.3, 0.3, 0.3, 1.0));
    m->setDiffuse(QVector4D(0.7, 0.7, 0.7, 1.0));
    m->setSpecular(QVector4D(1.0, 1.0, 1.0, 1.0));

    addMaterial(m);
    g->setMaterialName(m->getName());
    addGeometry(m->getName(), g);
}

Form::Primitive::~Primitive()
{
    _geometry.clear();
    _material.clear();
}

void Form::Primitive::setAmbiantColor(QVector4D color)
{
    if (_material.empty() == false)
    {
        Material *m = _material.front();
        m->setAmbiant(color);
    }
}

void Form::Primitive::setDiffuseColor(QVector4D color) {
    if (_material.empty() == false)
    {
        Material *m = _material.front();
        m->setDiffuse(color);
    }
}

void Form::Primitive::setSpecularColor(QVector4D color) {
    if (_material.empty() == false)
    {
        Material *m = _material.front();
        m->setSpecular(color);
    }
}

const QVector4D& Form::Primitive::getAmbiantColor()
{
    if (_material.empty() == false)
    {
        Material *m = _material.front();
        return m->getAmbiant();
    }
    return *(new QVector4D(0.0, 0.0, 0.0, 0.0));
}

const QVector4D& Form::Primitive::getDiffuseColor()
{
    if (_material.empty() == false)
    {
        Material *m = _material.front();
        return m->getDiffuse();
    }
    return *(new QVector4D(0.0, 0.0, 0.0, 0.0));
}

const QVector4D& Form::Primitive::getSpecularColor()
{
    if (_material.empty() == false)
    {
        Material *m = _material.front();
        return m->getSpecular();
    }
    return *(new QVector4D(0.0, 0.0, 0.0, 0.0));
}

void Form::Primitive::_addTri(QVector3D& v1, QVector3D& v2, QVector3D& v3)
{
    Geometry *g = _geometry.front();
    int face_count = g->vertices().size() / 3;

    //Creating triangle face
    g->addVertex(v1);
    g->addVertex(v2);
    g->addVertex(v3);
    g->addFace(face_count, face_count + 1, face_count + 2);
    //Computing triangle surface normal

    QVector3D n = QVector3D::crossProduct(v1 - v2, v2 - v3).normalized();

    g->addNormal(n);
    g->addNormal(n);
    g->addNormal(n);
}

void Form::Primitive::_addQuad(QVector3D& v1, QVector3D& v2, QVector3D& v3, QVector3D& v4)
{
    Geometry *g = _geometry.front();
    int face_count = g->vertices().size() / 3;

    //Creating triangles faces
    g->addVertex(v1);
    g->addVertex(v2);
    g->addVertex(v3);
    g->addVertex(v4);
    g->addFace(face_count, face_count + 1, face_count + 2);
    g->addFace(face_count, face_count + 2, face_count + 3);
    //Computing triangles surfaces normals
    g->addNormal(QVector3D::crossProduct(v1 - v2, v2 - v3).normalized());
    g->addNormal(QVector3D::crossProduct(v1 - v2, v2 - v3).normalized());
    g->addNormal(QVector3D::crossProduct(v1 - v3, v3 - v4).normalized());
    g->addNormal(QVector3D::crossProduct(v1 - v3, v3 - v4).normalized());
}

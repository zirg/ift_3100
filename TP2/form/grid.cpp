#include <QVector3D>

#include "grid.h"

Form::Grid::Grid(const int size)
{
    unsigned int vertices_count = 0;
    Geometry * g = _geometry.front();
    for(int i = 0; i <= size; ++i) {

        int face_count = vertices_count / 3;

        QVector3D v1(i, 0, 0);
        QVector3D v2(i, 0, size);
        QVector3D v3(0, 0, i);
        QVector3D v4(size, 0, i);

        g->addVertex(v1);
        g->addVertex(v2);
        g->addVertex(v3);
        g->addVertex(v4);
        g->addNormal(v1);
        g->addNormal(v1);
        g->addNormal(v1);
        g->addNormal(v1);
        g->addFace(face_count, face_count + 1, face_count + 2);
        g->addFace(face_count + 2, face_count + 2, face_count + 3);
        vertices_count += 12;
    };

    const int mid = size / 2;
    translate(QVector3D(-mid, 0.0, -mid));

    Form::Cylinder *axeZ = new Form::Cylinder(0.02, 1.0, 64);
    axeZ->orient(90, QVector3D(1.0, 0.0, 0.0));
    axeZ->move(QVector3D(mid, 0.0, mid));
    axeZ->translate(QVector3D(0, 0, 0.5));
    axeZ->setAmbiantColor(QVector4D(1.0, 0.0, 0.0, 1.0));
    axeZ->setDiffuseColor(QVector4D(1.0, 0.0, 0.0, 1.0));
    axeZ->setSpecularColor(QVector4D(1.0, 0.0, 0.0, 1.0));
    axeZ->getGeometry().front()->compile();
    axeZ->mat().front()->setType(Material::BASE_MATERIAL);
    Form::Cone *coneZ = new Form::Cone(0.05, 0.2, 64);
    coneZ->translate(QVector3D(0, 0.5, 0.0));
    coneZ->setAmbiantColor(QVector4D(1.0, 0.0, 0.0, 1.0));
    coneZ->setDiffuseColor(QVector4D(1.0, 0.0, 0.0, 1.0));
    coneZ->setSpecularColor(QVector4D(1.0, 0.0, 0.0, 1.0));
    coneZ->getGeometry().front()->compile();
    coneZ->mat().front()->setType(Material::BASE_MATERIAL);

    axeZ->addChild(coneZ);

    Form::Cylinder *axeY = new Form::Cylinder(0.02, 1.0, 64);
    axeY->move(QVector3D(mid, 0.0, mid));
    axeY->translate(QVector3D(0, 0.5, 0.0));
    axeY->setAmbiantColor(QVector4D(0.0, 1.0, 0.0, 1.0));
    axeY->setDiffuseColor(QVector4D(0.0, 1.0, 0.0, 1.0));
    axeY->setSpecularColor(QVector4D(0.0, 1.0, 0.0, 1.0));
    axeY->getGeometry().front()->compile();
    axeY->mat().front()->setType(Material::BASE_MATERIAL);
    Form::Cone *coneY = new Form::Cone(0.05, 0.2, 64);
    coneY->translate(QVector3D(0, 0.5, 0.0));
    coneY->setAmbiantColor(QVector4D(0.0, 1.0, 0.0, 1.0));
    coneY->setDiffuseColor(QVector4D(0.0, 1.0, 0.0, 1.0));
    coneY->setSpecularColor(QVector4D(0.0, 1.0, 0.0, 1.0));
    coneY->getGeometry().front()->compile();
    coneY->mat().front()->setType(Material::BASE_MATERIAL);

    axeY->addChild(coneY);


    Form::Cylinder *axeX = new Form::Cylinder(0.02, 1.0, 64);
    axeX->orient(90, QVector3D(0.0, 0.0, 1.0));
    axeX->move(QVector3D(mid, 0.0, mid));
    axeX->translate(QVector3D(0.5, 0.0, 0.0));
    axeX->setAmbiantColor(QVector4D(0.0, 0.0, 1.0, 1.0));
    axeX->setDiffuseColor(QVector4D(0.0, 0.0, 1.0, 1.0));
    axeX->setSpecularColor(QVector4D(0.0, 0.0, 1.0, 1.0));
    axeX->getGeometry().front()->compile();
    axeX->mat().front()->setType(Material::BASE_MATERIAL);
    Form::Cone *coneX = new Form::Cone(0.05, 0.2, 64);
    coneX->orient(180, QVector3D(0.0, 0.0, 1.0));
    coneX->translate(QVector3D(0, -0.5, 0.0));
    coneX->setAmbiantColor(QVector4D(0.0, 0.0, 1.0, 1.0));
    coneX->setDiffuseColor(QVector4D(0.0, 0.0, 1.0, 1.0));
    coneX->setSpecularColor(QVector4D(0.0, 0.0, 1.0, 1.0));
    coneX->getGeometry().front()->compile();
    coneX->mat().front()->setType(Material::BASE_MATERIAL);

    axeX->addChild(coneX);

    addChild(axeZ);
    addChild(axeY);
    addChild(axeX);

    g->compile();

    mat().front()->setType(Material::LINE_MATERIAL);
}

Form::Grid::~Grid()
{

}



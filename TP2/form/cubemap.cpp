#include "cubemap.h"

CubeMap::CubeMap() : Form::Cube(1000) {
    mat().front()->setType(Material::CUBE_MAP_MATERIAL);
}

void CubeMap::setTexture(const QOpenGLTexture *) {
    QOpenGLTexture *texture = new QOpenGLTexture(QOpenGLTexture::TargetCubeMap);

    texture->create();
    texture->bind();

    const char * suffixes[] = { "right", "left", "top", "bottom", "back", "front" };
    GLuint targets[] = { GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                         GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                         GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                         GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                         GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                         GL_TEXTURE_CUBE_MAP_NEGATIVE_Z};

    for( int i = 0; i < 6; i++ ) {
        QString fileName = QString("../assets/skybox/") + suffixes[i] + ".jpg";
        QImage image(fileName);
        image = image.convertToFormat(QImage::Format_RGB888);
        glTexImage2D(targets[i], 0, GL_RGB, image.width(), image.height(),0, GL_RGB, GL_UNSIGNED_BYTE, image.bits());
    }

    texture->release();

    mat().front()->setTexture(texture);
}

void CubeMap::setSize(const unsigned int size) {
    this->resize(QVector3D(size, size, size));
}

unsigned int CubeMap::getSize() const {
    return this->getScale().x();
}

CubeMap::~CubeMap() {

}

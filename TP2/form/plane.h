#ifndef PLANE_H
#define PLANE_H

#include "primitive.h"

namespace Form {

class Plane : public Primitive
{

public:
    Plane(float size = 1.0);

    float getSize() const;
    void setSize(float);

    ~Plane() = default;
private:
    float _size;
    void _generateGeometry();
};
}

#endif // PLANE_H

#include "cube.h"

Form::Cube::Cube(float size)
{
    float mid = size / 2.0;
    QVector3D v1, v2, v3, v4;

    //Face 1 - Front
    v1 = QVector3D(mid, mid, mid);
    v2 = QVector3D(mid, -mid, mid);
    v3 = QVector3D(-mid, -mid, mid);
    v4 = QVector3D(-mid, mid, mid);
    _addQuad(v1, v2, v3, v4);

    //Face 5 - Left Side
    v1 = QVector3D(-mid, -mid, mid);
    v2 = QVector3D(-mid, mid, mid);
    v3 = QVector3D(-mid, mid, -mid);
    v4 = QVector3D(-mid, -mid, -mid);
    _addQuad(v1, v2, v3, v4);

    //Face 3 - Bottom
    v1 = QVector3D(mid,-mid, mid);
    v2 = QVector3D(-mid, -mid, mid);
    v3 = QVector3D(-mid, -mid, -mid);
    v4 = QVector3D(mid, -mid, -mid);
    _addQuad(v1, v2, v3, v4);

    //Face 5 - Right Side
    v1 = QVector3D(mid, -mid, mid);
    v2 = QVector3D(mid, mid, mid);
    v3 = QVector3D(mid, mid, -mid);
    v4 = QVector3D(mid, -mid, -mid);
    _addQuad(v1, v2, v3, v4);

    //Face 4 - Top
    v1 = QVector3D(mid, mid, mid);
    v2 = QVector3D(-mid, mid, mid);
    v3 = QVector3D(-mid, mid, -mid);
    v4 = QVector3D(mid, mid, -mid);
    _addQuad(v1, v2, v3, v4);

    //Face 2 - Back
    v1 = QVector3D(mid, mid, -mid);
    v2 = QVector3D(mid, -mid, -mid);
    v3 = QVector3D(-mid, -mid, -mid);
    v4 = QVector3D(-mid, mid, -mid);
    _addQuad(v1, v2, v3, v4);

    Geometry * g = _geometry.front();
    g->normals().clear();
    g->addNormal(0.0, 0.0, 1.0);
    g->addNormal(0.0, 0.0, 1.0);
    g->addNormal(0.0, 0.0, 1.0);
    g->addNormal(0.0, 0.0, 1.0);

    g->addNormal(-1.0, 0.0, 0.0);
    g->addNormal(-1.0, 0.0, 0.0);
    g->addNormal(-1.0, 0.0, 0.0);
    g->addNormal(-1.0, 0.0, 0.0);

    g->addNormal(0.0, -1.0, 0.0);
    g->addNormal(0.0, -1.0, 0.0);
    g->addNormal(0.0, -1.0, 0.0);
    g->addNormal(0.0, -1.0, 0.0);

    g->addNormal(1.0, 0.0, 0.0);
    g->addNormal(1.0, 0.0, 0.0);
    g->addNormal(1.0, 0.0, 0.0);
    g->addNormal(1.0, 0.0, 0.0);

    g->addNormal(0.0, 1.0, 0.0);
    g->addNormal(0.0, 1.0, 0.0);
    g->addNormal(0.0, 1.0, 0.0);
    g->addNormal(0.0, 1.0, 0.0);

    g->addNormal(0.0, 0.0, -1.0);
    g->addNormal(0.0, 0.0, -1.0);
    g->addNormal(0.0, 0.0, -1.0);
    g->addNormal(0.0, 0.0, -1.0);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);

    g->addUV(0,0);
    g->addUV(1,0);
    g->addUV(1,1);
    g->addUV(0,1);
    g->compile();
}

Form::Cube::~Cube()
{

}



#include <cassert>
#include "bezier.h"

namespace Form {

Bezier::Bezier(const int density)
    : _theta(1 / 32), _points(3)
{
    if (density > 0)
        _theta = 1 / (double)density;

    _controlPoints = new QVector3D*[_points];
    for (size_t i = 0; i < _points ; ++i)
        _controlPoints[i] = new QVector3D[_points];

    _controlPoints[0][0] = QVector3D( -15., 15., -10.);
    _controlPoints[0][1] = QVector3D( -10.,  0.,  0.);
    _controlPoints[0][2] = QVector3D( -10., 10.,  10.);
    _controlPoints[1][0] = QVector3D(  0.,  00., -10.);
    _controlPoints[1][1] = QVector3D(  0.,  -10.,  0.);
    _controlPoints[1][2] = QVector3D(  0.,  0.,  10.);
    _controlPoints[2][0] = QVector3D(  10., 5., -10.);
    _controlPoints[2][1] = QVector3D(  10.,  0.,  0.);
    _controlPoints[2][2] = QVector3D(  10., 10.,  10.);

    _generateGeometry();
}

Bezier::~Bezier()
{
    for (size_t i = 0; i < _points ; ++i)
        delete _controlPoints[i];
    delete _controlPoints;
}

QVector3D &Bezier::getPoint(const size_t row, const size_t col)
{
    assert(row < _points && col < _points);
    return _controlPoints[row][col];
}

size_t Bezier::getNbPoints() const
{
    return _points;
}

void    Bezier::regenerate()
{
    _generateGeometry();
}

std::vector<QVector3D> Bezier::_generateCurve(const QVector3D& p1, const QVector3D& p2, const QVector3D& p3) const
{
    std::vector<QVector3D> ret;

    auto getPt = [](const double n1 , const double n2 , const double perc )
    {
        const double diff = n2 - n1;
        return n1 + (diff * perc);
    };

    for (double i = 0.0 ; i <= 1.0 ; i += _theta)
    {
        const double xa = getPt( p1.x() , p2.x() , i );
        const double ya = getPt( p1.y() , p2.y() , i );
        const double za = getPt( p1.z() , p2.z() , i );
        const double xb = getPt( p2.x() , p3.x() , i );
        const double yb = getPt( p2.y() , p3.y() , i );
        const double zb = getPt( p2.z() , p3.z() , i );

        const double x = getPt( xa , xb , i );
        const double y = getPt( ya , yb , i );
        const double z = getPt( za , zb , i );
        ret.push_back(QVector3D(x, y, z));
    }
    return ret;
}

void Bezier::_generateGeometry()
{
    Geometry *g = _geometry.front();
    g->clear();

    std::vector<QVector3D> curve1 = _generateCurve(_controlPoints[0][0], _controlPoints[1][0], _controlPoints[2][0]);
    std::vector<QVector3D> curve2 = _generateCurve(_controlPoints[0][1], _controlPoints[1][1], _controlPoints[2][1]);
    std::vector<QVector3D> curve3 = _generateCurve(_controlPoints[0][2], _controlPoints[1][2], _controlPoints[2][2]);

    std::vector<std::vector<QVector3D>> surface;
    for (size_t i = 0; i < curve1.size() - 1; ++i)
    {
        const QVector3D p1 = curve1[i];
        const QVector3D p2 = curve2[i];
        const QVector3D p3 = curve3[i];

        surface.push_back(_generateCurve(p1, p2, p3));
    }

    curve1.clear();
    curve2.clear();
    curve3.clear();

    size_t size = surface.size() - 1;
    for (size_t i = 0; i < size; ++i) {
        size_t size2 = surface.size() - 1;
        for (size_t j = 0; j < size2; ++j) {
            _addQuad(surface[i + 1][j + 1], surface[i + 1][j], surface[i][j], surface[i][j + 1]);
            g->addUV((float)(i + 1) / (float)size, (float)(j + 1) / (float)size2);
            g->addUV((float)(i + 1)/ (float)size, (float)j / (float)size2);
            g->addUV((float)i / (float)size, (float)(j) / (float)size2);
            g->addUV((float)i / (float)size, (float)(j + 1) / (float)size2);
        }
    }
    g->averageNormals();
    g->compile();
}


}

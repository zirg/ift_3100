#include <QtMath>
#include <QVector3D>

#include "sphere.h"

Form::Sphere::Sphere()
    : _radius(1.5), _density(42)
{ 
    _generateGeometry(_radius, _density);
}

Form::Sphere::Sphere(float radius, int density)
    :  _radius(radius), _density(density)
{
    _generateGeometry(_radius, _density);
}

void Form::Sphere::setRadius(float radius) {
    _radius = radius;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _density);
}

void Form::Sphere::setDensity(int density) {
    _density = density;
    Geometry *geo = _geometry.front();
    if (geo) geo->clear();
    _generateGeometry(_radius, _density);
}

float Form::Sphere::getRadius() const {
    return _radius;
}

int Form::Sphere::getDensity() const {
    return _density;
}

void Form::Sphere::_generateGeometry(float radius, int density)
{
    float theta1 = 0.0;
    float theta2 = 0.0;
    float azimuth1 = 0.0;
    float azimuth2 = 0.0;
    QVector3D v1, v2, v3, v4;
    Geometry *geo = _geometry.front();

    for (int t = 0; t < density; t++) //Iterate over theta to cover angle PI
    {
        theta1 = ((float)t / (float)(density - 1)) * M_PI;
        theta2 = ((float)(t + 1) / (float)(density - 1)) * M_PI;
        for (int a = 1; a < density; a++) //Iterate over azimuth to cover angle 2PI
        {
            azimuth1 = ((float)a / (float)(density - 1)) * 2.0 * M_PI;
            azimuth2 = ((float)(a + 1) / (float)(density - 1)) * 2.0 * M_PI;

            v1 = _toCarthesianCoord(radius, theta1, azimuth1);
            v2 = _toCarthesianCoord(radius, theta1, azimuth2);
            v3 = _toCarthesianCoord(radius, theta2, azimuth2);
            v4 = _toCarthesianCoord(radius, theta2, azimuth1);

            if (t == 0) // Prevent Top quad face
            {
                _addTri(v1, v3, v4);
                geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta1 / M_PI);
                geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta2 / M_PI);
                geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta2 / M_PI);
            }
            else if (t + 1 == density) // Prevent bottom quad face
            {
                _addTri(v1, v2, v3);
                geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta1 / M_PI);
                geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta1 / M_PI);
                geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta2 / M_PI);
            }
            else // Add quad poly
            {
                //Adding first triangle
                _addQuad(v1, v2, v3, v4);
                geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta1 / M_PI);
                geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta1 / M_PI);
                geo->addUV(azimuth2 / (2.0 * M_PI), 1.0 - theta2 / M_PI);
                geo->addUV(azimuth1 / (2.0 * M_PI), 1.0 - theta2 / M_PI);
            }
        }
    }
    geo->averageNormals();
    geo->compile();
}


QVector3D Form::Sphere::_toCarthesianCoord(float radius, float theta, float azimuth)
{
    //Conversion according to :
    //http://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
    azimuth += M_PI / 2.0;
    float x = radius * sin(theta) * cos(azimuth);
    float y = radius * cos(theta);
    float z = radius * sin(theta) * sin(azimuth);
    return QVector3D(x, y, z);
}

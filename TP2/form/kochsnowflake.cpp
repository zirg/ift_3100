#include "kochsnowflake.h"

Form::KochSnowflake::KochSnowflake(float size, int g) :
    Form::Pyramid(size)
{
    _size = size;
    _factor = g;
    _generateRec(this, 1);
}

Form::KochSnowflake::~KochSnowflake()
{

}

float Form::KochSnowflake::getSize() const {
    return _size;
}

int Form::KochSnowflake::getFactor() const {
    return _factor;
}

void Form::KochSnowflake::_clear() {
    std::list<Object3D*> child = this->getChildren();
    for (auto it = child.begin(); it != child.end(); ++it) {
       this->removeChild(*it);
       delete (*it);
    }
}

void Form::KochSnowflake::setSize(float size) {
    this->_clear();
    _size = size;
    _generateRec(this, 1);
}


void Form::KochSnowflake::setFactor(int factor) {
    this->_clear();
    _factor = factor;
    _generateRec(this, 1);
}

void Form::KochSnowflake::_generateRec(Object3D *parent, int current)
{
   if (current >= _factor)
        return;

   //Front pyramid
    Pyramid * p1 = new Pyramid(_size);
    p1->scale(QVector3D(0.5, 0.5, 0.5));
    p1->rotate(50.0, QVector3D(1.0, 0.0, 0.0));
    p1->translate(QVector3D(0.0, _size, -_size));


    //Left pyramid
    Pyramid * p2 = new Pyramid(_size);
    p2->scale(QVector3D(0.5, 0.5, 0.5));
    p2->translate(QVector3D(-_size / 1.32, _size / 1.32, -_size));
    p2->rotate(90.0, QVector3D(-0.5, 0.32, 1.32));


    //Right pyramid
    Pyramid * p3 = new Pyramid(_size);
    p3->scale(QVector3D(0.5, 0.5, 0.5));
    p3->translate(QVector3D(_size / 1.32, _size / 1.32, -_size));
    p3->rotate(90.0, QVector3D(-0.5, -0.32, -1.32));

    parent->addChild(p1);
    parent->addChild(p2);
    parent->addChild(p3);
    _generateRec(p1, current + 1);
    _generateRec(p2, current + 1);
    _generateRec(p3, current + 1);
    return;
}

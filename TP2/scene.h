#ifndef SCENE_H
#define SCENE_H

#include <iostream>
#include <list>
#include <map>

#include "material.h"
#include "geometry.h"
#include "object3d.h"
#include "light.h"
#include "camera.h"
#include "form/grid.h"
#include "form/cubemap.h"
#include "program/program.h"

class Scene
{
public:
    Scene();
    ~Scene() = default;

    //Public usage interface
    void                clear();
    bool                empty() const;
    void                setCamera(Camera *);
    Camera              *getCamera();

    void                addObject3D(Object3D *);
    void                removeObject3D(Object3D *);
    std::list<Object3D *>& getObject();

    void                addLight(Light *);
    void                removeLight(Light *);
    std::list<Light *>& getLight();

    void reset();

    void setScreenEffect(const Program::AScreenProgram::eProgram &effect);
    Program::AScreenProgram::eProgram getScreenEffect() const;

    void setCubeMap(CubeMap *cubemap);
    CubeMap *getCubeMap() const;
    void displayCubeMap(const bool state);
    bool cubeMapIsVisible() const;

    void displayGrid(const bool state);
    bool gridIsVisible() const;
    Form::Grid *getGrid() const;
    void setGrid(Form::Grid *grid);

private:
    bool                        _displayGrid;
    bool                        _displayCubemap;
    Form::Grid*                 _grid;
    Camera*                     _camera;
    CubeMap*                    _cubemap;
    std::list<Object3D *>       _obj;
    std::list<Light *>          _light;
    Program::AScreenProgram::eProgram _screenEffect;
};

#endif // SCENE_H

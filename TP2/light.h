#ifndef LIGHT_H
#define LIGHT_H

#include <QVector4D>
#include "movable.h"

class Light : public Movable
{
public:
    enum eLight
    {
        AMBIANT = 0,
        DIRECTIONAL,
        POINT,
        SPOT
    };

public:
    Light(Light::eLight type);
    ~Light();

    void                setType(eLight type);
    eLight              getType() const;

    void                setIntensity(float i);
    float               getIntensity() const;

    void                setAmbiant(QVector4D color);
    const QVector4D&    getAmbiant() const;
    void                setDiffuse(QVector4D color);
    const QVector4D&    getDiffuse() const;
    void                setSpecular(QVector4D color);
    const QVector4D&    getSpecular() const;

    void                setConstantAtt(float value);
    float               getConstantAtt() const;
    void                setLinearAtt(float value);
    float               getLinearAtt() const;
    void                setQuadraticAtt(float value);
    float               getQuadraticAtt() const;

    void                setCutoff(float value);
    float               getCutoff() const;
    float               getCosCutOff() const;

    void                setExponent(float value);
    float               getExponent() const;

    QVector3D           getDirection() const;
private:
    Light::eLight _type;
    float         _intensity;
    QVector4D     _ambiant;
    QVector4D     _diffuse;
    QVector4D     _specular;
    float         _constant_att;// [0.0, 1.0] Point + Spot
    float         _linear_att;// [0.0, 1.0] Point + Spot
    float         _quadratic_att;// [0.0, 1.0] Point + Spot
    float         _cutoff; // [0.0, 90.0] Spot
    float         _coscutoff;// [1.0, 0.0] Spot
    float         _exponent; // [0.0, +inf] Spot
};

#endif // LIGHT_H

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <vector>
#include <QMatrix4x4>
#include <QVector2D>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

#include "threadpool.h"

class Geometry : protected QOpenGLFunctions
{
public:
    Geometry();
    ~Geometry();

    std::vector<float>&  vertices();
    std::vector<int>&    faces();
    std::vector<float>&  normals();
    std::vector<float>&  uvs();
    QOpenGLBuffer *vboVertex();
    QOpenGLBuffer *vboIndex();
    QOpenGLBuffer *vboNormal();

    void    destroy();
    bool    compile();
    bool    isGenerated();
    void    clear();
    void    addVertex(QVector3D vertex);
    void    addVertex(float x, float y, float z);
    void    addFace(int a, int b, int c);
    void    addNormal(const QVector3D& normal);
    void    addNormal(float x, float y, float z);
    void    addUV(float x, float y);
    void    addUV(const QVector2D &uv);

    void    setMaterialName(const std::string&);
    const std::string&  getMaterialName() const;
    void    averageNormals();
protected:
    bool    _generated;
    ThreadPool _pool;

    QOpenGLBuffer       *_vbo_vertex;
    QOpenGLBuffer       *_vbo_normal;
    QOpenGLBuffer       *_vbo_index;

    std::vector<float>  _vertex;
    std::vector<int>    _face;
    std::vector<float>  _normal;
    std::vector<float>  _uv;

    std::string         _material_name;
};

#endif // GEOMETRY_H


#ifndef VIDEO_OBSERVER_HPP
#define VIDEO_OBSERVER_HPP

#include <algorithm> // std::find
#include <list>

#include <QImage>

namespace Video
{
class Observable;

class Observer
{
public:
    virtual void addObservable(Observable* listenable)
    {
        m_obervers.push_back(listenable);
    }

    virtual void removeObservable(Observable* listenable)
    {
        auto it = std::find(m_obervers.begin(), m_obervers.end(), listenable);
        if (it != m_obervers.end())
            m_obervers.erase(it);
    }

    virtual void onNewFrame(const QImage& frame) = 0;

protected:
    virtual ~Observer() = default;

    std::list<Observable*> m_obervers;
};
}

#endif // VIDEO_OBSERVER_HPP

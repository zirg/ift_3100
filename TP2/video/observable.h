
#ifndef VIDEO_OBSERVABLE_HPP
#define VIDEO_OBSERVABLE_HPP

#include <algorithm> // std::find
#include <list>
#include <mutex>

#include <QImage>

#include "observer.h"

namespace Video
{
class Observable
{
public:
    virtual void addObserver(Observer* listener)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_obervables.push_back(listener);
        listener->addObservable(this);
    }

    virtual void removeObserver(Observer* listener)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        listener->removeObservable(this);
        auto it = find(m_obervables.begin(), m_obervables.end(), listener);
        if (it != m_obervables.end())
            m_obervables.erase(it);
    }

protected:
    virtual ~Observable() = default;

    virtual void notify(const QImage& frame)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (auto itb = m_obervables.begin(); itb != m_obervables.end(); ++itb) {
            (*itb)->onNewFrame(frame);
        }
    }

private:
    mutable std::mutex m_mutex;

    std::list<Observer*> m_obervables;
};
}

#endif // VIDEO_OBSERVABLE_HPP


#include "videocamera.h"

namespace Video
{

VideoCamera::VideoCamera()
    : _camera(new QCamera), _videoSurface(new ::Video::VideoSurface)
{
    _videoSurface->addObserver(this);
    _camera->setViewfinder(_videoSurface);
}

VideoCamera::~VideoCamera()
{
    if (_camera != 0)
        delete _camera;
    _camera = nullptr;
    if (_videoSurface != 0)
        delete _videoSurface;
    _videoSurface = nullptr;
}

void VideoCamera::start()
{
    _camera->start();
}

void VideoCamera::stop()
{
    _camera->stop();
    _camera->unload();
}

void VideoCamera::onNewFrame(const QImage& frame)
{
    this->notify(frame);
}

}

#ifndef VIDEO_CAMERAVIDEOSURFACE_H
#define VIDEO_CAMERAVIDEOSURFACE_H

#include <QtMultimedia/QAbstractVideoSurface>

#include "observable.h"

namespace Video
{
class VideoSurface : public Observable, public QAbstractVideoSurface
{
public:
    VideoSurface() = default;

    ~VideoSurface() = default;

private:

     bool present(const QVideoFrame& frame);

     QList<QVideoFrame::PixelFormat>  supportedPixelFormats(QAbstractVideoBuffer::HandleType type = QAbstractVideoBuffer::NoHandle) const;

};
}

#endif // VIDEO_CAMERAVIDEOSURFACE_H

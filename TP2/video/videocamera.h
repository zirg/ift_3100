
#ifndef VIDEO_CAMERA_H
#define VIDEO_CAMERA_H

#include <QtMultimedia/QCamera>

#include "videosurface.h"

#include "observable.h"
#include "observer.h"

namespace Video
{
class VideoCamera : public Observer, public Observable
{
public:
    VideoCamera();

    ~VideoCamera();

    void start();

    void stop();

private:

    void onNewFrame(const QImage& frame);

    QCamera* _camera;

    VideoSurface* _videoSurface;

};
}

#endif // VIDEO_CAMERA_H

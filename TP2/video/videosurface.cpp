
#include "videosurface.h"

namespace Video
{

bool VideoSurface::present(const QVideoFrame& frame)
{
    QVideoFrame cloneFrame(frame);

    if(cloneFrame.map(QAbstractVideoBuffer::ReadOnly))
    {
        const unsigned int width = cloneFrame.width();
        const unsigned int height = cloneFrame.height();
        const unsigned int size_line = cloneFrame.bytesPerLine();
        const unsigned char* bits = cloneFrame.bits();

        QImage img(bits,width,height,size_line, QVideoFrame::imageFormatFromPixelFormat(cloneFrame.pixelFormat()));

        this->notify(img);

        cloneFrame.unmap();
    }
    return true;
}

QList<QVideoFrame::PixelFormat>  VideoSurface::supportedPixelFormats(QAbstractVideoBuffer::HandleType type) const
{
    if (type == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB32;
    }
    return QList<QVideoFrame::PixelFormat>();
}

}

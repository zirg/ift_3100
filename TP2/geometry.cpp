
#include "geometry.h"

Geometry::Geometry()
    : _generated(false),
      _pool(1)
{
    if (QOpenGLContext::currentContext() != NULL)
    {
        initializeOpenGLFunctions();
    }

    _vbo_vertex = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    _vbo_index = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    _vbo_normal = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
}

Geometry::~Geometry()
{
    this->destroy();

    delete _vbo_vertex;
    delete _vbo_index;
    delete _vbo_normal;
}

void    Geometry::destroy()
{
    _vbo_vertex->destroy();
    _vbo_index->destroy();
    _vbo_normal->destroy();
}

std::vector<float>&  Geometry::vertices() { return _vertex; }
std::vector<int>&    Geometry::faces() { return _face; }
std::vector<float>&  Geometry::normals() { return _normal; }
std::vector<float>&  Geometry::uvs() { return _uv; }

bool    Geometry::compile()
{
    _vbo_vertex->create();
    _vbo_vertex->bind();
    _vbo_vertex->allocate(_vertex.data(), _vertex.size() * sizeof(float));
    _vbo_vertex->release();

    _vbo_index->create();
    _vbo_index->bind();
    _vbo_index->allocate(_face.data(), _face.size() * sizeof(int));
    _vbo_index->release();

    //averageNormals();
    _vbo_normal->create();
    _vbo_normal->bind();
    _vbo_normal->allocate(_normal.data(), _normal.size() * sizeof(float));
    _vbo_normal->release();

    return true;
}

void    Geometry::averageNormals()
{
    _pool.enqueue([this] {
#pragma omp parallel
        {
            int idx[56];
            float x, y, z;
            float normal[3] = {0.0f, 0.0f, 0.0f};
            int count;
            for (int j = _face.size() - 1; j > 0; --j)
            {
                count = 0;
                normal[0] = 0.0f;
                normal[1] = 0.0f;
                normal[2] = 0.0f;
                x = _vertex[_face[j] * 3];
                y = _vertex[_face[j] * 3 + 1];
                z = _vertex[_face[j] * 3 + 2];
#pragma omp for schedule(auto)
                for (int i = _face[j] * 3; i < (int)_vertex.size() - 2; i += 3)
                {
                    if (count < 55 && _vertex[i] == x && _vertex[i + 1] == y && _vertex[i + 2] == z)
                    {
                        idx[count] = i;
                        count++;
                        normal[0] += _normal[i];
                        normal[1] += _normal[i + 1];
                        normal[2] += _normal[i + 2];
                    }
                }

                normal[0] /= (double)count;
                normal[1] /= (double)count;
                normal[2] /= (double)count;
                for (int i = 0; i < count && i < 56; ++i)
                {
                    _normal[idx[i]] = normal[0];
                    _normal[idx[i] + 1] = normal[1];
                    _normal[idx[i] + 2] = normal[2];
                }
            }
            _generated = true;
        }
    });
}

bool Geometry::isGenerated()
{
    return this->_generated;
}

QOpenGLBuffer *Geometry::vboVertex() { return _vbo_vertex; }
QOpenGLBuffer *Geometry::vboIndex() { return _vbo_index; }
QOpenGLBuffer *Geometry::vboNormal() { return _vbo_normal; }

void    Geometry::clear()
{
    _vertex.clear();
    _face.clear();
    _normal.clear();
    _uv.clear();

    _vbo_vertex->destroy();
    _vbo_index->destroy();
    _vbo_normal->destroy();
}

void    Geometry::addVertex(QVector3D vertex)
{
    _vertex.push_back(vertex.x());
    _vertex.push_back(vertex.y());
    _vertex.push_back(vertex.z());
}

void    Geometry::addVertex(float x, float y, float z)
{
    _vertex.push_back(x);
    _vertex.push_back(y);
    _vertex.push_back(z);
}

void    Geometry::addFace(int a, int b, int c)
{
    _face.push_back(a);
    _face.push_back(b);
    _face.push_back(c);
}

void    Geometry::addNormal(const QVector3D& normal)
{
    _normal.push_back(normal.x());
    _normal.push_back(normal.y());
    _normal.push_back(normal.z());
}

void    Geometry::addNormal(float x, float y, float z)
{
    _normal.push_back(x);
    _normal.push_back(y);
    _normal.push_back(z);
}

void    Geometry::addUV(const QVector2D &uv) {
    _uv.push_back(uv.x());
    _uv.push_back(uv.y());
}

void    Geometry::addUV(float x, float y)
{
    _uv.push_back(x);
    _uv.push_back(y);
}

void    Geometry::setMaterialName(const std::string& name)
{
    _material_name = name;
}

const std::string&  Geometry::getMaterialName() const
{
    return _material_name;
}

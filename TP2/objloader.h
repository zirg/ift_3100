#ifndef OBJLOADER
#define OBJLOADER

#include <map>
#include <QVector3D>
#include <QVector2D>

#include "material.h"
#include "object3d.h"

class ObjLoader {
private:
    std::string _pathObj;

    std::vector<QVector3D> _vertices, _normals;
    std::vector<QVector2D> _uvs;

    Material *_defaultMaterial;
    std::map<std::string, Material *> _materials;

    typedef void (Material::*pfComponent)(QVector4D);
    std::map<char, pfComponent> _components;

public:
    ObjLoader();
    ~ObjLoader();
    Object3D* load(const std::string &pathObj);

private:
    Geometry *_createGeometry();
    void _cleanMaterials();
    void _addTri(Geometry *g, QVector3D& v1, QVector3D& v2, QVector3D& v3, bool = true);
    void _addQuad(Geometry *g, QVector3D& v1, QVector3D& v2, QVector3D& v3, QVector3D & v4, bool = true);
    bool _parseMtl(const std::string &name);
    void _checkNormals(Object3D *obj);
    bool _parseLine(Geometry *geo, const std::string &line);
};

#endif // OBJLOADER


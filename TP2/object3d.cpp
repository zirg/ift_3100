#include "object3d.h"

Object3D::Object3D()
    : _parent(nullptr)
{}

Object3D::~Object3D()
{}

std::list<Geometry *>& Object3D::geo() { return _geometry; }

std::list<Material *>& Object3D::mat() { return _material; }

void                    Object3D::addChild(Object3D *o)
{
    o->_parent = this;
    _child.push_back(o);
}

void                    Object3D::removeChild(Object3D *o)
{
    o->_parent = nullptr;
    _child.remove(o);
}

std::list<Object3D *>&   Object3D::getChildren()
{
    return _child;
}

Object3D*   Object3D::getParent()
{
    return _parent;
}

void       Object3D::addGeometry(const std::string& material_name,
                        Geometry *geometry)
{
    _geometry.push_back(geometry);
    geometry->setMaterialName(material_name);
}

void        Object3D::addGeometry(Geometry *geometry)
{
    _geometry.push_back(geometry);
}

std::list<Geometry *>&  Object3D::getGeometry()
{
    return _geometry;
}

void        Object3D::removeGeometry(Geometry *g)
{
    _geometry.remove(g);
}

void        Object3D::addMaterial(Material *material)
{
  _material.push_back(material);
}

void       Object3D::removeMaterial(Material *m)
{
    _material.remove(m);
}

Material*   Object3D::getMaterial(const std::string& name)
{
    for (std::list<Material *>::iterator it = _material.begin();
         it != _material.end(); ++it)
    {
        if ((*it)->getName() == name)
            return *it;
    }
    return NULL;
}
